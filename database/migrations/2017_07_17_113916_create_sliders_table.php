<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSlidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sliders', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slider_image');
            $table->string('slider_tittle');
            $table->string('slider_subtittle');
            $table->string('slider_status');
            $table->timestamps();
        });
    }



    public function down()
    {
        Schema::dropIfExists('sliders');
    }
}
