<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Auth::routes();

//Route::get('/index', 'HomeController@index');
Route::get('/', 'HomeController@home');
Route::get('menu/{id}', 'HomeController@menu');
Route::get('submenu/{id}', 'HomeController@submenu');
Route::get('/about', 'HomeController@about');

Route::get('/portfolio/{id}', 'HomeController@portfolio');
Route::get('/service/{id}', 'HomeController@service');
Route::get('/packages', 'HomeController@packages');
Route::get('/contact', 'HomeController@contact');
Route::get('/gallery', 'HomeController@gallery');
Route::post('/storecontact', 'HomeController@storecontact');


// Route::get('/wedding_event/{id}', 'HomeController@wedding_event');
// Route::get('/allint/{id}', 'HomeController@allint');
// Route::get('/int/{id}', 'HomeController@int');
// Route::get('/birthday_cat', 'HomeController@birthday_cat');
// Route::get('/birthday/{id}', 'HomeController@birthday');
// Route::get('/music_cat', 'HomeController@music_cat');
// Route::get('/music/{id}', 'HomeController@music');

// Route::get('/service', 'HomeController@service');
// Route::get('/package', 'HomeController@package');
// Route::get('/gallery', 'HomeController@gallery');







// Route::get('/service_category/{id}', 'HomeController@service_cat');
// Route::get('/allpackage', 'HomeController@allpackage');
// Route::get('/package/{id}', 'HomeController@singlepackage');
// Route::get('/gallery', 'HomeController@gallery');
// Route::get('/client_contact', 'HomeController@client_contact');
// Route::get('/og_contact', 'HomeController@og_contact');
// Route::post('/storecontact_client', 'HomeController@storecontact_client');
// Route::post('/storecontact', 'HomeController@storecontact_og');
//Route::post('/form','HomeController@upload'); 


//Hit counter Ajax

// Route::get('/check_session_status', 'HitController@check_session_status');
// Route::get('/destroy_session_status', 'HitController@destroy_session_status');

// Backend routes Start

//Route::get(['middleware' => 'auth'], function () {
Route::group(['middleware' => 'auth'], function(){
    
			Route::get('/shobarjonnoweb', 'DashboardController@index');
			
			
			Route::get('/complain', 'ComplainController@complain');
	
		Route::post('/postcomplain', 'ComplainController@postcomplain');






			// Package category
			// Route::get('/packagecategories', 'PackageCategoryController@index');
			// Route::get('/addpackagecategory', 'PackageCategoryController@add');
			// Route::post('/storepackagecategory', 'PackageCategoryController@store');
			// Route::get('/editpackagecategory/', 'PackageCategoryController@edit');
			
			// Route::post('/updatepackagecategory/{id}', 'PackageCategoryController@update');
			// Route::get('/deletepackagecategory/{id}', 'PackageCategoryController@delete');

			// Route::get('/package_items', 'PackageCategoryController@package_items');
			// Route::post('/addpackageitem', 'PackageCategoryController@addpackageitem');
			// Route::get('/deletepackageitem/{id}', 'PackageCategoryController@deletepackageitem');



			// Interior subcategory
			// Route::get('/intsubcategories', 'SubcategoryController@index');
			// Route::get('/addintsubcategory', 'SubcategoryController@add');
			// Route::post('/storeintsubcategory', 'SubcategoryController@store');
			// Route::get('/editintsubcategory/{id}', 'SubcategoryController@edit');
			// Route::post('/updateintsubcategory/{id}', 'SubcategoryController@update');
			// Route::get('/deleteintsubcategory/{id}', 'SubcategoryController@destroy');

			// // /Packages 
			// Route::get('/packages', 'PackageController@index');
			// Route::get('/addpackage', 'PackageController@add');
			// Route::post('/storepackage', 'PackageController@store');
			// Route::get('/editpackage/{id}', 'PackageController@edit');
			// Route::post('/updatepackage/{id}', 'PackageController@update');
			// Route::get('/deletepackage/{id}', 'PackageController@destroy');

			// // /birthday 
			// Route::get('/storys', 'StoryController@index');
			// Route::get('/addstory', 'StoryController@add');
			// Route::post('/storestory', 'StoryController@store');
			// Route::get('/editstory/{id}', 'StoryController@edit');
			// Route::post('/updatestory/{id}', 'StoryController@update');
			// Route::get('/deletestory/{id}', 'StoryController@destroy');
//    Video
			// Route::get('/editvideo', 'BirthdayController@edit_video');
			// Route::post('/updatevideo/', 'BirthdayController@update_video');

			//  /Service 
			Route::get('/allservices', 'ServiceController@index');
			Route::get('/addservice', 'ServiceController@add');
			Route::post('/storeservice', 'ServiceController@store');
			Route::get('/singleservice/{id}', 'ServiceController@view');
			Route::get('/editservice/{id}', 'ServiceController@edit');
			Route::post('/updateservice/{id}', 'ServiceController@update');
			Route::get('/deleteservice/{id}', 'ServiceController@destroy');

			
			//  Portfolio , food
			Route::get('/allportfolios', 'PortfolioController@index');
			Route::get('/addportfolio', 'PortfolioController@add');
			Route::post('/storeportfolio', 'PortfolioController@store');
			Route::get('/singleportfolio/{id}', 'PortfolioController@view');
			Route::get('/editportfolio/{id}', 'PortfolioController@edit');
			Route::post('/updateportfolio/{id}', 'PortfolioController@update');
			Route::get('/deleteportfolio/{id}', 'PortfolioController@destroy');
			
			//  /Music 
			// Route::get('/musics', 'MusicController@index');
			// Route::get('/addmusic', 'MusicController@add');
			// Route::post('/storemusic', 'MusicController@store');
			// Route::get('/editmusic/{id}', 'MusicController@edit');
			// Route::post('/updatemusic/{id}', 'MusicController@update');
			// Route::get('/deletemusic/{id}', 'MusicController@destroy');


			// Event

			// Route::get('/events', 'EventController@index');
			// Route::get('/addevent', 'EventController@add');
			// Route::post('/storeevent', 'EventController@store');
			// Route::get('/editevent/{id}', 'EventController@edit');
			// Route::post('/updateevent/{id}', 'EventController@update');
			// Route::get('/deleteevent/{id}', 'EventController@destroy');


			// // package category
			Route::get('/categories', 'CategoryController@index');
			Route::get('/addcategory', 'CategoryController@add');
			Route::post('/storecategory', 'CategoryController@store');
			Route::get('/editcategory/{id}', 'CategoryController@edit');
			Route::post('/updatecategory/{id}', 'CategoryController@update');
			Route::get('/deletecategory/{id}', 'CategoryController@destroy');

			Route::get('/intsubcategories', 'SubcategoryController@index');
			Route::get('/addintsubcategory', 'SubcategoryController@add');
			Route::post('/storeintsubcategory', 'SubcategoryController@store');
			Route::get('/editintsubcategory/{id}', 'SubcategoryController@edit');
			Route::post('/updateintsubcategory/{id}', 'SubcategoryController@update');
			Route::get('/deleteintsubcategory/{id}', 'SubcategoryController@destroy');

			// // Slider
			Route::get('/sliders', 'SliderController@index');
			Route::get('/addslider', 'SliderController@add');
			Route::post('/storeslider', 'SliderController@store');
			Route::get('/editslider/{id}', 'SliderController@edit');
			Route::post('/updateslider/{id}', 'SliderController@update');
			Route::get('/deleteslider/{id}', 'SliderController@destroy');


			// // Gallery
			// Route::get('/galleries', 'GalleryController@index');
			// Route::get('/addgallery', 'GalleryController@add');
			// Route::post('/storegallery', 'GalleryController@store');
			// Route::post('/storegalleryvideo', 'GalleryController@storevideo');
			// Route::get('/editgallery/{id}', 'GalleryController@edit');
			// Route::post('/updategallery/{id}', 'GalleryController@update');
			// Route::get('/deletegallery/{id}', 'GalleryController@destroy');


			//Route::post('/storegallerycat', 'GalleryController@storegallerycat');



			// // work Gallery
			// Route::get('/workgalleries', 'WorkGalleryController@index');
			// Route::get('/addworkgallery', 'WorkGalleryController@add');
			// Route::post('/storeworkgallery', 'WorkGalleryController@store');
			// Route::post('/storeworkgalleryvideo', 'WorkGalleryController@storevideo');
			// Route::get('/editworkgallery/{id}', 'WorkGalleryController@edit');
			// Route::post('/updateworkgallery/{id}', 'WorkGalleryController@update');
			// Route::get('/deleteworkgallery/{id}', 'WorkGalleryController@destroy');

			// Route::post('/storeworkcat', 'WorkGalleryController@storeworkcat');

			// Route::get('/workgallerycat', 'WorkGalleryController@workgallerycat');
			// Route::get('/deleteworkgallerycat/{id}', 'WorkGalleryController@deleteworkgallerycat');




				// //  Gallery
			Route::get('/galleries', 'GalleryController@index');
			Route::get('/addgallery', 'GalleryController@add');
			Route::post('/storegallery', 'GalleryController@store');
			Route::post('/storegalleryvideo', 'GalleryController@storevideo');
			Route::get('/editgallery/{id}', 'GalleryController@edit');
			Route::post('/updategallery/{id}', 'GalleryController@update');
			Route::get('/deletegallery/{id}', 'GalleryController@destroy');

			// Route::post('/storegallerycat', 'GalleryController@storegallerycat');

			// Route::get('/gallerycat', 'GalleryController@gallerycat');
			// Route::get('/deletegallerycat/{id}', 'GalleryController@deletegallerycat');


			// // Team
			Route::get('/teammembers', 'TeamController@index');
			Route::get('/viewteammember/{id}', 'TeamController@show');
			Route::get('/addteammember', 'TeamController@add');
			Route::post('/storeteammember', 'TeamController@store');
			Route::get('/editteammember/{id}', 'TeamController@edit');
			Route::post('/updateteammember/{id}', 'TeamController@update');
			Route::get('/deleteteammember/{id}', 'TeamController@destroy');


			// // Review
			Route::get('/reviews', 'ReviewController@index');
			// Route::get('/viewreview/{id}', 'ReviewController@show');
			Route::get('/addreview', 'ReviewController@add');
			Route::post('/storereview', 'ReviewController@store');
			Route::get('/editreview/{id}', 'ReviewController@edit');
			Route::post('/updatereview/{id}', 'ReviewController@update');
			Route::get('/deletereview/{id}', 'ReviewController@destroy');



			// // About
			Route::get('/abouts', 'AboutController@index');
			// Route::get('/viewreview/{id}', 'ReviewController@show');
			Route::get('/addabout', 'AboutController@add');
			Route::post('/storeabout', 'AboutController@store');
			Route::get('/editabout/{id}', 'AboutController@edit');
			Route::post('/updateabout/{id}', 'AboutController@update');
			Route::get('/deleteabout/{id}', 'AboutController@destroy');


			// // cont
			Route::get('/messages', 'ContactController@index');

			Route::get('/deletecontact/{id}', 'ContactController@destroy');

			// // offer
			// Route::get('/offers', 'OfferController@index');
			// Route::get('/addoffer', 'OfferController@add');

			// Route::post('/storeoffer', 'OfferController@store');

		//	Route::get('/editoffer/{id}', 'OfferController@edit');

			//Route::post('/updateoffer/{id}', 'OfferController@update');
			//Route::get('/deleteoffer/{id}', 'OfferController@destroy');






//  Blog section backend

    //  /blog
     Route::get('/alloffers', 'BlogController@index');
    // Route::get('/addoffer', 'BlogController@add');
    // Route::post('/storeoffer', 'BlogController@store');
     Route::get('/singleoffer/{id}', 'BlogController@view');
     Route::get('/editoffer/{id}', 'BlogController@edit');
     Route::post('/updateoffer/{id}', 'BlogController@update');
    // Route::get('/deleteoffer/{id}', 'BlogController@destroy');




});

