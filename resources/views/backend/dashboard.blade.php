@extends('layouts.backend')

@section('content') 
<div class="row">

@php
      $messages = \DB::table('contacts')->get();
    $users = \DB::table('users')->get();
    $reviews = \DB::table('reviews')->get();
    $events = \DB::table('portfolios')->get();
    $visitor = \DB::table('visitors')->first();



@endphp
                        <div class="col-md-2">

                            <div class="widget widget-warning widget-carousel box">
                                <div class="owl-carousel" id="owl-example">
                                    <div>                                    
                                        <div class="widget-title">Total Visitors</div>                                                                        
                                        <div class="widget-subtitle"></div>
                                        <div class="widget-int">{{ $visitor->counter }}</div>
                                    </div>
                                   
                                    <div>                                    
                                        <div class="widget-title">New</div>
                                        <div class="widget-subtitle">Visitors Today</div>
                                        <div class="widget-int">{{ $visitor->daily_count }}</div>
                                    </div>
                                </div>                            
                               <div class="widget-controls">                                
                                    <a href="#" class="widget-control-right widget-remove" data-toggle="" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
                                </div>                             
                            </div>

                        </div>


                        <div class="col-md-2">

                             <div class="widget widget-info widget-item-icon box"  onclick="">
                                <div class="widget-item-left">
                                    <span class="fa fa-comments-o"></span>
                                </div>                             
                                <div class="widget-data">
                                    <div class="widget-int num-count">{{ count($reviews) }}</div>
                                    <div class="widget-title">Client Review</div>
                                    
                                </div>      
                                <div class="widget-controls">                                
                                    <a href="#" class="widget-control-right widget-remove" data-toggle="" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
                                </div>   
                            </div>               
                            
                           
                            
                        </div>
                        <div class="col-md-2">
                            
                            <!-- START WIDGET MESSAGES -->
                            <div class="widget widget-danger widget-item-icon box" onclick="">
                                <div class="widget-item-left">
                                    <span class="fa fa-envelope"></span>
                                </div>                             
                                <div class="widget-data">
                                    <div class="widget-int num-count">{{ count($messages) }}</div>
                                    <div class="widget-title">Orders</div>
                                    <div class="widget-subtitle">In your queue</div>
                                </div>      
                                <div class="widget-controls">                                
                                    <a href="#" class="widget-control-right widget-remove" data-toggle="" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
                                </div>
                            </div>                            
                            <!-- END WIDGET MESSAGES -->
                            
                        </div>

                        

                        <div class="col-md-2">
                            
                            <!-- START WIDGET REGISTRED -->
                            <div class="widget widget-success widget-item-icon box" onclick="">
                                <div class="widget-item-left">
                                    <span class="fa fa-user"></span>
                                </div>
                                <div class="widget-data">
                                    <div class="widget-int num-count">{{ count($users) }}</div>
                                    <div class="widget-title">Users</div>
                                    <div class="widget-subtitle">On your website</div>
                                </div>
                                <div class="widget-controls">                                
                                    <a href="#" class="widget-control-right widget-remove" data-toggle="" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
                                </div>                            
                            </div>                            
                            <!-- END WIDGET REGISTRED -->
                            
                        </div>

                       

                         <div class="col-md-2">
                            
                            <!-- START WIDGET REGISTRED -->
                            <div class="widget widget-primary widget-item-icon box" onclick="">
                                <div class="widget-item-left">
                                    <span class="fa fa-user"></span>
                                </div>
                                <div class="widget-data">
                                    <div class="widget-int num-count">{{ count($events) }}</div>
                                    <div class="widget-title">Total</div>
                                    <div class="widget-subtitle">Food Items</div>
                                </div>
                                <div class="widget-controls">                                
                                    <a href="#" class="widget-control-right widget-remove" data-toggle="" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
                                </div>                            
                            </div>                            
                            <!-- END WIDGET REGISTRED -->
                            
                        </div>
                         <div class="col-md-2">

                            <div class="widget widget-danger widget-padding-sm box">
                                <div class="widget-big-int plugin-clock">00:00</div>                            
                                <div class="widget-subtitle plugin-date">Loading...</div>
                               <div class="widget-controls">                                
                                    <a href="#" class="widget-control-right widget-remove" data-toggle="" data-placement="top" title="Remove Widget"><span class="fa fa-times"></span></a>
                                </div>                              
                                <div class="widget-buttons widget-c3">
                                    <div class="col">
                                        <a href="#"><span class="fa fa-clock-o"></span></a>
                                    </div>
                                    <div class="col">
                                        <a href="#"><span class="fa fa-bell"></span></a>
                                    </div>
                                    <div class="col">
                                        <a href="#"><span class="fa fa-calendar"></span></a>
                                    </div>
                                </div>                            
                            </div>                        

                        </div>
                            
                          
                            
                        </div>
                       
                    </div>

  <div class="row">


                        <div class="col-md-9">

                            <div class="col-md-8">
                                 <div class="panel panel-success box">
                                <div class="panel-heading">
                                    <div class="panel-title-box">
                                        <h3>Order Chart</h3>
                                        <span>All orders</span>
                                    </div>
                                    <ul class="panel-controls" style="margin-top: 2px;">
                                        <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                                      
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>                                        
                                            <ul class="dropdown-menu">
                                                <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a></li>
                                                <li><a href="#" class="panel-remove"><span class="fa fa-times"></span> Remove</a></li>
                                            </ul>                                        
                                        </li>                                        
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <div style="width:100%;">
                                        {!! $postjs->render() !!}
                                    </div>             
                                </div>
                            </div>
                            </div>
                            
                           


                          

                            <div class="col-md-4 " >
                                 <div class="panel panel-success box">
                                <div class="panel-heading">
                                     <div class="panel-title-box">
                                        <h3>Visitors</h3>
                                        <span>All Division</span>
                                    </div>
                                    <ul class="panel-controls" style="margin-top: 2px;">
                                        <li><a href="#" class="panel-fullscreen"><span class="fa fa-expand"></span></a></li>
                                        
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown"><span class="fa fa-cog"></span></a>                                        
                                            <ul class="dropdown-menu">
                                                <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span> Collapse</a></li>
                                                <li><a href="#" class="panel-remove"><span class="fa fa-times"></span> Remove</a></li>
                                            </ul>                                        
                                        </li>                                        
                                    </ul>
                                </div>
                                <div class="panel-body">
                                    <div style="height: 110%;">
                                        {!! $visitorjs->render() !!}
                                    </div>             
                                </div>
                            </div>

                            </div>

{{--   <div class="col-md-12">

                            <div class="panel panel-primary box">
                                <div class="panel-body">
                                    <h2><span class="fa fa-facebook"></span> Latest Posts from Facebook</h2>
                                    
                                </div>  

                               

                                <div class="panel-body list-group">
                                    @foreach ($response as $fb)
                                        @if (!empty($fb['message'])) 
                                        <div class="list-group-item">
                                        <div class="user">
                                         <p>
                                            <h4><a href="http://facebook.com/{{ $fb['from']['id'] }}"  target="_blank">{{ $fb['from']['name'] }}</a></h4>
                                        </p>
                                       
                                        </div>
                                       
                                           @php
                      
                                                $h = "6"; // Hour for time zone goes here e.g. +7 or -4, just remove the + or -
                                                $hm = $h * 60;
                                                $ms = $hm * 60;
                                                $mydate = gmdate("d-M-Y", strtotime($fb['updated_time']));
                                                $mytime = gmdate("d-M-Y h:i A", strtotime($fb['updated_time'])+ $ms);


                                                $rawdate = strtotime($mydate)+ $ms;
                                                $rawtime = strtotime($mytime);
   
                                            @endphp 
                                            
                                            <p><span class="text-muted"><a style="color: gray" href="http://facebook.com/{{ $fb['id'] }}" target="_blank">{{ $mytime }}</a></span></p>
                                           <p>
                                                @php
                                                    print_r($fb['message']);
                                                @endphp
                                           </p>
                                        </div>
                                        @endif
                                    @endforeach
                                </div>
                            </div>

                        </div>

             --}}
                    

                             <!-- START SPARKLINE CHARTS -->
                          

                        </div>

                         <div class="col-md-3">
                            <div class="fb-page" data-href="https://www.facebook.com/shobarjonnoweb/" data-tabs="timeline" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true"><blockquote cite="https://www.facebook.com/shobarjonnoweb/" class="fb-xfbml-parse-ignore"><a href="https://www.facebook.com/shobarjonnoweb/">Shobarjonnoweb</a></blockquote></div>
                        </div>
                       

                    </div>




@endsection