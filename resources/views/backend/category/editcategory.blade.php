@extends('layouts.backend')

@section('content') 
<div class="">
<div class="row">
    <div class="col-md-8">
    <hr>
    <h3>Edit Category</h3>
        <form action="{{ url('updatecategory/'.$cat[0]->id) }}" class="form-horizontal" method="post" enctype="multipart/form-data"">
        {{ csrf_field() }}
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Category Name</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="cat_name" value="{{ $cat[0]->cat_name }}" />
                                            </div>                                            
                                            {{-- <span class="help-block">This is sample of text field</span> --}}
                                        </div>
                                    </div>
                                    
                              
                                    
                                  <div class="form-group">
                                       <label class="col-md-3 col-xs-12 control-label"></label>
                                        <div class="col-md-6 col-xs-12">                                                                                            {{-- 
                                           <button class="btn btn-primary">Clear Form</button> --}}
                                           <input type="submit" name="submit" class="btn btn-primary">
                                           {{--  pull-right --}}
                                        </div>
                                    </div>

                        
        </form>
    </div>
</div>
</div>
@endsection