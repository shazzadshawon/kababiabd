@extends('layouts.backend')

@section('content') 
    <div class="">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                 <div class="panel panel-success">
                                <div class="panel-heading  panel-primary">                                
                                    <h3 class="panel-title">All Categories</h3>
                                   <div class="pull-right">
                                       <a href="{{ url('addcategory') }}" class="btn btn-primary">Add Category</a>
                                   </div>          
                                </div>
                                <div class="panel-body">
                                    <table class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                
                                                <th>Action</th>
                                               
                                            </tr>
                                        </thead>
                                        @php
                                            $i=1;
                                        @endphp
                                        <tbody>
                                            @foreach($cats as $cat)
                                            <tr>
                                                <td>{{ $i++ }}</td>
                                                <td>{{ $cat->cat_name }}</td>
                                                
                                                <td>
                                                    <a href="{{ url('editcategory/'.$cat->id) }}" class="btn btn-primary">Edit</a>
                                                    <a href="{{ url('deletecategory/'.$cat->id) }}" class="btn btn-danger">Delete</a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
    </div>
            </div>
        </div>
    </div>
@endsection