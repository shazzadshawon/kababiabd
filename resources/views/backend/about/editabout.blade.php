@extends('layouts.backend')

@section('content') 
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
             <div class="panel panel-success">
                 <div class="panel-heading"><h4>Update Spacial Service</h4></div>
                 <div class="panel-body">
                     <div class="block">
                                <form class="form-horizontal" method="POST" action="{{ url('updateabout/'.$about->id) }}"  enctype="multipart/form-data">      
                                {{ csrf_field() }}                             
                                   
                                     <div class="form-group">
                                        <label class="col-md-2 control-label">Title </label>
                                        <div class="col-md-10">
                                             <input type="text" name="do" class="form-control" required value="{{$about->about_title}}">
                                        </div>
                                    </div>

                                     <div class="form-group">
                                        <label class="col-md-2 control-label">Description </label>
                                        <div class="col-md-10">
                                            
                                          <textarea name="editor1">
                                              @php
                                                print_r($about->about_description);
                                              @endphp
                                          </textarea>
                                        </div>
                                    </div>
                                      
                                    <div class="form-group">
                                        <label class="col-md-2 control-label"></label>
                                        <div class="col-md-10">
                                            <input type="submit"  class="btn btn-success btn-lg" name="Submit" />
                                        </div>
                                    </div>

                                    
                                </form>
             </div>
                 </div>
             </div>
        </div>
    </div>
@endsection