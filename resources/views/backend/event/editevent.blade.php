@extends('layouts.backend')

@section('content') 
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
             <div class="panel panel-success">
                 <div class="panel-heading"><h4>Update Wedding Event</h4></div>
                 <div class="panel-body">
                     <div class="block">
                                
                               
                                 <form class="form-horizontal" method="POST" action="{{ url('updateevent/'.$event->id) }}"  enctype="multipart/form-data">      
                                {{ csrf_field() }}                             
                                    <div class="form-group">
                                        <label class="col-md-2 control-label"> Title</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="event_name" value="{{ $event->event_name }}" />
                                        </div>
                                    </div>
                                  {{--   <div class="form-group">
                                        <label class="col-md-2 control-label">Category</label>
                                        <div class="col-md-10">
                                            <select class="form-control" name="service_type_id">
                                            <option value="{{ $service->service_type_id }}">{{ $service->type_name }}</option>
                                               <option value="">Choose one</option>
                                                @foreach ($servicetypes as $sub)
                                                    <option value="{{ $sub->id }}">{{ $sub->type_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div> --}}

                                       <div class="form-group">
                                        <label class="col-md-2 control-label">date</label>
                                        <div class="col-md-10">
                                             <input type="date" class="form-control" name="event_date" value=""  autofocus>
                                             <span>{{ $event->event_date }}</span>
                                        </div>
                                    </div>
              
                                                                                
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Description </label>
                                        <div class="col-md-10">
                                            <div class="">
                                                <textarea  name="editor1">{{ $event->event_description }}</textarea>
       
                                            </div>
                                        </div>
                                    </div>

                                 


                                    <div class="form-group">
                                        <label class="col-md-2 control-label"></label>
                                        <div class="col-md-10">
                                            <input type="submit"  class="brn btn-success btn-lg" name="Submit" />
                                        </div>
                                    </div>

                                    
                                </form>
             </div>
                 </div>
             </div>
        </div>
    </div>
@endsection