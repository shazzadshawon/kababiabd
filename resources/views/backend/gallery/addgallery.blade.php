@extends('layouts.backend')

@section('content') 
<div class="row clearfix">
<div class="col-md-12">
    {{-- <button class="btn btn-success pull-right" data-toggle="modal" data-target="#modal_no_footer">Add Category</button> --}}
    <br>
</div>
</div>
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
             <div class="panel panel-success">
                 <div class="panel-heading"><h4>Gallery Image</h4></div>
                 <div class="panel-body">
                     <div class="block">
                                
                               
                                 <form class="form-horizontal" method="POST" action="{{ url('storegallery') }}"  enctype="multipart/form-data">      
                                {{ csrf_field() }}                             
                     
                                       <div class="form-group">
                                        <label class="col-md-2 control-label">Image</label>
                                        <div class="col-md-10">
                                             <input id="name" type="file" class="form-control" name="gallery_image" value="" required autofocus>
                                        </div>
                                    </div>

                                 {{--    <div class="form-group">
                                        <label class="col-md-2 control-label">Choose Category</label>
                                        <div class="col-md-10">
                                           <select name="place" required class="form-control" >
                                               @foreach ($cats as $element)
                                                   <option value="{{ $element->id }}">{{ $element->cat_name }}</option>
                                               @endforeach
                                           </select>
                                           <a href="" data-toggle="modal" data-target="#modal_no_footer">Add Category</a>
                                        </div>
                                    </div>
               --}}
                                                                                
                                    


                                    <div class="form-group">
                                        <label class="col-md-2 control-label"></label>
                                        <div class="col-md-10">
                                            <input type="submit"  class="btn btn-success btn-lg" name="Submit" value="Submit" />
                                        </div>
                                    </div>

                                    
                                </form>
             </div>
                 </div>
             </div>
        </div>
      
    </div>








@endsection