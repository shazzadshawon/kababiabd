@extends('layouts.backend')

@section('content') 
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
             <div class="panel panel-success">
                 <div class="panel-heading"><h4>Update offer</h4></div>
                 <div class="panel-body">
                     <div class="block">
                              
                               
                                 <form class="form-horizontal" method="POST" action="{{ url('updateoffer/'.$offer->id) }}"  enctype="multipart/form-data">      
                                {{ csrf_field() }}                             
                                 
                                    
                                 
                                     <div class="form-group">
                                        <label class="col-md-2 control-label">Offer Description</label>
                                        <div class="col-md-10">
                                         <input type="text" class="form-control" name="offer_description"  value="{{ $offer->offer_description }}"/>
                                            
                                        </div>
                                    </div>
                                      
                                     <div class="form-group">
                                        <label class="col-md-2 control-label">Offer Start</label>
                                        <div class="col-md-10">
                                            <input type="date" class="form-control" name="offer_start"  value="{{ $offer->offer_start }}"/>
                                            <span>{{ $offer->offer_start }}</span>
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label class="col-md-2 control-label">Offer end</label>
                                        <div class="col-md-10">
                                            <input type="date" class="form-control" name="offer_end"  value="{{ $offer->offer_end }}"/>
                                            <span>{{ $offer->offer_end }}</span>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label"></label>
                                        <div class="col-md-10">
                                            <input type="submit"  class="brn btn-success btn-lg" name="Submit" />
                                        </div>
                                    </div>

                                    
                                </form>
             </div>
                 </div>
             </div>
        </div>
    </div>
@endsection