@extends('layouts.backend')

@section('content') 
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
             <div class="panel panel-success">
                 <div class="panel-heading"><h4>Update Video</h4></div>
                 <div class="panel-body">
                     <div class="block">
                                
                               
                                 <form class="form-horizontal" method="POST" action="{{ url('updatevideo') }}"  enctype="multipart/form-data">      
                                {{ csrf_field() }}                             
                                   
                                 

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Select Video</label>
                                        <div class="col-md-10">
                                             <input id="video_name" type="file" class="form-control" name="video_name" value=""  autofocus>
                                        </div>
                                    </div>
                                    

                                     <div class="form-group">
                                        <label class="col-md-2 control-label"></label>
                                        <div class="col-md-10">
                                            <figure class="effect-moses load_video">
                                                <video style="width: 600px" poster="{{ asset('public/') }}images/page_load.gif" id="bgvid" playsinline autoplay muted loop>
                                                   {{--  <source src="http://thenewcode.com/assets/videos/polina.webm" type="video/webm"> --}}
                                                    <source src="{{ asset('public/uploads/video/'.$video->video_name) }}" type="video/mp4">
                                                </video>
                                               
                                            </figure>
                                        </div>
                                    </div>
              
                                 

                                    <div class="form-group">
                                        <label class="col-md-2 control-label"></label>
                                        <div class="col-md-10">
                                            <input type="submit"  class="brn btn-success btn-lg" name="Submit" />
                                        </div>
                                    </div>

                                    
                                </form>
             </div>
                 </div>
             </div>
        </div>
    </div>
@endsection