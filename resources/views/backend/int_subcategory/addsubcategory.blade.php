@extends('layouts.backend')

@section('content') 
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
             <div class="panel panel-success">
                 <div class="panel-heading"><h4>Add Sub Category</h4></div>
                 <div class="panel-body">
                     <div class="block">
                              
                               
                                 <form class="form-horizontal" method="POST" action="{{ url('storeintsubcategory') }}"  enctype="multipart/form-data">      
                                {{ csrf_field() }}                             
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Sub Category Name</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="sub_cat_name"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Category</label>
                                        <div class="col-md-10">
                                            <select class="form-control" name="category_id">
                                            <option value="">Choose one</option>
                                                @foreach ($types as $type)
                                                    <option value="{{ $type->id }}">{{ $type->cat_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label"></label>
                                        <div class="col-md-10">
                                            <input type="submit"  class="brn btn-success btn-lg" name="Submit" />
                                        </div>
                                    </div>

                                    
                                </form>
             </div>
                 </div>
             </div>
        </div>
    </div>
@endsection

