@extends('layouts.backend')

@section('content') 
    <div class="">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                 <div class="panel panel-success">
                                <div class="panel-heading  panel-primary">                                
                                    <h3 class="panel-title">All Sub Categories</h3>
                                   <div class="pull-right">
                                       <a href="{{ url('addintsubcategory') }}" class="btn btn-primary">Add Sub Category</a>
                                   </div>                        
                                </div>
                                <div class="panel-body">
                                    <table class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Category</th>
                                                <th>Action</th>
                                               
                                            </tr>
                                        </thead>
                                        @php
                                            $i=1;
                                        @endphp
                                        <tbody>
                                            @foreach($subcats as $sub)
                                            <tr>
                                                <td>{{ $i++ }}</td>
                                                
                                                <td>{{ $sub->sub_cat_name }}</td>
                                                <td>{{ $sub->cat_name }}</td>
                                                <td>
                                                    <a href="{{ url('editintsubcategory/'.$sub->id) }}" class="btn btn-primary">Edit</a>
                                                   

                                                        <button type="button" class="btn btn-danger mb-control" data-box="#message-box-warning">delete</button>
                                                   

                                                            <div class="message-box message-box-warning animated fadeIn" id="message-box-warning">
                                                                <div class="mb-container">
                                                                    <div class="mb-middle">
                                                                        <div class="mb-title"><span class="fa fa-warning"></span> Warning</div>
                                                                        <div class="mb-content">
                                                                            <p>Deleting this subcategory will
                                                                            also delete the services under this subcategory</p>
                                                                            <p>Are you sure you want to delete ?</p>                  
                                                                        </div>
                                                                        <div class="mb-footer">
                                                                            <button class="btn btn-default mb-control-close">Cancel</button>
                                                                            <a href="{{ url('deleteintsubcategory/'.$sub->id) }}" class="btn btn-danger">confirm</a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
    </div>
            </div>
        </div>
    </div>
@endsection