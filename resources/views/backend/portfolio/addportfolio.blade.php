@extends('layouts.backend')

@section('content') 
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
             <div class="panel panel-success">
                 <div class="panel-heading"><h4>Add Food Item</h4></div>
                 <div class="panel-body">
                     <div class="block">
                                
                               
                                 <form class="form-horizontal" method="POST" action="{{ url('storeportfolio') }}"  enctype="multipart/form-data">      
                                {{ csrf_field() }}                             
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Title</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="service_title"/>
                                        </div>
                                    </div>
                                     <div class="form-group">
                                        <label class="col-md-2 control-label">Product Code</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="code"/>
                                        </div>
                                    </div>

                                    
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Sub Category</label>
                                        <div class="col-md-10">
                                            <select class="form-control" name="sub_cat_id">
                                            <option value="">Choose one</option>
                                                @foreach ($subcategories as $sub)
                                                    <option value="{{ $sub->id }}">{{ $sub->sub_cat_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div> 

                                       <div class="form-group">
                                        <label class="col-md-2 control-label">Image</label>
                                        <div class="col-md-10">
                                             <input id="name" type="file" class="form-control" name="name" value="{{ old('name') }}" required autofocus>
                                        </div>
                                    </div>
                                    
                                     <div class="form-group">
                                        <label class="col-md-2 control-label">Price</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="price"/>
                                        </div>
                                    </div>
                                                                                
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Description </label>
                                        <div class="col-md-10">
                                            <div class="">
                                                <textarea  name="editor1" rows="1"></textarea>
       
                                            </div>
                                        </div>
                                    </div>
                                             
                                    <!--<div class="form-group">-->
                                    <!--    <label class="col-md-2 control-label">Address </label>-->
                                    <!--    <div class="col-md-10">-->
                                    <!--        <div class="">-->
                                    <!--            <textarea  name="editor2" rows="1"></textarea>-->
       
                                    <!--        </div>-->
                                    <!--    </div>-->
                                    <!--</div>-->

                                 


                                    <div class="form-group">
                                        <label class="col-md-2 control-label"></label>
                                        <div class="col-md-10">
                                            <input type="submit"  class="btn btn-success btn-lg" name="Submit" />
                                        </div>
                                    </div>

                                    
                                </form>
             </div>
                 </div>
             </div>
        </div>
    </div>
@endsection