@extends('layouts.backend')

@section('content') 
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
             <div class="panel panel-success">
                 <div class="panel-heading"><h4>Update Food Item</h4></div>
                 <div class="panel-body">
                     <div class="block">
                                
                               
                                 <form class="form-horizontal" method="POST" action="{{ url('updateportfolio/'.$service->id) }}"  enctype="multipart/form-data">      
                                {{ csrf_field() }}                             
                                    <div class="form-group">
                                        <label class="col-md-2 control-label"> Title</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="service_title" value="{{ $service->service_title }}" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label"> Product Code</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="code" value="{{ $service->code }}" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Sub Category</label>
                                        <div class="col-md-10">
                                            <select class="form-control" name="service_type_id">
                                            <option value="{{ $service->service_type_id }}">{{ $service->sub_cat_name }}</option>
                                               <option value="">Choose one</option>
                                                @foreach ($subcategories as $sub)
                                                    <option value="{{ $sub->id }}">{{ $sub->sub_cat_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div>

                                       <div class="form-group">
                                        <label class="col-md-2 control-label">Service Image</label>
                                        <div class="col-md-10">
                                             <input id="name" type="file" class="form-control" name="name" value=""  autofocus>
                                        </div>
                                    </div>
              
                                                                                
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Description </label>
                                        <div class="col-md-10">
                                            <div class="">
                                                <textarea  name="editor1">@php
                                                    print_r($service->service_description);
                                                @endphp</textarea>
       
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Price</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="price" value="{{ $service->price }}" />
                                        </div>
                                    </div>
                                 


                                    <div class="form-group">
                                        <label class="col-md-2 control-label"></label>
                                        <div class="col-md-10">
                                            <input type="submit"  class="btn btn-success btn-lg" name="Submit" />
                                        </div>
                                    </div>

                                    
                                </form>
             </div>
                 </div>
             </div>
        </div>
    </div>
@endsection