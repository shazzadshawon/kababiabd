@extends('layouts.backend')

@section('content') 
    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                            
                            <div class="panel panel-default">
                                <div class="panel-body posts">
                                            
                                    <div class="post-item">
                                        <div class="post-title">
                                            {{ $service->service_title }}
                                           
                                        </div>
                                         <div align="right" >
                                                 <?php
                                                        echo '<a class="btn btn-info" href="' . htmlspecialchars($login_link) . '">Post to Facebook !</a>';
                                                    ?>
                                            </div>
                                        <!--<div class="post-date"><span class="fa fa-calendar"></span>{{ $service->created_at }}</div>-->
                                        <div class="post-text">                                            
                                        <img class="img img-responsive" src="{{asset('public/uploads/service/'.$service->service_image)}}">
                                            <p>
                                                <?php print_r($service->service_description); ?>
                                            </p>
                                        </div>
                                       
                                    </div>                                            
                                            
                                </div>
                            </div>
                            
                        </div>
                        
                    </div>
@endsection