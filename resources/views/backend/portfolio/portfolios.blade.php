@extends('layouts.backend')

@section('content') 
    <div class="">
        <div class="row">
            <div class="col-md-12 ">
                 <div class="panel panel-success">
                                <div class="panel-heading  panel-primary">                                
                                    <h3 class="panel-title">All Food Items</h3>
                                    <div class="pull-right">
                                        <a class="btn btn-info" href="{{ url('addportfolio') }}"><span class="fa fa-plus"></span> Add New Item</a>
                                    </div>
                                   {{--  <ul class="panel-controls">
                                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>        --}}                         
                                </div>
                                <div class="panel-body">
                                    <table class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th style="width: 10%">Title</th>
                                                <th>Image</th>
                                                <th>Product Code</th>
                                                <th>Sub category</th>
                                                <th>Price</th>
                                                <th style="width: 30%">Description</th>
                                                <th>Action</th>
                                               
                                            </tr>
                                        </thead>
                                        @php
                                            $i=1;
                                        @endphp
                                        <tbody>
                                            @foreach($services as $cat)
                                            <tr>
                                                <td>{{ $i++ }}</td>
                                                <td>{{ $cat->service_title }}</td>
                                                <td><img style="width: 100px; height: 60px" src="{{ asset('public/uploads/portfolio/'.$cat->service_image)  }}"></td>
                                                <td>{{ $cat->code }}</td>
                                               <td>{{ $cat->sub_cat_name }}</td>
                                               
                                               <td>{{ $cat->price }}</td>
                                                
                                                <td>@php
                                                    print_r($cat->service_description);
                                                @endphp</td>
                                                <td>
                                                    <a href="{{ url('editportfolio/'.$cat->id) }}" class="btn btn-primary">Edit</a>
                                                    <a href="{{ url('singleportfolio/'.$cat->id) }}" class="btn btn-warning">View</a> 
                                                     

                                                     <button type="button" class="btn btn-danger mb-control" data-box="#{{ $cat->id }}">Delete</button>
                                                   

        <div class="message-box message-box-warning animated fadeIn" id="{{ $cat->id }}">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-warning"></span> Warning</div>
                    <div class="mb-content">
                        <p>Are you sure you want to delete this item ?</p>                  
                    </div>
                    <div class="mb-footer">
                        <button class="btn btn-default mb-control-close">Cancel</button>
                        <a href="{{ url('deleteportfolio/'.$cat->id) }}" class="btn btn-danger">Delete</a>
                    </div>
                </div>
            </div>
        </div>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
    </div>
            </div>
        </div>
    </div>
@endsection