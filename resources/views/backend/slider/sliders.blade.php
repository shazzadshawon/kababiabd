@extends('layouts.backend')

@section('content') 
    <div class="">
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                 <div class="panel panel-success">
                                <div class="panel-heading  panel-primary">                                
                                    <h3 class="panel-title">Cover Image</h3>
                                  
                                    <div class="pull-right">
                                        {{-- <a class="btn btn-primary" href="{{ url('addslider') }}"><span >Add Slider Image</span></a> --}}
                                    </div>
                                </div>
                                <div class="panel-body">
                                    <table class="table datatable">
                                        <thead>
                                            <tr>
                                             
                                                <th>Image</th>
                                                <th>Title</th>
                                                <th>Subtitle</th>
                                                <th>Action</th>
                                               
                                            </tr>
                                        </thead>
                                        @php
                                            $i=1;
                                        @endphp
                                        <tbody>
                                            @foreach($sliders as $cat)
                                            <tr>
                                              
                                                
                                                <td><img style="width: 100px; height: 60px" src="{{ asset('public/uploads/slider/'.$cat->slider_image)  }}"></td>
                                                <td>{{ $cat->slider_title }}</td>
                                                <td>{{ $cat->slider_subtitle }}</td>
                                                <td>
                                                    <a href="{{ url('editslider/'.$cat->id) }}" class="btn btn-primary">Edit</a>
                                                     {{-- <a href="{{ url('deleteslider/'.$cat->id) }}" class="btn btn-danger">Delete</a> --}}



                                                     {{-- <button type="button" class="btn btn-danger mb-control" data-box="#{{ $cat->id }}">Delete </button> --}}
                                                   

        <div class="message-box message-box-warning animated fadeIn" id="{{ $cat->id }}">
            <div class="mb-container">
                <div class="mb-middle">
                    <div class="mb-title"><span class="fa fa-warning"></span> Warning</div>
                    <div class="mb-content">
                        <p>Are you sure you want to delete this item ?</p>                  
                    </div>
                    <div class="mb-footer">
                        <button class="btn btn-default mb-control-close">Cancel</button>
                        <a href="{{ url('deleteslider/'.$cat->id) }}" class="btn btn-danger">Delete</a>
                    </div>
                </div>
            </div>
        </div>
                                                     
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
    </div>
            </div>
        </div>
    </div>
@endsection