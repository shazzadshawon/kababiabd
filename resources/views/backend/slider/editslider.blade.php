@extends('layouts.backend')

@section('content') 
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
             <div class="panel panel-success">
                 <div class="panel-heading"><h4>Update Cover Image</h4></div>
                 <div class="panel-body">
                     <div class="block">
                                
                               
                                 <form class="form-horizontal" method="POST" action="{{ url('updateslider/'.$slider->id) }}"  enctype="multipart/form-data">      
                                {{ csrf_field() }}                             
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Title</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="slider_title" value="{{ $slider->slider_title }}" />
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">SubTitle</label>
                                       
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="slider_subtitle"  value="{{ $slider->slider_subtitle }}"/>
                                        </div>
                                    </div>

                                       <div class="form-group">
                                        <label class="col-md-2 control-label">Cover Image</label>
                                        <div class="col-md-10">
                                             <input id="name" type="file" class="form-control" name="slider_image" value=""  autofocus>
                                        </div>
                                    </div>
            


                                    <div class="form-group">
                                        <label class="col-md-2 control-label"></label>
                                        <div class="col-md-10">
                                            <input type="submit"  class="brn btn-success btn-lg" name="Submit" />
                                        </div>
                                    </div>

                                    
                                </form>
             </div>
                 </div>
             </div>
        </div>
    </div>
@endsection