@extends('layouts.backend')

@section('content') 
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
             <div class="panel panel-success">
                 <div class="panel-heading"><h4>Add Slider Image</h4></div>
                 <div class="panel-body">
                     <div class="block">
                                <h4>Add Form Service</h4>
                               
                                 <form class="form-horizontal" method="POST" action="{{ url('storeslider') }}"  enctype="multipart/form-data">      
                                {{ csrf_field() }}                             
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Title</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="slider_title"/>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">SubTitle</label>
                                       
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="slider_subtitle"/>
                                        </div>
                                    </div>

                                       <div class="form-group">
                                        <label class="col-md-2 control-label">Slider Image</label>
                                        <div class="col-md-10">
                                             <input id="name" type="file" class="form-control" name="slider_image" value="" required autofocus>
                                        </div>
                                    </div>
            


                                    <div class="form-group">
                                        <label class="col-md-2 control-label"></label>
                                        <div class="col-md-10">
                                            <input type="submit"  class="brn btn-success btn-lg" name="Submit" />
                                        </div>
                                    </div>

                                    
                                </form>
             </div>
                 </div>
             </div>
        </div>
    </div>
@endsection