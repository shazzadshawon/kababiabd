@extends('layouts.backend')

@section('content') 
    <div class="">
    <div class="row">
         <div class="col-md-12 panel pull-right">
             <button class="btn btn-info" data-toggle="modal" data-target="#modal_basic"><span class="fa fa-plus"></span> Add new Item</button>
         </div>
    </div>
        <div class="row">
         <div class="modal" id="modal_basic" tabindex="-1" role="dialog" aria-labelledby="defModalHead" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h4 class="modal-title" id="defModalHead"><span class="fa fa-plus"></span> Add Package Item</h4>
                    </div>
                    <div class="modal-body">
                         <form action="{{ url('addpackageitem') }}" class="form-horizontal" method="post" enctype="multipart/form-data"">
        {{ csrf_field() }} 
        <div class="panel panel-success">
          <div class="panel-heading"></div>
          <div class="panel-body">
             <div class="form-group">
                <label class="col-md-3 col-xs-12 control-label">Item</label>
                <div class="col-md-6 col-xs-12">                                            
                    <div class="input-group">
                        <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                        <input type="text" class="form-control" name="item" value="" />
                    </div>                                            
                    {{-- <span class="help-block">This is sample of text field</span> --}}
                </div>
            </div>
            <div class="form-group">
                <label class="col-md-3 col-xs-12 control-label">Category</label>
                <div class="col-md-6 col-xs-12">                                            
                    <select name="cat_id" class="form-control">
                        @foreach ($cats as $c)
                            <option value="{{ $c->id }}">{{ $c->package_title }}</option>
                        @endforeach
                    </select>                                        
                    {{-- <span class="help-block">This is sample of text field</span> --}}
                </div>
            </div>
             <div class="form-group">
                <label class="col-md-3 col-xs-12 control-label"></label>
                <div class="col-md-6 col-xs-12">                                            
                    <input type="submit" name="submit"  value="Update" class="btn btn-primary">                                  
                    {{-- <span class="help-block">This is sample of text field</span> --}}
                </div>
            </div>
            
                                    
          </div>
        </div>
                             </form>      
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        @foreach ($cats as $cat)
             <div class="col-md-6 ">

                 <div class="panel panel-success">
                                <div class="panel-heading  panel-primary">                                
                                    <h3 class="panel-title">{{ $cat->package_title }}</h3>
                                   {{--  <ul class="panel-controls">
                                        <li><a href="#" class="panel-collapse"><span class="fa fa-angle-down"></span></a></li>
                                        <li><a href="#" class="panel-refresh"><span class="fa fa-refresh"></span></a></li>
                                        <li><a href="#" class="panel-remove"><span class="fa fa-times"></span></a></li>
                                    </ul>        --}}                         
                                </div>
                                <div class="panel-body">
                                    <table class="table datatable">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Name</th>
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        @php
                                            $i=1;
                                            $items = DB::table('package_items')->where('cat_id',$cat->id)->get();
                                        @endphp
                                        <tbody>
                                            @foreach($items as $item)
                                            <tr>
                                                <td>{{ $i++ }}</td>
                                                <td>{{ $item->item }}</td>
                                                
                                                <td>
                                                    {{-- <a href="{{ url('editpackagecategory/'.$item->id) }}" class="btn btn-primary">Edit</a> --}}
                                                    <a href="{{ url('deletepackageitem/'.$item->id) }}" class="btn btn-danger">Delete</a>
                                                </td>
                                            </tr>
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
    </div>
            </div>
        @endforeach
           
        </div>
    </div>
@endsection