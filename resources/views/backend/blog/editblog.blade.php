@extends('layouts.backend')

@section('content') 
    <div class="row">
        <div class="col-md-12">
             <div class="panel panel-success">
                 <div class="panel-heading"><h4>Update Offer</h4></div>
                 <div class="panel-body">
                    <div class="block">
                        <div class="row">
                            <div class="col-md-8">
                                   <form class="form-horizontal" method="POST" action="{{ url('updateoffer/'.$blog->id) }}"  enctype="multipart/form-data">      
                                {{ csrf_field() }}                             
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Service Title</label>
                                        <div class="col-md-10">
                                            <input type="text" class="form-control" name="blog_title" value="{{ $blog->blog_title }}" />
                                        </div>
                                    </div>
                                  {{--   <div class="form-group">
                                        <label class="col-md-2 control-label">Category</label>
                                        <div class="col-md-10">
                                            <select class="form-control" name="blog_type_id">
                                            <option value="{{ $blog->blog_type_id }}">{{ $blog->type_name }}</option>
                                               <option value="">Choose one</option>
                                                @foreach ($blogtypes as $sub)
                                                    <option value="{{ $sub->id }}">{{ $sub->type_name }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                    </div> --}}

                                       <div class="form-group">
                                        <label class="col-md-2 control-label">Cover Image</label>
                                        <div class="col-md-10">
                                             <input id="name" type="file" class="form-control" name="name" value=""  autofocus>
                                        </div>
                                    </div>
              
                                                                                
                                    <div class="form-group">
                                        <label class="col-md-2 control-label">Description </label>
                                        <div class="col-md-10">
                                            <div class="">
                                                <textarea  name="editor1">@php
                                                    print_r($blog->blog_description);
                                                @endphp</textarea>
       
                                            </div>
                                        </div>
                                    </div>

                                 


                                    <div class="form-group">
                                        <label class="col-md-2 control-label"></label>
                                        <div class="col-md-10">
                                            <input type="submit"  class="btn btn-success btn-lg" name="Submit" />
                                        </div>
                                    </div>

                                    
                                </form>
                            </div>
                            <div class="col-md-4">
                                  <img id="blah" alt="your image" class="img img-thumbnail" style="width: 300px; height: 300px;" src="{{asset('public/uploads/blog/'.$blog->blog_image)}}" />
                            </div>
                        </div>          
                    </div>
                 </div>
             </div>
        </div>
    </div>
@endsection