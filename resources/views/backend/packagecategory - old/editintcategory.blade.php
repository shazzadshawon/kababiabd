@extends('layouts.backend')

@section('content') 
<div class="">
<div class="row">
    <div class="col-md-8">
    <hr>
    <h3>Edit package Category</h3>
        <form action="{{ url('updatepackagecategory/'.$cat[0]->id) }}" class="form-horizontal" method="post" enctype="multipart/form-data"">
        {{ csrf_field() }}
                                    <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Category Name</label>
                                        <div class="col-md-6 col-xs-12">                                            
                                            <div class="input-group">
                                                <span class="input-group-addon"><span class="fa fa-pencil"></span></span>
                                                <input type="text" class="form-control" name="cat_name" value="{{ $cat[0]->cat_name }}" />
                                            </div>                                            
                                            {{-- <span class="help-block">This is sample of text field</span> --}}
                                        </div>
                                    </div>
                                    
                                  
                                    
                                {{--     <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">Category Type</label>
                                        <div class="col-md-6 col-xs-12">           
                                           @php
                       						$typ =  DB::table('servicetypes')
                       						->where('id',$cat[0]->cat_type)
                       						->get();
                                           @endphp                                                      
                                            <select class="form-control select" name="cat_type">
                                                <option value="{{ $cat[0]->cat_type }}">{{ $typ[0]->type_name}}</option>
                                                @foreach ($types as $type)
                                                    <option value="{{ $type->id }}">{{ $type->type_name }}</option>
                                                @endforeach
                                                
                                                
                                            </select>
                                           
                                        </div>
                                    </div>
                                     --}}
                                   {{--  <div class="form-group">
                                        <label class="col-md-3 col-xs-12 control-label">File</label>
                                        <div class="col-md-6 col-xs-12">                                                                                                                                        
                                            <input type="file" class="fileinput btn-primary" name="filename" id="filename" title="Browse file"/>
                                            <span class="help-block">Input type file</span>
                                        </div>
                                    </div> --}}
                                    
                                  <div class="form-group">
                                       <label class="col-md-3 col-xs-12 control-label"></label>
                                        <div class="col-md-6 col-xs-12">                                                                                            {{-- 
                                           <button class="btn btn-primary">Clear Form</button> --}}
                                           <input type="submit" name="submit" class="btn btn-primary">
                                           {{--  pull-right --}}
                                        </div>
                                    </div>

                        
        </form>
    </div>
</div>
</div>
@endsection