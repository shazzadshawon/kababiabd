@extends('layouts.backend')

@section('content') 
        <div class="row">
            <div class="col-md-10 col-md-offset-1">
                 <div class="panel panel-success">
                    

                     <div class="content-frame">   
                    
                    <!-- START CONTENT FRAME TOP -->
                    <div class="content-frame-top">                        
                        <div class="page-title">                    
                            <h2><span class="fa fa-image"></span> Special Food Items</h2>
                        </div>                                      
                        <div class="pull-right">     
                        <a href="{{ url('addteammember') }}" class="btn btn-info"><span class="fa fa-upload"></span> Add Item</a>                       
                           
                        </div>                         
                    </div>
                    
                   
                    <!-- END CONTENT FRAME RIGHT -->
                
                    <!-- START CONTENT FRAME BODY -->
                    <div class="">
                        
                       
                        <div class="gallery" id="links">
                             {{-- <div class="row"> --}}
                                 @foreach ($teams as $gal)
                                 {{-- <div class="col-md-3"> --}}
                                    <div class="gallery-item"  title="" data-gallery>
                                            <div class="image">                              
                                                <img style="height: 200px" src="{{ asset('public/uploads/team/'.$gal->team_image) }}" alt="{{ $gal->team_image }}"/>                                        
                                                                                                              
                                            </div>
                                            <div class="row" align="center">
                                            <h4>{{ $gal->team_title }}</h4>
                                               {{-- <a class="btn btn-success btn-xs col-md-4" tooltip="test" target="_blank" href="{{ asset('viewteammember'.$gal->id) }}" ><span class="fa fa-eye"></span></a> --}}
                                    <a class="btn btn-primary btn-xs  col-md-6" target="" href="{{ url('editteammember/'.$gal->id) }}" ><span class="fa fa-edit"></span></a>
                                    <a class="btn btn-danger btn-xs  col-md-6" href="{{ url('deleteteammember/'.$gal->id) }}"><span class="fa fa-trash-o"></span></a>
                                            </div>
                                                                      
                                    </div>
                                    
                                     {{-- </div> --}}
                                 @endforeach
                             {{-- </div> --}}
                           

                             
                        </div>
                             
                       {{--  <ul class="pagination pagination-sm pull-right push-down-20 push-up-20">
                            <li class="disabled"><a href="#">«</a></li>
                            <li class="active"><a href="#">1</a></li>
                            <li><a href="#">2</a></li>
                            <li><a href="#">3</a></li>
                            <li><a href="#">4</a></li>                                    
                            <li><a href="#">»</a></li>
                        </ul> --}}
                    </div>       
                    <!-- END CONTENT FRAME BODY -->
                </div>
                               
                </div>
            </div>
        </div>
@endsection