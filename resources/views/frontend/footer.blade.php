<div class="subscribe agileits-w3layouts">
    <div class="container">
        <div class="col-md-3 social-icons w3-agile-icons">
            <h4>Keep in touch</h4>
            <ul>
                <li><a href="https://www.facebook.com/KababiaBD" target="_blank" class="fa fa-facebook icon facebook"> </a></li>
                <li><a href="https://www.twitter.com" target="_blank" class="fa fa-twitter icon twitter"> </a></li>
                <li><a href="https://plus.google.com/discover" target="_blank" class="fa fa-google-plus icon googleplus"> </a></li>
                <li><a href="https://dribbble.com/" target="_blank" class="fa fa-dribbble icon dribbble"> </a></li>
                <li><a href="#" target="_blank" class="fa fa-rss icon rss"> </a></li>
            </ul>
        </div>

        <div class="w3_footer_grids">
            <div class="col-xs-12 col-sm-3 footer-grids w3-agileits">
                <h3>company</h3>
                <ul>
                    <li><a href="{{ url('about') }}">About Us</a></li>
                    <li><a href="{{ url('contact') }}">Contact Us</a></li>
                    <li><a href="{{ url('gallery') }}">Gallery</a></li>
                    <li><a href="{{ url('packages') }}">Packages</a></li>
                </ul>
            </div>
        </div>

        <div class="col-md-6 subscribe-right">
            <h3 class="w3ls-title" style="color:black;">Subscribe to Our <br><span>Newsletter</span></h3>
            <form action="" method="post">
                <input type="email" name="email" placeholder="Enter your Email..." required="">
                <input type="submit" value="Subscribe">
                <div class="clearfix"> </div>
            </form>
            <img src="images/i1.png" class="sub-w3lsimg" alt=""/>
        </div>
        <div class="clearfix"> </div>
    </div>
</div>

<div class="copyw3-agile">
    <div class="container">
        <p>Created with <i class="fa fa-heart" style="color:red" aria-hidden="true"></i> by <a href="http://shobarjonnoweb.com/" target="_blank">Shobarjonnoweb.com</a></p>
    </div>
</div>