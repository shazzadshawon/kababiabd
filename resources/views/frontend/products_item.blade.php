<!--LOOP-->

@for ($i = 0; $i < count($portfolios); $i=$i+5)
    {{-- expr --}}

@if(!empty($portfolios[$i]))
<div class="col-xs-6 col-sm-4 product-grids">
    <div class="flip-container">
        <div class="flipper agile-products">
            <div class="front">
                <img style="height: 170px; width: 100%;" src="{{ asset('public/uploads/portfolio/'. $portfolios[$i]->service_image) }}" class="img-responsive" alt="img">
                <div class="agile-product-text">
                    <h5>{{ $portfolios[$i]->service_title }}</h5>
                    
                </div>
            </div>
            <div class="back">
                <h4>{{ $portfolios[$i]->service_title }}</h4>
                <p>@php
                    print_r(substr($portfolios[$i]->code, 0,20));
                @endphp</p>
                <h6>{{ $portfolios[$i]->price }}<sup>&#2547;</sup></h6>
               
                    <a href="#" data-toggle="modal" data-target="#{{ $portfolios[$i]->id }}">View Details</a>
            </div>
        </div>
    </div>
</div>

@endif

@if(!empty($portfolios[$i+1]))
<div class="col-xs-6 col-sm-4 product-grids">
    <div class="flip-container">
        <div class="flipper agile-products">
            <div class="front">
                <div class="agile-product-text agile-product-text2">
                    <h5>{{ $portfolios[$i+1]->service_title }}</h5>
                   
                </div>
                 <img style="height: 170px; width: 100%;" src="{{ asset('public/uploads/portfolio/'. $portfolios[$i+1]->service_image) }}" class="img-responsive" alt="img">
            </div>
            <div class="back">
               <h4>{{ $portfolios[$i+1]->service_title }}</h4>
                <p>@php
                    print_r(substr($portfolios[$i+1]->code, 0,20));
                @endphp</p>
                <h6>{{ $portfolios[$i+1]->price }}<sup>&#2547;</sup></h6>
                <a href="#" data-toggle="modal" data-target="#{{ $portfolios[$i+1]->id }}">View Details</a>
            </div>
        </div>
    </div>
</div>
@endif

@if(!empty($portfolios[$i+2]))
<div class="col-xs-6 col-sm-4 product-grids">
    <div class="flip-container">
        <div class="flipper agile-products">
            <div class="front">
                 <img style="height: 170px; width: 100%;" src="{{ asset('public/uploads/portfolio/'. $portfolios[$i+2]->service_image) }}" class="img-responsive" alt="img">
                <div class="agile-product-text">
                    <h5>{{ $portfolios[$i+2]->service_title }}</h5>
                    
                </div>
            </div>
            <div class="back">
                  <h4>{{ $portfolios[$i+2]->service_title }}</h4>
                <p>@php
                    print_r(substr($portfolios[$i+2]->code, 0,20));
                @endphp</p>
                <h6>{{ $portfolios[$i+2]->price }}<sup>&#2547;</sup></h6>
               <a href="#" data-toggle="modal" data-target="#{{ $portfolios[$i+2]->id }}">View Details</a>
            </div>
        </div>
    </div>
</div>
@endif

@if(!empty($portfolios[$i+3]))
<div class="col-xs-6 col-sm-6 product-grids">
    <div class="flip-container flip-container1">
        <div class="flipper agile-products">
            <div class="front">
                <div class="agile-product-text agile-product-text2">
                    <h5>{{ $portfolios[$i+3]->service_title }}</h5>
                    
                </div>
                 <img style="height: 270px; width: 100%;" src="{{ asset('public/uploads/portfolio/'. $portfolios[$i+3]->service_image) }}" class="img-responsive" alt="img">
            </div>
            <div class="back">
                 <h4>{{ $portfolios[$i+3]->service_title }}</h4>
                <p>@php
                    print_r(substr($portfolios[$i+3]->code, 0,20));
                @endphp</p>
                <h6>{{ $portfolios[$i+3]->price }}<sup>&#2547;</sup></h6>
               <a href="#" data-toggle="modal" data-target="#{{ $portfolios[$i+3]->id }}">View Details</a>
            </div>
        </div>
    </div>
</div>
@endif

@if(!empty($portfolios[$i+4]))
<div class="col-xs-6 col-sm-6 product-grids">
    <div class="flip-container flip-container1">
        <div class="flipper agile-products">
            <div class="front">
                  <img style="height: 270px; width: 100%;" src="{{ asset('public/uploads/portfolio/'. $portfolios[$i+4]->service_image) }}" class="img-responsive" alt="img">
                <div class="agile-product-text">
                    <h5>{{ $portfolios[$i+4]->service_title }}</h5>
                    
                </div>
            </div>
            <div class="back">
                  <h4>{{ $portfolios[$i+4]->service_title }}</h4>
                <p>@php
                    print_r(substr($portfolios[$i+4]->code, 0,20));
                @endphp</p>
                <h6>{{ $portfolios[$i+4]->price }}<sup>&#2547;</sup></h6>
               <a href="#" data-toggle="modal" data-target="#{{ $portfolios[$i+4]->id }}">View Details</a>
            </div>
        </div>
    </div>
</div>
@endif


@include('frontend.detailModal')
<!--LOOP-->


@endfor

<div class="clearfix"></div>
