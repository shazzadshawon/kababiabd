<div id="hit_counter" class="agileinfo_counter_section total_hit_counter">
    <div class="wthree_title_agile">
        <h3 class="h-t">Visitor<span>Counter</span></h3>
<!--        <p><i class="fa fa-bomb" aria-hidden="true"></i></p>-->

    </div>
    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
        <p class="sub_para two event_soon">Count of Total Visitors</p>
        <div class="wthree-counter-agile">
            <section class="total_visitor">
            @php
                $lifetime = DB::table('hit_counter')->first();
               // $all = \Visitor::all();
                //$today = DB::table('visitor_registry')->whereDate('created_at', DB::raw('CURDATE()'))->count();
                //print_r($today);
            @endphp
                <span id="tva_s" hidden="true">0</span>
                <span id="tva_e" hidden="false">{{ $lifetime->counter }}</span>
                <span id="total_visitor_all" class="simply-amount count"></span>
                <div class="clearfix"></div>
            </section>
        </div>
    </div>
    <div class="col-md-6 col-lg-6 col-sm-6 col-xs-12">
        <p class="sub_para two event_soon">Total Visitors Today</p>
        <div class="wthree-counter-agile">
            <section class="total_visitor_today">
                <span id="tvd_s" hidden="true">0</span>
                <span id="tvd_e" hidden="true">{{ $lifetime->daily_count }}</span>
                <span id="total_visitor_day" class="simply-amount count"></span>
                <div class="clearfix"></div>
            </section>
        </div>
    </div>
    <div class="clearfix"></div>
</div>

<script type="application/javascript">
    function decimalChecker(num){
        if(num >= 1000){
            return 500;
        }
        else if (num >= 100) {
            return 100;
        } else if(num <= 100 && num > 10){
            return 10;
        } else if (num >= 10) {
            return 1;
        }
    }

    var el = $('#hit_counter').offset().top;
    var TVA_s = parseInt($('#tva_s').html());
    var TVA_e = parseInt($('#tva_e').html());

    var TVD_s = parseInt($('#tvd_s').html());
    var TVD_e = parseInt($('#tvd_e').html());

    var tva_step = decimalChecker(TVA_e);
    var tve_step = decimalChecker(TVD_e);

    var lastScrollTop = 0;
    $(window).scroll(function(event) {
        var st = $(this).scrollTop();
        var y = window.pageYOffset;
        var pos = el - 945;

//        console.log("y = "+y+"  pos = "+pos);
        $('#total_visitor_all').animationCounter({
            start: TVA_s,
            end: TVA_e,
            step: tva_step,
            delay: 50
        });
        $('#total_visitor_day').animationCounter({
            start: TVD_s,
            end: TVD_e,
            step: tve_step,
            delay: 50
        });
    });
</script>