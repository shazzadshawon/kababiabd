

@extends('layouts.frontend')

@section('content')
{{-- w3ls_banner_section second --}}
<div class="">
	  <img style="height: 250px; width: 100%;" src="{{ asset('public/images/inner_bg.jpg') }}">
   </div>
	<!-- banner-slider -->
	<!-- breadcrumbs -->
	<div class="w3l_agileits_breadcrumbs">
		<div class="container">
			<ul>
				<li><a href="{{ url('/') }}">Home</a><span>«</span></li>
				<li>Packages</li>
			</ul>
		</div>
	</div>
<!-- //breadcrumbs -->

		<!--/story-->
        
        <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="speach">
                    <div class="resp-tabs-container">
                        <div class="tab1">
                            <div class="wthree_agile_tabs">
                                <div class="col-md-12">
                                    <div class="tab-info_text_agile_w3l">
                                        <h3>International Package Tours</h3>

                                        <p>Red Elegance is a tour operator in Bangladesh. It is widely recognized to arrange all kinds of national and international TOUR PACKAGES,  international and domestic AIR TICKET and VISA support are free from hassle & full of entertainment. Indulge yourself with experience about tourism in home & abroad.</p>
                                        <p>We are providing online personalized and customized tour package to ensure the best services. We have established our own set up in different countries like Bhutan, India, Nepal, Thailand, Malaysia, Singapore, China, Dubai and Expanding.</p>
                                        <p>Red Elegance offers exciting international tour packages from Bangladesh. It is good news for the travelers that we arrange package tour within their budget. Our clients can also customize their tour packages however they want to enjoy their holidays. We are successful only when our clients become satisfied with our arrangements.</p>
                                        
                                    </div>
                                </div>
                                <div class="clearfix"></div>	
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    
    
		<div class="w3l_inner_section">
			<div class="container">
			    <div class="wthree_title_agile">
			        <h2>Packages</h2>
					<!-- <p><i class="fa fa-heart-o" aria-hidden="true"></i></p> -->
					
				</div>

				<div class="inner_w3l_agile_grids">
				@foreach($packages as $package)
				  	<div class="col-md-4 w3_tabs_grid">
						<div class="grid">
							<a href="{{url('package/'.$package->id)}}" class="lsb-preview" data-lsb-group="header">
								<h1>
                                    <span>{{ $package->package_name }}</span><br>
                                    <small><h3>{{$package->package_subtitle}}</h3></small>
                                </h1>
							</a>
						</div>
					</div>
				@endforeach
                  
                   
                    
                    
                  
					<div class="clearfix"> </div>
				</div>
			</div>
					
  
    	</div>
		
		<div class="container">
			<div class="inner_w3l_agile_grids">
			<div class="row">
				<div class="col-md-12">
					<div class="speach">
						<div class="resp-tabs-container">
							<div class="tab1">
								<div class="wthree_agile_tabs">
									<div class="col-md-6">
										<div class="tab_info_img_agile_w3l">
											 <img src="{{asset('public/images/corporate_speach.png')}}" class="img-responsive" alt=""/>
										</div>
									</div>
									<div class="col-md-6">
										<div class="tab-info_text_agile_w3l">
											<h3>Dirrector's Word </h3>
											
											<p>Lorem Duis aute irure dolor reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur malesuada Aliquam luctus, nibh eleifend lacinia gravida Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet, consectetur lacinia consectetur lacinia</p>
										</div>
									</div>
									<div class="clearfix"></div>	
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
<!--//story-->
<!--//counter-->

@endsection