<div class="w3ls_banner_section">
    <div class="snow-container">
        <div class="snow foreground"></div>
        <div class="snow foreground layered"></div>
        <div class="snow middleground"></div>
        <div class="snow middleground layered"></div>
        <div class="snow background"></div>
        <div class="snow background layered"></div>
    </div>
    @php
        $sliders = DB::table('sliders')->get();
        //print_r($sliders[0]->id);
    @endphp
    <div class="callbacks_container">
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
             <ol class="carousel-indicators">
              @foreach($sliders as $slider)
                <li data-target="#myCarousel" data-slide-to="{{$slider->id}}" class=""></li>
              @endforeach
  </ol>
            <!-- Wrapper for slides -->
             <div class="carousel-inner">
             @php
                 $x=0;
             @endphp
                @foreach($sliders as $slider)
                <div class="item @if ($x==0) active @endif">
                  <img style="height: 600px" src="{{asset('public/uploads/slider/'.$slider->slider_image)}}" alt="Los Angeles">
                  <div class="carousel-caption banner_agile-info">
                    <h3>{{ $slider->slider_title }}</h3>
                    <p>{{ $slider->slider_subtitle }}</p>
                  </div>
                </div>
                @php
                    $x++;
                @endphp
                @endforeach
            <!-- Left and right controls -->
            <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                <span class="glyphicon glyphicon-chevron-left"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="right carousel-control" href="#myCarousel" data-slide="next">
                <span class="glyphicon glyphicon-chevron-right"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <div class="clearfix"></div>
    </div>
</div>