@extends('layouts.frontend')

@section('content')
    <div class="">
      <img style="height: 250px; width: 100%;" src="{{ asset('public/images/inner_bg.jpg') }}">
   </div>
    <!-- banner-slider -->
    <!-- breadcrumbs -->



    <div class="w3l_agileits_breadcrumbs">
        <div class="container">
            <ul>
                <li><a href="{{url('/')}}">Home</a><span>«</span></li>
               {{--  <li><a href="{{url('/allwedding')}}">Wedding</a><span>«</span></li> --}}
                <li>Wedding<span>«</span></li>
            
                <li>{{ $service->service_title }}</li>
            </ul>
        </div>
    </div>
<!-- //breadcrumbs -->
    
    <!--/story-->
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="speach">
                    <div class="resp-tabs-container">
                        <div class="tab1">
                            <div class="wthree_agile_tabs">
                                <div class="col-md-6">
                                    <div class="tab_info_img_agile_w3l">
                                         <img src="{{asset('public/uploads/service/'.$service->service_image)}}" class="img-responsive" alt=""/>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="tab-info_text_agile_w3l">
                                        <h3>{{ $service->service_title }}</h3>

                                        <?php print_r($service->service_description); ?>
                                    </div>

                                </div>
                                <div class="clearfix"></div>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>  
<!--//story-->
        
    <div class="gap"></div>

@endsection