@php
    $reviewsObj = DB::table('reviews')->get();
      $reviews = array();

        foreach ($reviewsObj as $obj) {
            $reviews[] = (array)$obj;
        }
@endphp

<div id="review" class="wthree-news text-center">
    <div class="container">
          <div class="wthree_title_agile">
                    <h3><span>Reviews</span></h3>
                <p><i class="fa fa-heart-o" aria-hidden="true"></i></p>
                
              </div>
             <p class="sub_para">OUR CLIENT REVIEWS</p>
             <div class="inner_w3l_agile_grids">
          <div class="col-md-6 wthree-right">
            <div class="w3-agile-left">
              <div class="work-left">
                <h4><?php print_r($reviews[0]['created_at']); ?></h4>
              </div>
              <div class="agileits-right text-left">
                <a href="single.html"><?php print_r($reviews[0]['review_title']); ?></a>
                <p><?php print_r($reviews[0]['review_description']); ?></p>
              </div>
              <div class="clearfix"></div>
            </div>
            <div class="wel-right">
              <div class="content-item item-image1">
                 <a href=""><img style="width: 350px" src="{{asset('public/uploads/review/'.$reviews[0]['review_image'])}}" alt="" /></a>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="col-md-6 wthree-right no-marg">
            <div class="wel-right2">
              <div class="content-item item-image2">
                 <a href=""><img style="width: 350px" src="{{asset('public/uploads/review/'.$reviews[1]['review_image'])}}" alt="" /></a>
              </div>
            </div>
            <div class="w3-agile-left2">
              <div class="work-left">
                <h4><?php print_r($reviews[1]['created_at']); ?></h4>
              </div>
              <div class="agileits-right text-left">
                  <a href="single.html"><?php print_r($reviews[1]['review_title']); ?></a>
                <p><?php print_r($reviews[1]['review_description']); ?></p>
              </div>
              <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="clearfix"></div>
          <div class="col-md-6 wthree-right">
            <div class="w3-agile-left">
              <div class="work-left">
                <h4><?php print_r($reviews[2]['created_at']); ?></h4>
              </div>
              <div class="agileits-right text-left">
                  <a href="single.html"><?php print_r($reviews[2]['review_title']); ?></a>
                <p><?php print_r($reviews[2]['review_description']); ?></p>
              </div>
              <div class="clearfix"></div>
            </div>
            <div class="wel-right ">
              <div class="content-item item-image1">
                 <a href=""><img style="width: 350px" src="{{asset('public/uploads/review/'.$reviews[2]['review_image'])}}" alt="" /></a>
              </div>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="col-md-6 wthree-right no-marg">
            <div class="wel-right2">
              <div class="content-item item-image2">
                <a href=""><img style="width: 350px" src="{{asset('public/uploads/review/'.$reviews[3]['review_image'])}}" alt="" /></a>
              </div>
            </div>
            <div class="w3-agile-left2">
              <div class="work-left">
                <h4><?php print_r($reviews[3]['created_at']); ?></h4>
              </div>
              <div class="agileits-right text-left">
                   <a href="single.html"><?php print_r($reviews[3]['review_title']); ?></a>
                <p><?php print_r($reviews[3]['review_description']); ?></p>
              </div>
              <div class="clearfix"></div>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="clearfix"></div>
        </div>
      </div>
</div>