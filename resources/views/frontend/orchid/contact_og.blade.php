




@extends('layouts.frontend')

@section('content')

	<!-- banner-slider -->
	<div class="">
      <img style="height: 250px; width: 100%;" src="{{ asset('public/images/inner_bg.jpg') }}">
   	</div>
	<!-- banner-slider -->
	<!-- breadcrumbs -->
	<div class="w3l_agileits_breadcrumbs">
		<div class="container">
			<ul>
				<li><a href="{{url('/')}}">Home</a><span>«</span></li>
				<li>Contact</li>
			</ul>
		</div>
	</div>
	<!-- //breadcrumbs -->

			<!--/story-->
				<div class="w3l_inner_section">
					<div class="container">
						   <div class="wthree_title_agile">
						        <h3>Contact <span>Us</span></h3>
								<p><i class="fa fa-map-marker" aria-hidden="true"></i></p>
								
							</div>
						 <p class="sub_para">FIND OUR LOCATION</p>
						 <div class="inner_w3l_agile_grids">
						       <div class="w3ls_footer_grid">
									<div class="col-md-4 w3ls_footer_grid_left">
										<div class="w3ls_footer_grid_leftl">
											<i class="fa fa-map-marker" aria-hidden="true"></i>
										</div>
										<div class="w3ls_footer_grid_leftr">
											<h4>Location</h4>
											<p>House No. CB-05, Road No. 09, Garishon, Dhaka Cantonment</p>
										</div>
										<div class="clearfix"> </div>
									</div>
									<div class="col-md-4 w3ls_footer_grid_left">
										<div class="w3ls_footer_grid_leftl">
											<i class="fa fa-envelope" aria-hidden="true"></i>
										</div>
										<div class="w3ls_footer_grid_leftr">
											<h4>Email</h4>
											<a href="mailto:info@example.com">admin@orchidgarden.com</a>
										</div>
										<div class="clearfix"> </div>
									</div>
									<div class="col-md-4 w3ls_footer_grid_left">
										<div class="w3ls_footer_grid_leftl">
											<i class="fa fa-phone" aria-hidden="true"></i>
										</div>
										<div class="w3ls_footer_grid_leftr">
											<h4>Call Us</h4>
                                            <p>Md Nazmul Islam</p>
											<p>+8801712517349</p>
											<p>+8801882116777</p>
										</div>
										<div class="clearfix"> </div>
									</div>
									<div class="clearfix"> </div>
								</div>
                               	<div class="w3_mail_grids">
								<form action="{{url('storecontact_og')}}" method="post">
								{{ csrf_field() }}
									<div class="col-md-6 w3_agile_mail_grid">
											<input type="text" placeholder="Your Name"  required="" name="contact_title">
											<input type="email" placeholder="Your Email" required="" name="contact_email">
											<input type="text" placeholder="Your Phone Number" required="" name="contact_phone">
											


										
									</div>
									<div class="col-md-6 w3_agile_mail_grid">
										<textarea name="Message" placeholder="Your Message" required="" name="contact_description"></textarea>
										<input type="submit" value="Submit">
									</div>
									<div class="clearfix"> </div>
								</form>
							</div>
					   </div>
							<div class="map">
                                <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d3650.4551015044253!2d90.3949456!3d23.8024106!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3755c718f4e34671%3A0xda05c3a954dc624!2z4KaX4KeN4Kav4Ka-4Kaw4Ka_4Ka44KaoIOCmrOCmvuCmuCDgprjgp43gpp_gpqo!5e0!3m2!1sbn!2sbd!4v1500803286590" width="600" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
							</div>
						</div>
					</div>
					

			<!--//story-->


@endsection