@extends('layouts.frontend')

@section('content')

	<!-- banner-slider -->
    
{{-- w3ls_banner_section second --}}
<div class="">
      <img style="height: 250px; width: 100%;" src="{{ asset('public/images/inner_bg.jpg') }}">
   </div>
	<!-- breadcrumbs -->
	<div class="w3l_agileits_breadcrumbs">
		<div class="container">
			<ul>
				<li><a href="{{url('/')}}">Home</a><span>«</span></li>
				<li>About Us</li>
			</ul>
		</div>
	</div>
<!-- //breadcrumbs -->

    <div class="w3l_inner_section about">
        <div class="container">
               <div class="wthree_title_agile">
                    <h2>Our <span>Story</span></h2>
                </div>

            <div class="col-md-12">
                <div class="tab-info_text_agile_w3l">
                    <p>Orchid Garden is widely leading & recognized as a wholesome, complete events solution provider. It’s a team of young, creative, enthusiastic and dynamic professionals with a sparkling stream of ideas having vast experience in the field of Events & Entertainments. Our goal is to give each & every event a different Meaning, Identity and a Vision with true professionalism to chart the roads of informative & entertaining events.</p>
                    <p>Our Philosophy & Commitment is encapsulated in the following statement:
                        “To provide & deliver event solutions & services with the highest standard of professionalism, creativity upholding at all times quality, integrity and innovation.”
                    </p>
                    <p>Our expertise covers all the aspects of Events: Planning, Management & Coordination and Execution. We deliver cost effective events without compromising on the final output.</p>
                </div>
            </div>
        </div>

        <div class="gap"></div>
        <div class="container mission">
               <div class="wthree_title_agile">
                    <h2>Our <span>Mission</span></h2>
                </div>

            <div class="col-md-12">
                <div class="tab-info_text_agile_w3l">
                    <div class="list-group" style="text-align: center">
                        <a href="#" class="list-group-item">To provide complete services (Dhaka cantoment)</a>
                        <a href="#" class="list-group-item">To provide standard & quality</a>
                        <a href="#" class="list-group-item">To provide fantastic events</a>
                        <a href="#" class="list-group-item">Satisfaction of our clients</a>
                    </div>
                </div>
            </div>
        </div>

        <div class="gap"></div>
        
        <div class="container vision">
               <div class="wthree_title_agile">
                    <h2>Our <span>Vision</span></h2>
                </div>

            <div class="col-md-12">
                <div class="tab-info_text_agile_w3l">
                    
                    <div class="well">
                        <strong>
                            Holding ourself to the highest of personal and business ethics and standards, and by continually challenging normal perceptions of excellence and creativity, we will meet, then exceed, your needs, desires and expectations.
                        </strong>
                    </div>
                    
                    <div class="well">
                        <strong>
                            You + Us = Successful Event.
                        </strong>
                    </div>
                    
                    <div class="well">
                        <strong>In continuous partnership with you throughout all phases leading to your event, by bringing 20 years of experience to your planning table for expert guidance, and in providing spectacular weddings , corporate, entertainment and musical services, will culminate into a one-of-a-kind event life long memory.</strong>
                    </div>
                </div>
            </div>
        </div>

        <div class="gap"></div>
        <div class="container value">
               <div class="wthree_title_agile">
                    <h2>Our <span>Awards</span></h2>
                </div>

            <div class="col-md-12">
                <div class="tab-info_text_agile_w3l">
                    
                    <p>Event design, consulting, planning for those desiring their spacial day to be beyond the ordinary</p>
                    
                    <div class="panel panel-default">
                        <div class="panel-heading">DOIROTH</div>
                        <div class="panel-body">
                            In Recognition
                            of the Contribution
                            to
                            DOIROTH
                            A Duet Art Exhibition
                            20-25 March 2015

                        </div>
                    </div>
                    

                </div>
            </div>
        </div>

        <div class="gap"></div>
        
        <div class="container weDoCon">
               <div class="wthree_title_agile">
                    <h2>Where <span>We ENLISTED </span></h2>
                </div>

            <div class="col-md-12">
                <div class="tab-info_text_agile_w3l">
                    
                    <p>Party Planner, Wedding Planner, Decorations Balloons, Invitations, Favors and so much more. Our events are the talk of the town; well after it's over. We do all the work for you! We have a long list of sastisified customers to prove that "we know our stuff"</p>
                    
                    <p>Our Enlisted Organization :</p>
                    
                    <div class="list-group weDo">
                        @foreach ($abouts as $about)
                            <a href="#" class="list-group-item">{{ $about->about_description }}</a>
                        @endforeach
                      
                    </div>
                </div>
            </div>
         </div>
        
      </div>
<!--//story-->

<!-- /property-grids -->
<div class="property-grids">
    <div class="agile-homes-w3l  grid">
        <div class="col-md-12 home-agile-text">
            <h4>Message</h4>
            <p>“I would like to thank you for going through the company profile of our company orchid garden & EventManagement and Solutions Here you will find a wealth of information about our business and services. </p>
            <p>Founded in 2004.Our company specializes in organizing high quality Bangladeshi events. And offering a wide range of event services and solutions.</p>
            <p>Our teams of skilled professionals have an established track record of successfully organizing events of the highest caliber and we are proud to be an active member of the Bangladeshi Association of Exhibition and Events </p>
            <p>At orchid gardenwe put our customers and partners at the middle of our attention. We deploy professionalism and creativity to contribute to the success of their business and to gain their loyalty in a long term relationship of mutual trust. Our teams combine expertise, motivation and training with the aim to satisfy our partner’s needs and to engage in a continuous improvement process. </p>
            <p>It is our strong belief that the success of any company lies in its capability to acquire dedicated professionals who work hand in hand. We are doing this by offering orchid garden employees a climate of responsibility, appreciation and respect, enabling everyone to develop to their full potential. ”</p>
            <div class="clearfix"></div>
            <div class="date">
               <h4>Md Nazmul Islam</h4>
               <h5>Founder and Chairman</h5>
               <h5>Orchid Garden</h5>
            </div>
        </div>  
        <div class="clearfix"></div>    
     </div>
</div>
<!-- //property-grids -->

      <!-- /Events-->
            <div class="wthree-news text-center">
                <div class="container">
                    <div class="wthree_title_agile">
                                <h3>Our <span>Logistics</span></h3>
                                
                            </div>                       
                         <div class="inner_w3l_agile_grids spa-agile">

                            <div class="col-md-3 spa-grid">
                                
                                    <i class="fa fa-paint-brush" aria-hidden="true"></i>
                                
                                    <h4>Decorator and Flower</h4>
                                    <p>Name</p>
                                
                            </div>
                            <div class="col-md-3 spa-grid">
                                <i class="fa fa-music" aria-hidden="true"></i>
                                    <h4>Sound and Lighting</h4>
                                    <p>Name</p>
                            </div>
                            <div class="col-md-3 spa-grid lost">
                                
                                    <i class="fa fa-camera" aria-hidden="true"></i>
                                
                                    <h4>Photography partners</h4>
                                    <p>Name</p>
                                
                            </div>
                            <div class="col-md-3 spa-grid lost">
                                
                                <i class="fa fa-birthday-cake" aria-hidden="true"></i>
                                
                                
                                    <h4>Balloons and palki and Tom Tom</h4>
                                    <p>Name</p>
                             
                            </div>
                            <div class="clearfix"> </div>
                         </div>
                    </div>
            </div>
       <!-- //Events-->

@endsection