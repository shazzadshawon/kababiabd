@extends('layouts.frontend')


@section('content')
<!-- banner-slider -->
<div class="">
      <img style="height: 250px; width: 100%;" src="{{ asset('public/images/inner_bg.jpg') }}">
   </div>

	<!-- banner-slider -->
	<!-- breadcrumbs -->
	<div class="w3l_agileits_breadcrumbs">
		<div class="container">
			<ul>
				<li><a href="{{url('/')}}">Home</a><span>«</span></li>
				<li>{{$catname}}</li>
			</ul>
		</div>
	</div>
<!-- //breadcrumbs -->

		<!--/story-->

		<div class="w3l_inner_section">
			<div class="container">
			    <div class="wthree_title_agile">
			        <h2>{{$catname}}</h2>
					<!-- <p><i class="fa fa-heart-o" aria-hidden="true"></i></p> -->
					
				</div>

				<div class="inner_w3l_agile_grids">
				@foreach($ints as $ser)
				  	<div class="col-md-4 w3_tabs_grid">
						<div class="grid">
							<a href="{{url('int/'.$ser->id)}}" class="lsb-preview" data-lsb-group="header">
								<h1><span>{{$ser->service_title}}</span></h1>
							</a>
						</div>
					</div>
				@endforeach
					
					<div class="clearfix"> </div>
				</div>
			</div>
					
  
    	</div>
		
		<div class="container">
			<div class="inner_w3l_agile_grids">
			<div class="row">
				<div class="col-md-12">
					<div class="speach">
						<div class="resp-tabs-container">
							<div class="tab1">
								<div class="wthree_agile_tabs">
									<div class="col-md-6">
										<div class="tab_info_img_agile_w3l">
											 <img src="{{asset('public/images/corporate_speach.png')}}" class="img-responsive" alt=""/>
										</div>
									</div>
									<div class="col-md-6">
										<div class="tab-info_text_agile_w3l">
											<h3>Dirrector's Word </h3>
											
											<p>Lorem Duis aute irure dolor reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur malesuada Aliquam luctus, nibh eleifend lacinia gravida Lorem ipsum dolor sit amet,Lorem ipsum dolor sit amet, consectetur lacinia consectetur lacinia</p>
										</div>
									</div>
									<div class="clearfix"></div>	
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

	</div>
<!--//story-->
<!--//counter-->
@endsection