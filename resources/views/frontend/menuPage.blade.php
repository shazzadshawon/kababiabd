<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Menu - Kababia</title>
        <?php require('head.php'); ?>
    </head>
    <body>
        <!-- banner -->
        <div class="banner about-w3bnr" style="background: url(images/menuBanner.jpg) no-repeat center;">

            <!-- header -->
            <?php require('header.php'); ?>
            <!-- //header-end -->

            <!-- banner-text -->
            <div class="banner-text">
                <div class="container">
                    <h2><span>You Tried The Rest</span><br> Now Try The Best</h2>
                </div>
            </div>
        </div>
        <!-- //banner -->
        <!-- breadcrumb -->
        <div class="container">
            <ol class="breadcrumb w3l-crumbs">
                <li><a href="index.php"><i class="fa fa-home"></i> Home</a></li>
                <li class="active">Menu Category Name</li>
            </ol>
        </div>
        <!-- //breadcrumb -->
        <!-- menu list -->
        <div class="wthree-menu">
            <img src="images/i2.jpg" class="w3order-img" alt=""/>
            <div class="container">
                <h3 class="w3ls-title">Menu Category Name</h3>
                <p class="w3lsorder-text">Here are the Sub Category list under this Category</p>
                <div class="menu-agileinfo">
                    <div class="col-md-4 col-sm-4 col-xs-6 menu-w3lsgrids">
                        <a href="products.php"> Breakfast</a>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6 menu-w3lsgrids">
                        <a href="products.php"> Salads</a>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6 menu-w3lsgrids">
                        <a href="products.php"> Hot Food</a>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6 menu-w3lsgrids">
                        <a href="products.php"> Breads</a>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6 menu-w3lsgrids">
                        <a href="products.php"> Deli Pots & Little Pots</a>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6 menu-w3lsgrids">
                        <a href="products.php"> Snacks & Treats</a>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6 menu-w3lsgrids">
                        <a href="products.php"> Drinks</a>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6 menu-w3lsgrids">
                        <a href="products.php"> South Indian</a>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6 menu-w3lsgrids">
                        <a href="products.php"> Catering</a>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6 menu-w3lsgrids">
                        <a href="products.php"> Soups</a>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6 menu-w3lsgrids">
                        <a href="products.php"> Lunchbox</a>
                    </div>
                    <div class="col-md-4 col-sm-4 col-xs-6 menu-w3lsgrids">
                        <a href="products.php"> Dinner</a>
                    </div>
                    <div class="clearfix"> </div>
                </div>

            </div>
        </div>
        <!-- //menu list -->

        <!-- Offers -->
        <?php require('offers.php'); ?>
        <!-- //Offers -->

        <!--Footer-->
        <?php require('footer.php'); ?>
        <!-- //footer -->

        <?php require('foot.php'); ?>
    </body>
</html>