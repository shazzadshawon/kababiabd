@extends('layouts.frontend')

@section('content')

  <!-- banner -->
        <div class="banner about-w3bnr" style="background: url({{ asset('images/HarialiKebab.jpg') }}) no-repeat right;">

            <!-- header -->
            @include('frontend.header')
            <!-- //header-end -->

            <!-- banner-text -->
            <div class="banner-text">
                <div class="container">
                    <h2>You Tried the Best<br> <span>now try the best</span></h2>
                </div>
            </div>
        </div>

        <div class="container">
            <ol class="breadcrumb w3l-crumbs">
                <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Home</a></li>
               
                <li class="active">Gallery</li>
            </ol>
        </div>
        <!-- //breadcrumb -->

        <!-- products -->
        <div class="products">
            <div class="container">
                <div class="col-md-12">
                    <div class="product-top">
                        <h4>Gallery</h4>
                    
                      
                      
                        <div class="clearfix"> </div>
                    </div>
                    <div class="products-row">


                      


@for ($i = 0; $i < count($portfolios); $i=$i+5)
    {{-- expr --}}

@if(!empty($portfolios[$i]))
 <a href="#" data-toggle="modal" data-target="#{{ $portfolios[$i]->id }}">
<div class="col-xs-6 col-sm-4 product-grids">
    <div class="flip-container">
        <div class="flipper agile-products">
            <div class="front">
                <img   style="height: 100%; width: 100%;"  src="{{ asset('public/uploads/gallery/'. $portfolios[$i]->gallery_image) }}" class="img-responsive" alt="img">
               
            </div>
            <div class="back">
                   


                    
            </div>
        </div>
    </div>
</div>
</a>
@endif

@if(!empty($portfolios[$i+1]))
<a href="#" data-toggle="modal" data-target="#{{ $portfolios[$i+1]->id }}">
<div class="col-xs-6 col-sm-4 product-grids">
    <div class="flip-container">
        <div class="flipper agile-products">
            <div class="front">
                
                 <img   style="height: 100%; width: 100%;"  src="{{ asset('public/uploads/gallery/'. $portfolios[$i+1]->gallery_image) }}" class="img-responsive" alt="img">
            </div>
            <div class="back">
             
                
            </div>
        </div>
    </div>
</div>
</a>
@endif

@if(!empty($portfolios[$i+2]))
<a href="#" data-toggle="modal" data-target="#{{ $portfolios[$i+2]->id }}">
<div class="col-xs-6 col-sm-4 product-grids">
    <div class="flip-container">
        <div class="flipper agile-products">
            <div class="front">
                 <img  style="height: 100%; width: 100%;" src="{{ asset('public/uploads/gallery/'. $portfolios[$i+2]->gallery_image) }}" class="img-responsive" alt="img">
              
            </div>
            <div class="back">

            </div>
        </div>
    </div>
</div>
</a>
@endif

@if(!empty($portfolios[$i+3]))
<a href="#" data-toggle="modal" data-target="#{{ $portfolios[$i+3]->id }}">
<div class="col-xs-6 col-sm-6 product-grids">
    <div class="flip-container flip-container1">
        <div class="flipper agile-products">
            <div class="front">
                
                 <img  style="height: 100%; width: 100%;" src="{{ asset('public/uploads/gallery/'. $portfolios[$i+3]->gallery_image) }}" class="img-responsive" alt="img">
            </div>
            <div class="back">
                
              
            </div>
        </div>
    </div>
</div>
</a>
@endif

@if(!empty($portfolios[$i+4]))
<a href="#" data-toggle="modal" data-target="#{{ $portfolios[$i+4]->id }}">
<div class="col-xs-6 col-sm-6 product-grids">
    <div class="flip-container flip-container1">
        <div class="flipper agile-products">
            <div class="front">
                 <img  style="height: 100%; width: 100%;" src="{{ asset('public/uploads/gallery/'. $portfolios[$i+4]->gallery_image) }}" class="img-responsive" alt="img">
                
            </div>
            <div class="back">
                
               
            </div>
        </div>
    </div>
</div>
</a>
@endif


@include('frontend.galleryModal')
<!--LOOP-->


@endfor

<div class="clearfix"></div>







                    </div>
                </div>
             
                <div class="clearfix"> </div>
            </div>
        </div>
        <!-- //products -->
