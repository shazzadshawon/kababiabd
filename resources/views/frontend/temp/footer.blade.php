<footer id="footer">
    <div class="primary-footer">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                    <div class="footer-logo">
                        <figure>
                            <img src="{{ asset('public/assets/images/footer-img.png') }}" alt="">
                        </figure>
                        <h4>Shadi<span></span>Mubarak</h4>
                        <span class="oct-class">Event Management</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="main-footer">
        <div class="container">
            <div class="row">
                <div class="left-footer">
                    <span class="psd-temp-class">Crafted with <i class="fa fa-heart"> </i> by <a class="macro-class" href="http://shobarjonnoweb.com/">Shobarjonnoweb.com</a></span>
                </div>
                <div class="right-footer">
                    <a href="https://www.facebook.com/shadimubarak.bd01/" class="circle-class" target="_blank"><i class="fa fa-facebook"> </i></a>
                    <a href="#" class="circle-class"><i class="fa fa-twitter"> </i></a>
                    <a href="#" class="circle-class"><i class="fa fa-instagram"> </i></a>
                    <a href="#" class="circle-class"><i class="fa fa-pinterest"> </i></a>
                </div>
            </div>
        </div>
    </div>
</footer>