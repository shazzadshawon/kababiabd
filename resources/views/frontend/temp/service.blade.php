﻿@extends('layouts.frontend')

@section('content')


			<!--banner Section start-->
            <div class="parallax-window" data-parallax="scroll" data-image-src="{{ asset('public/assets/images/blog-banner.jpg') }}">
                <section class="banner">
                    <div class="banner-content-wrapper">
                        <h1>Services</h1>
                    </div>
                </section>
            </div>
			<!--banner Section End-->

			<!--Content Area Start-->
			<div id="content">
				<!-- about us section start here -->

				<!-- about us section end here -->

				<!-- about us section start here -->
				<section class="event-content-section">
					<div class="container">
						<div class="row clearfix">
							<div class="header-center">
								<h2>Services</h2>
							</div>
						</div>

							@php
								$i=0;
							@endphp

							@foreach ($services as $service)
								<div class="row" id="{{ $service->id }}">
									
								@if ($i%2 == 0)
							<div class="col-xs-12 col-sm-6">

								<div class="left-part">
									<div class="left-part-header">
										<h3><a href="#">{{ $service->service_title }}</a></h3>
									</div>
									<p>
										@php
											print_r($service->service_description);
										@endphp
									</p>
									
								</div>
							</div>
							<div class="col-xs-12 col-sm-6">
								<div class="right-part">
									<figure>
										<img src=" {{ asset('public/uploads/service/'.$service->service_image) }}" alt="#">
									</figure>

								</div>
							</div>
								@else

							<div class="col-xs-12 col-sm-6">
								<div class="right-part">
									<figure>
										<img src=" {{ asset('public/uploads/service/'.$service->service_image) }}" alt="#">
									</figure>

								</div>
							</div>
							<div class="col-xs-12 col-sm-6">

								<div class="left-part">
									<div class="left-part-header">
										<h3><a href="#">{{ $service->service_title }}</a></h3>
									</div>
									<p>
										@php
											print_r($service->service_description);
										@endphp
									</p>
									
								</div>
							</div>
							
								@endif
								</div>


								@php
									$i++;
								@endphp

								<br><br><br><br><br>
							@endforeach

						


						
					</div>
				</section>
				<!-- about us section end here -->

			</div>
			<!--Content Area End-->
@endsection