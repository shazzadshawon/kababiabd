<div class="header-center">
    <h2>Gallery</h2>
</div>
<div class="tabbing-wrapper">
    <button title="Tooltip on left" data-placement="left" data-toggle="tooltip" class="btn btn-default" data-filter="all" type="button">
        ALL
    </button>
    <button title="Tooltip on top" data-placement="top" data-toggle="tooltip" class="btn btn-default" data-filter="wedding" type="button">
        WEDDING
    </button>
    <button title="Tooltip on bottom" data-placement="bottom" data-toggle="tooltip" class="btn btn-default" data-filter="music" type="button">
        MUSIC
    </button>
    <button title="Tooltip on right" data-placement="right" data-toggle="tooltip" class="btn btn-default" data-filter="event" type="button">
        EVENT
    </button>
</div>

<div class="gallery-blog">
    <ul class="gallery-img-sec clearfix">

        @foreach ($galleries as $gallery)
            <li class="main-item all {{ $gallery->folder }}">
                <a class="fancybox-button" data-fancybox-group="fancybox-button" href="{{ asset('public/uploads/gallery/'.$gallery->gallery_image) }}" title=""> <img style="height: 250px" alt="" src="{{ asset('public/uploads/gallery/'.$gallery->gallery_image) }}"> </a>
            </li>
        @endforeach
       
       
    </ul>
    <button title="Tooltip on bottom" data-placement="bottom" data-toggle="tooltip" class="btn btn-default" type="button" onclick="window.location.href='{{ url('gallery') }}'">
        LOAD MORE
    </button>
</div>