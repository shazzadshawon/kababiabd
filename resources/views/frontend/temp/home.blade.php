@extends('layouts.frontend')

@section('content')



            <!--Banner Section start-->
            <section class="banner">
                @include('frontend.slider')
            </section>
            <!--banner Section End-->

            <!--Content Area Start-->
            <div id="content">
                <!-- happy couple section start here -->
                <section class="happy-couple" id="couple">
                    <div class="container">
                        <div class="col-md-12">
                            <div class="header-center">
                                <h2>Message</h2>
                               
                            </div>
                            <p class="message_pic"><img class="img-responsive" src=" {{ asset('public/assets/images/rezaul_karim.jpg') }}"></p>
                            <div>
                                <h3 class="message_head" style="font-weight: 800;">Md. Rezaul Karim Bhuyan</h3>
                                <p>“Shadi Mubarak” helps you to celebrate wedding in a special and unique way while taking the load off your shoulders. We ensure that everything happens exactly the way you want it. Whether you want the entire wedding to be planned and arranged or a specific event managed, you will find our services truly affordable and well worth.</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="bell">
                                    <div class="inner-header-wrapper-left inner-header-wrapper">
                                        <img class="img-responsive watermark" src=" {{ asset('public/assets/images/watermark/palki1.png') }}" alt="">
                                    </div>
                                </div>
                                <div class="jack">
                                    <div class="inner-header-wrapper-right inner-header-wrapper">
                                        <img class="img-responsive watermark" src=" {{ asset('public/assets/images/watermark/horse2.png') }}" alt="">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--happy couple section end here -->

                <!--Our-stroy section start here -->
                @include('frontend.home_banner')
                <!--Our-stroy end here -->

                <!--Our-success-story section start here -->
                <section class="our-success-Story" id="our-story">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="header-center">
                                    <h2>Our Story</h2>
                                </div>
                               @include('frontend.our_story')



                            </div>
                        </div>
                    </div>
                </section>
                <!--Our-success-story end here -->

                <!--wedding-event section start here -->
                <section class="wedding-event" id="services">
                    <div class="wedding-event-top">
                        <div class="container">
                            <div class="row">
                                <div class="col-xs-12">
                                    <div class="header-center">
                                        <h2>Our Services</h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="row padding-right">
                            <div class="col-xs-12 col-md-7 col-sm-7">
                                <figure>
                                    <div class="row" style="margin-bottom: 5%;">
                                        <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                                            <img src=" {{ asset('public/assets/images/service_grit/s_l_1.jpg') }}" class="img-responsive">
                                        </div>

                                        <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter sprinkle">
                                            <img src=" {{ asset('public/assets/images/service_grit/s_l_2.jpg') }}" class="img-responsive">
                                        </div>

                                        <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                                            <img src=" {{ asset('public/assets/images/service_grit/s_l_3.jpg') }}" class="img-responsive">
                                        </div>
                                    </div>

                                    <div class="row" style="margin-bottom: 5%;">
                                        <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                                            <img src=" {{ asset('public/assets/images/service_grit/s_l_4.jpg') }}" class="img-responsive">
                                        </div>

                                        <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter sprinkle">
                                            <img src=" {{ asset('public/assets/images/service_grit/s_l_5.jpg') }}" class="img-responsive">
                                        </div>

                                        <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                                            <img src=" {{ asset('public/assets/images/service_grit/s_l_6.jpg') }}" class="img-responsive">
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 5%;">
                                        <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                                            <img src=" {{ asset('public/assets/images/service_grit/s_l_7.jpg') }}" class="img-responsive">
                                        </div>

                                        <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter sprinkle">
                                            <img src=" {{ asset('public/assets/images/service_grit/s_l_8.jpg') }}" class="img-responsive">
                                        </div>

                                        <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                                            <img src=" {{ asset('public/assets/images/service_grit/s_l_9.jpg') }}" class="img-responsive">
                                        </div>
                                    </div>
                                </figure>
                            </div>
                            <div class="col-xs-12 col-md-pull-1 col-md-5 col-sm-pull-2 col-sm-5">
                                <div class="event-description-wrapper">
                                    <div class="event-description">
                                        <h3><a href="#">Transportation &amp; Entertainment</a></h3>
                                        <p>
                                            We can help you arrange for all the travel and the accommodation of the guests at no extra hassle to you, making it possible for you to concentrate on other parts of your planning.
                                        </p>
                                        <p>
                                            Whether you are planning a live band, or a DJ evening, the audience is sure to enjoy it. The dancing will come once everyone has had a chance to mingle, and early diners have had the chance to get their fill.
                                        </p>
                                        <a class="read-more" href="{{  url('service')  }}">Read More<i class="fa fa-angle-right"></i></a>

                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row padding-right">
                            <div class="col-xs-12 col-md-5 col-sm-5 push">
                                <div class="event-description-wrapper reception-description-sec">
                                    <div class="event-description">
                                        <h3><a href="#">Lighting &amp; Catering Services</a></h3>
                                        <p>
                                            Good and well placed lighting, we have found, can hide all the minor faults that can often make a wedding venue unattractive
                                        </p>
                                        <p>
                                            Finding and hiring the right catering services is one of the top three requirements of an event.
                                        </p>
                                        <a class="read-more" href="{{  url('service')  }}">Read More<i class="fa fa-angle-right"></i></a>

                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-7 col-sm-7">
                                <figure>
                                    <div class="row" style="margin-bottom: 5%;">
                                        <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                                            <img src=" {{ asset('public/assets/images/service_grit/s_l_10.jpg') }}" class="img-responsive">
                                        </div>

                                        <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter sprinkle">
                                            <img src=" {{ asset('public/assets/images/service_grit/s_l_11.jpg') }}" class="img-responsive">
                                        </div>

                                        <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                                            <img src=" {{ asset('public/assets/images/service_grit/s_l_12.jpg') }}" class="img-responsive">
                                        </div>
                                    </div>

                                    <div class="row" style="margin-bottom: 5%;">
                                        <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                                            <img src=" {{ asset('public/assets/images/service_grit/s_l_13.jpg') }}" class="img-responsive">
                                        </div>

                                        <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter sprinkle">
                                            <img src=" {{ asset('public/assets/images/service_grit/s_l_14.jpg') }}" class="img-responsive">
                                        </div>

                                        <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                                            <img src=" {{ asset('public/assets/images/service_grit/s_l_15.jpg') }}" class="img-responsive">
                                        </div>
                                    </div>
                                    <div class="row" style="margin-bottom: 5%;">
                                        <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                                            <img src=" {{ asset('public/assets/images/service_grit/s_l_16.jpg') }}" class="img-responsive">
                                        </div>

                                        <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter sprinkle">
                                            <img src=" {{ asset('public/assets/images/service_grit/s_l_17.jpg') }}" class="img-responsive">
                                        </div>

                                        <div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
                                            <img src=" {{ asset('public/assets/images/service_grit/s_l_18.jpg') }}" class="img-responsive">
                                        </div>
                                    </div>
                                </figure>
                            </div>
                        </div>
                    </div>
                </section>
                <!--wedding-event section end here -->

                <!--image-section start here -->
                <section class="important-people" id="people-blog">
                    <div class="container">
                        <div class="row">
                            <div class="header-center">
                                <h2>Our Team</h2>
                            </div>
                        </div>
                    </div>

                    <div class="img-section clearfix">
    <div class="container">
        <div class="row">
            <div id="people">
                @foreach ($members as $member)
                     <div class="item">
                        <div class="img">
                            <img style="height: 300px" src="{{ asset('public/uploads/team/'.$member->team_image) }}" alt="">
                            <span class="overlay"> <span>{{ $member->team_title }}</span> </span>
                            <span class="overlay"> <span><small class="designation">{{ $member->team_description }}</small> </span> </span>
                        </div>
                    </div>
                @endforeach
              
            </div>
        </div>
    </div>
</div>

                </section>
              
                <section class="gallery-part" id="gallary">
                    @include('frontend.home_gallery')
                </section>
                <!--wedding-gift section end -->

                <!--Attending-section-start-->
                <section class="attending-class" id="contact">
                    <div class="container">
                        <div class="row">
                            <div class="header-center">
                                <h2>Contact Us</h2>
                            </div>
                        </div>
                        <div class="row text-center">
                            <div class="col-xs-12 col-sm-10 col-sm-push-1">
                                <div class="rsvp-wrapper" data-ng-controller="rsvp2Ctrl">
                                    <h3>CONTACT FORM</h3>
                                   <form action="{{url('storecontact')}}" method="post" name="form">
                                {{ csrf_field() }}
                                        <div class="inner-wrapper">
                                            <div class="input-box">
                                                <input id="name" type="text" name="contact_title" placeholder="Your Name" required="">
                                               
                                            </div>

                                            <div class="input-box">
                                                <input id="mail" type="email" name="contact_email" placeholder="Email" required="">
                                                
                                            </div>
                                        </div>
                                        <div class="inner-wrapper">
                                            <div class="input-box">
                                                <input type="text" name="contact_phone" placeholder="Phone Number" required="">

                                               

                                            </div>
                                            <div class="input-box">
                                                

                                            </div>                                          
                                        </div>
                                        <textarea placeholder="Massage" name="contact_description" data-ng-model="user.notes" required=''>
                                        
                                                
                                        </textarea>
                                        <div data-ng-show="submit &&  form.massage.$invalid">
                                            <span data-ng-show="submit && form.massage.$error.required" class="error-msg">Please enter</span>

                                        </div>

                                        <div id="contactSuccess" class="showmsg">
                                            <span>Hey! Thanks for reaching out. I will get back to you soon</span>
                                        </div>
                                        <button type="success">
                                            Submit
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!--Attending-section-end-->
            </div>
            <!--Content Area End-->



@endsection