<li data-transition="boxslide">
    <!-- MAIN IMAGE -->
    <img src="assets/images/slider_image/s5.jpg" alt="">
    <!-- LAYERS -->
    <div class="banner-content-wrapper">
        <!-- LAYER NR. 1 -->
        <div class="tp-caption NotGeneric-Title tp-resizeme" id="slide-393-layer-9" data-x="center" data-hoffset="" data-y="center" data-voffset="60" data-width="['auto','auto','auto','auto']" data-height="['auto','auto','auto','auto']" data-transform_idle="o:1;" data-transform_in="z:0;rX:0deg;rY:0;rZ:0;sX:2;sY:2;skX:0;skY:0;opacity:0;s:1000;e:Power2.easeOut;" data-transform_out="s:1000;e:Power3.easeInOut;s:1000;e:Power3.easeInOut;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-start="1500" data-splitin="none" data-splitout="none" data-responsive_offset="on" style="">
            <img alt="#" src="assets/images/img-design.PNG">
        </div>
        <!-- LAYER NR. 2 -->
        <div class="tp-caption NotGeneric-Title   tp-resizeme title-head" id="slide-392-layer-8" data-x="center" data-hoffset="" data-y="center" data-voffset="90" data-width="['auto','auto','auto','auto']" data-height="['auto','auto','auto','auto']" data-transform_idle="o:1;" data-transform_in="y:[-100%];z:0;rZ:35deg;sX:1;sY:1;skX:0;skY:0;s:2000;e:Power4.easeInOut;" data-transform_out="s:1000;e:Power2.easeInOut;s:1000;e:Power2.easeInOut;" data-mask_in="x:0px;y:0px;s:inherit;e:inherit;" data-start="1300" data-splitin="chars" data-splitout="none" data-responsive_offset="on" data-elementdelay="0.05" style="font-size: 60px; color:#433e3e; line-height: 1.4; text-transform: uppercase; letter-spacing: 5px; font-family: 'Roboto', sans-serif; font-weight: 500">
            <h1 class="head-text">Shadi Mubarak</h1>
        </div>
    </div>
</li>