﻿<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
		<title>Shadi Mubarak</title>
		<link rel="shortcut icon" type="image/x-icon" href="favicon.ico">
		<link href='https://fonts.googleapis.com/css?family=Roboto:400,300,500,700,900,400italic,500italic,700italic,900italic%7CRoboto+Slab:400,300,700%7CPlayfair+Display:400,400italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
		<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700%7COpen+Sans:400,300%7CLibre+Baskerville:400,400italic' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" type="text/css" href="assets/css/bootstrap.css">
		<link rel="stylesheet" type="text/css" href="assets/css/settings.css">
		<link rel="stylesheet" type="text/css" href="assets/css/simple-line-icons.css">
		<link rel="stylesheet" type="text/css" href="assets/css/font-awesome.css">
		<link rel="stylesheet" type="text/css" href="assets/css/owl.carousel.css">
		<link rel="stylesheet" type="text/css" href="assets/css/jquery.fancybox.css">
		<link rel="stylesheet" type="text/css" href="assets/css/global.css">
		<link rel="stylesheet" type="text/css" href="assets/css/style.css">
		<link rel="stylesheet" type="text/css" href="assets/css/responsive.css">
		<link rel="stylesheet" type="text/less" href="assets/css/skin.less">
	</head>
	<body data-ng-app="websiteApp">

		<!--Loader Start-->
		<?php //include('loader.php');?>
		<!--Loader End-->

		<!--Page Wrapper Start-->
		<div id="wrapper" class="home">
			<!--Header Section Start-->
			    <?php include('header.php'); ?>
			<!--Header Section End-->

			<!--Banner Section start-->
			<section class="banner">
                <?php include('slider.php'); ?>
			</section>
			<!--banner Section End-->

			<!--Content Area Start-->
			<div id="content">
				<!-- happy couple section start here -->
				<section class="happy-couple" id="couple">
					<div class="container">
						<div class="col-md-12">
							<div class="header-center">
								<h2>Message</h2>
							</div>
							<p class="message_pic"><img class="img-responsive" src="assets/images/rezaul_karim.jpg"></p>
							<div>
								<h3 class="message_head" style="font-weight: 800;">Md. Rezaul Karim Bhuyan</h3>
								<p>“Shadi Mubarak” helps you to celebrate wedding in a special and unique way while taking the load off your shoulders. We ensure that everything happens exactly the way you want it. Whether you want the entire wedding to be planned and arranged or a specific event managed, you will find our services truly affordable and well worth.</p>
							</div>
						</div>
						<div class="row">
							<div class="col-xs-12">
								<div class="bell">
									<div class="inner-header-wrapper-left inner-header-wrapper">
										<img class="img-responsive watermark" src="assets/images/watermark/palki1.png" alt="">
									</div>
								</div>
								<div class="jack">
									<div class="inner-header-wrapper-right inner-header-wrapper">
										<img class="img-responsive watermark" src="assets/images/watermark/horse2.png" alt="">
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!--happy couple section end here -->

				<!--Our-stroy section start here -->
                <?php include('home_banner.php');?>
				<!--Our-stroy end here -->

				<!--Our-success-story section start here -->
				<section class="our-success-Story" id="our-story">
					<div class="container">
						<div class="row">
							<div class="col-xs-12">
								<div class="header-center">
									<h2>Our Story</h2>
								</div>
								<div class="timeline-scale"></div>
								<div class="left-right-wrapper clearfix">
									<span class="time-line-divider"> </span>
									<div class="left-section-content">
										<article class="met-section">
											<h3><a href="#">WEDDING PLANNING &amp; MANAGEMENT</a></h3>
											<div class="arrow-cls">
												<span class="css-shape">Details</span>
											</div>
											<figure>
												<img src="assets/images/services/wpser.png" alt="wedding">
											</figure>
											<p>
												We offer you an array of services for wedding event management and co-ordination while you can simply sit back and enjoy the celebrations and our expert team takes care of all the arrangements. We have an expansive database and personal relationships with many of Bangladeshi's best known vendors and key suppliers. So it easy for us to manage the best value of your money.
											</p>
											<a class="read-more" href="#">Read More <i class="fa fa-angle-right"></i></a>
										</article>
										<article class="met-section">
											<h3><a href="#">VENUE SELECTION</a></h3>
											<div class="arrow-cls">
												<span class="css-shape">Details</span>
											</div>
											<figure>
												<img src="assets/images/services/wpser3.jpg" alt="wedding">
											</figure>
											<p>
												Selecting the Ideal Place for Your Wedding is a key factor that needed to be considered when planning your wedding ceremony and reception, so beforehand you can always consult us for the choice of venue selection.
											</p>
											<a class="read-more" href="#">Read More <i class="fa fa-angle-right"></i></a>
										</article>
										<article class="met-section">
											<h3><a href="#">DESIGN &amp; DÉCOR</a></h3>
											<div class="arrow-cls">
												<span class="css-shape">Details</span>
											</div>
											<figure>
												<img src="assets/images/services/wpser4.png" alt="wedding">
											</figure>
											<p>
												We can help you with all kinds of Wedding Décor options – right from conventional décor ideas to designer themes. We work on minute details while planning the décor for each wedding to give it a different look.
											</p>
											<a class="read-more" href="#">Read More <i class="fa fa-angle-right"></i></a>
										</article>
									</div>
									<div class="right-section-content">
										<article class="met-section met-section-right">
											<h3><a href="#">PHOTO &amp; VIDEOGRAPHY</a></h3>
											<div class="arrow-cls right-arrow-cls">
												<span class="css-shape">Details</span>
											</div>
											<figure>
												<img src="assets/images/services/wpser5.png" alt="wedding">
											</figure>
											<p>
												The blissful day of your wedding is a utopian time when two lives unite, and two hearts beat as one. And wedding photo & videography is the memorializing of two people reveling in love and making a promise to share a life – for a lifetime! Our wedding photography services will preserve each magic moment for all time!
											</p>
											<a class="read-more" href="#">Read More <i class="fa fa-angle-right"></i></a>
										</article>

										<article class="met-section met-section-right">
											<h3><a href="#">RENTALS &amp; TRAVELS</a></h3>
											<div class="arrow-cls right-arrow-cls">
												<span class="css-shape">Details</span>
											</div>
											<figure>
												<img src="assets/images/services/wpser6.png" alt="wedding">
											</figure>
											<p>
												When you prepare your wedding guest list, chances are there will be a number of people, especially among your relatives and close friends, who will be coming in from out of town. Making the proper travel arrangements to make sure these guests can attend your special event is a major part of wedding planning. We know that experienced planners always take these travel needs into account while they organize your event.
											</p>
											<a class="read-more" href="#">Read More <i class="fa fa-angle-right"></i></a>
										</article>
										<article class="met-section met-section-right">
											<h3><a href="#">FOOD &amp; BEVERAGE</a></h3>
											<div class="arrow-cls right-arrow-cls">
												<span class="css-shape">Details</span>
											</div>
											<figure>
												<img src="assets/images/services/wpser7.png" alt="wedding">
											</figure>
											<p>
												Serving good tasty food to large groups of people at an important event like your wedding day requires much more than just good cooking skills. For the caterer to have the right cooking staff is only the most basic requirement. You need to look into much more when choosing the perfect caterer to serve the best food to your guests, especially when the turnout will be a large one, as it is at a wedding.
											</p>
											<a class="read-more" href="#">Read More <i class="fa fa-angle-right"></i></a>
										</article>
									</div>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!--Our-success-story end here -->

				<!--wedding-event section start here -->
				<section class="wedding-event" id="services">
					<div class="wedding-event-top">
						<div class="container">
							<div class="row">
								<div class="col-xs-12">
									<div class="header-center">
										<h2>Our Services</h2>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="container">
						<div class="row padding-right">
							<div class="col-xs-12 col-md-7 col-sm-7">
								<figure>
									<div class="row" style="margin-bottom: 5%;">
										<div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
											<img src="assets/images/service_grit/s_l_1.jpg" class="img-responsive">
										</div>

										<div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter sprinkle">
											<img src="assets/images/service_grit/s_l_2.jpg" class="img-responsive">
										</div>

										<div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
											<img src="assets/images/service_grit/s_l_3.jpg" class="img-responsive">
										</div>
									</div>

									<div class="row" style="margin-bottom: 5%;">
										<div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
											<img src="assets/images/service_grit/s_l_4.jpg" class="img-responsive">
										</div>

										<div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter sprinkle">
											<img src="assets/images/service_grit/s_l_5.jpg" class="img-responsive">
										</div>

										<div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
											<img src="assets/images/service_grit/s_l_6.jpg" class="img-responsive">
										</div>
									</div>
									<div class="row" style="margin-bottom: 5%;">
										<div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
											<img src="assets/images/service_grit/s_l_7.jpg" class="img-responsive">
										</div>

										<div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter sprinkle">
											<img src="assets/images/service_grit/s_l_8.jpg" class="img-responsive">
										</div>

										<div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
											<img src="assets/images/service_grit/s_l_9.jpg" class="img-responsive">
										</div>
									</div>
								</figure>
							</div>
							<div class="col-xs-12 col-md-pull-1 col-md-5 col-sm-pull-2 col-sm-5">
								<div class="event-description-wrapper">
									<div class="event-description">
										<h3><a href="#">Transportation &amp; Entertainment</a></h3>
										<p>
											We can help you arrange for all the travel and the accommodation of the guests at no extra hassle to you, making it possible for you to concentrate on other parts of your planning.
										</p>
										<p>
											Whether you are planning a live band, or a DJ evening, the audience is sure to enjoy it. The dancing will come once everyone has had a chance to mingle, and early diners have had the chance to get their fill.
										</p>
										<a class="read-more" href="services.php">Read More<i class="fa fa-angle-right"></i></a>

									</div>
								</div>
							</div>
						</div>
						<div class="row padding-right">
							<div class="col-xs-12 col-md-5 col-sm-5 push">
								<div class="event-description-wrapper reception-description-sec">
									<div class="event-description">
										<h3><a href="#">Lighting &amp; Catering Services</a></h3>
										<p>
											Good and well placed lighting, we have found, can hide all the minor faults that can often make a wedding venue unattractive
										</p>
										<p>
											Finding and hiring the right catering services is one of the top three requirements of an event.
										</p>
										<a class="read-more" href="services.php">Read More<i class="fa fa-angle-right"></i></a>

									</div>
								</div>
							</div>
							<div class="col-xs-12 col-md-7 col-sm-7">
								<figure>
									<div class="row" style="margin-bottom: 5%;">
										<div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
											<img src="assets/images/service_grit/s_l_10.jpg" class="img-responsive">
										</div>

										<div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter sprinkle">
											<img src="assets/images/service_grit/s_l_11.jpg" class="img-responsive">
										</div>

										<div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
											<img src="assets/images/service_grit/s_l_12.jpg" class="img-responsive">
										</div>
									</div>

									<div class="row" style="margin-bottom: 5%;">
										<div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
											<img src="assets/images/service_grit/s_l_13.jpg" class="img-responsive">
										</div>

										<div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter sprinkle">
											<img src="assets/images/service_grit/s_l_14.jpg" class="img-responsive">
										</div>

										<div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
											<img src="assets/images/service_grit/s_l_15.jpg" class="img-responsive">
										</div>
									</div>
									<div class="row" style="margin-bottom: 5%;">
										<div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
											<img src="assets/images/service_grit/s_l_16.jpg" class="img-responsive">
										</div>

										<div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter sprinkle">
											<img src="assets/images/service_grit/s_l_17.jpg" class="img-responsive">
										</div>

										<div class="gallery_product col-lg-4 col-md-4 col-sm-4 col-xs-6 filter hdpe">
											<img src="assets/images/service_grit/s_l_18.jpg" class="img-responsive">
										</div>
									</div>
								</figure>
							</div>
						</div>
					</div>
				</section>
				<!--wedding-event section end here -->

				<!--image-section start here -->
				<section class="important-people" id="people-blog">
					<div class="container">
						<div class="row">
							<div class="header-center">
								<h2>Our Team</h2>
							</div>
						</div>
					</div>

                    <?php include('team.php');?>

				</section>
				<!--image-section end here -->

				<!--&lt;!&ndash;Get-intouch section start &ndash;&gt;-->
				<!--<div class="touch">-->
					<!--<div class="container">-->
						<!--<span>Write your special wishes. We love to hear from all of you ...</span>-->
						<!--<a class="touch-class" href="#">Get in Touch</a>-->
					<!--</div>-->
				<!--</div>-->
				<!--&lt;!&ndash;Get-intouch section end &ndash;&gt;-->

				<!--wedding-gift-section start-->
				<section class="gallery-part" id="gallary">
                    <?php include('home_gallery.php'); ?>
				</section>
				<!--wedding-gift section end -->

				<!--Attending-section-start-->
				<section class="attending-class" id="contact">
					<div class="container">
						<div class="row">
							<div class="header-center">
								<h2>Contact Us</h2>
							</div>
						</div>
						<div class="row text-center">
							<div class="col-xs-12 col-sm-10 col-sm-push-1">
								<div class="rsvp-wrapper" data-ng-controller="rsvp2Ctrl">
									<h3>CONTACT FORM</h3>
									<form class="rsvpForm" name="form" novalidate="">
										<div class="inner-wrapper">
											<div class="input-box">
												<input id="name" type="text" name="fname" placeholder="Your Name" data-ng-pattern="/^(/D)+$/" data-ng-model="user.name" required="">
												<div data-ng-show="form.fname.$invalid && submit ">
													<span data-ng-show="form.fname.$invalid && submit" class="error-msg">This is not a valid name.</span>

												</div>
											</div>

											<div class="input-box">
												<input id="mail" type="email" name="email" placeholder="Email" data-ng-model="user.email" required="">
												<div data-ng-show="form.email.$invalid && submit ">
													<span data-ng-show="form.email.$invalid &&  submit" class="error-msg">This is not a valid email.</span>
												</div>
											</div>
										</div>
										<div class="inner-wrapper">
											<div class="input-box">
												<input type="text" name="Phone" placeholder="Phone Number" data-ng-model="user.meal" required="" data-ng-pattern="/^([a-zA-Z0-9 _-]+)$/">

												<div data-ng-show="form.Phone.$invalid && submit">

													<span data-ng-show="form.Phone.$invalid &&  submit" class="error-msg">This is not a valid phone.</span>

												</div>

											</div>
											<div class="input-box">
												<input type="text" name="area" placeholder="Area Code" data-ng-model="user.meal" required="" data-ng-pattern="/^([a-zA-Z0-9 _-]+)$/">

												<div data-ng-show="form.area.$invalid && submit">

													<span data-ng-show="form.area.$invalid &&  submit" class="error-msg">This is not a valid area Code.</span>

												</div>

											</div>											
										</div>
										<textarea placeholder="Massage" name="massage" data-ng-model="user.notes" required=''>
										
												
										</textarea>
										<div data-ng-show="submit &&  form.massage.$invalid">
											<span data-ng-show="submit && form.massage.$error.required" class="error-msg">Please enter</span>

										</div>

										<div id="contactSuccess" class="showmsg">
											<span>Hey! Thanks for reaching out. I will get back to you soon</span>
										</div>
										<button data-ng-click="update(form.$valid)">
											Submit
										</button>
									</form>
								</div>
							</div>
						</div>
					</div>
				</section>
				<!--Attending-section-end-->
			</div>
			<!--Content Area End-->

			<!--Footer Section start-->
            <?php include('footer.php');?>
			<!--Footer Section End-->
		</div>
		
		<!--Page Wrapper End-->
		<script type="text/javascript" src="assets/js/angular.js"></script>
		<script type="text/javascript" src="assets/js/jquery-1.11.3.min.js"></script>
		<script type="text/javascript" src="assets/js/bootstrap.js"></script>
		<script type="text/javascript" src="assets/js/less.js"></script>
		<script type="text/javascript" src="assets/js/owl.carousel.js"></script>
		<script type="text/javascript" src="assets/js/jquery.appear.js"></script>
		<script type="text/javascript" src="assets/js/isotope.pkgd.js"></script>
		<script type="text/javascript" src="assets/js/jquery.plugin.js"></script>
		<script type="text/javascript" src="assets/js/jquery.fancybox.js"></script>
		<script type="text/javascript" src="assets/js/jquery.countdown.min.js"></script>
		<script type="text/javascript" src="assets/js/app.js"></script>

		<!-- revolution slider Js -->
		<script type="text/javascript" src="assets/js/jquery.themepunch.tools.min.js"></script>
		<script type="text/javascript" src="assets/js/jquery.themepunch.revolution.min.js"></script>
		<script type="text/javascript" src="assets/js/jquery.revolution.js"></script>
		<script type="text/javascript" src="assets/extensions/revolution.extension.video.min.js"></script>
		<script type="text/javascript" src="assets/extensions/revolution.extension.slideanims.min.js"></script>
		<script type="text/javascript" src="assets/extensions/revolution.extension.actions.min.js"></script>
		<script type="text/javascript" src="assets/extensions/revolution.extension.layeranimation.min.js"></script>
		<script type="text/javascript" src="assets/extensions/revolution.extension.kenburn.min.js"></script>
		<script type="text/javascript" src="assets/extensions/revolution.extension.navigation.min.js"></script>
		<script type="text/javascript" src="assets/extensions/revolution.extension.migration.min.js"></script>
		<script type="text/javascript" src="assets/extensions/revolution.extension.parallax.min.js"></script>
		<!--  revolution slider Js -->

		<script type="text/javascript" src="assets/js/site.js"></script>
		<script type="text/javascript" src="assets/js/nav.js"></script>

		<!-- Switcher Js -->
		<script src="assets/js/theme-option/style-switcher/assets/js/style.switcher.js"></script>
		<script src="assets/js/theme-option/style-switcher/assets/js/jquery.cookie.js"></script>
		<!-- Switcher Js -->
        <script src="assets/js/lsb.min.js"></script>
	</body>
</html>