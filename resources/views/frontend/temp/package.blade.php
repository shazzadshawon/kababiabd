@extends('layouts.frontend')

@section('content')



    <!--banner Section start-->
    <div class="parallax-window" data-parallax="scroll" data-image-src="{{ asset('public/assets/images/blog-banner.jpg') }}">
        <section class="banner">

            <div class="banner-content-wrapper">
                <h1>Packages</h1>
            </div>
        </section>
    </div>
    <!--banner Section End-->

    <!--Content Area Start-->
    <div id="content">

        <!-- accomodation start here -->
        <section class="travel-page-content">
@php
    $i=0;
@endphp
        @foreach ($categories as $cat)
            <div class="container">


                <div class="row">
                    <div class="header-center-content">
                        <h2>{{ $cat->cat_name }}</h2>
                       {{--  <p>
                            We offer you an array of services for wedding event management and co-ordination while you can simply sit back and enjoy the celebrations and our expert team takes care of all the arrangements.
                        </p> --}}
                    </div>
                </div>

                @php
                    $packages = DB::table('interior')->where('service_sub_cat_id',$cat->id)->get();
                @endphp

                @foreach ($packages as $package)
                    <div class="row bg-color">
                    @if ($i%2 == 0)
                         <div class="col-xs-12 col-sm-6">

                            <div class="left-part">
                                <figure>
                                    <img src=" {{ asset('public/uploads/interior/'.$package->service_image) }}" alt="#">
                                </figure>

                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">
                            <div class="right-part">
                                <div class="left-part-header">
                                    <h3><a href="#">{{ $package->service_title }}</a></h3>
                                </div>
                                @php
                                    print_r($package->service_description);
                                @endphp
                               
                            </div>
                        </div>
                    @else
                        <div class="col-xs-12 col-sm-6">
                            <div class="right-part">
                                <div class="left-part-header">
                                    <h3><a href="#">{{ $package->service_title }}</a></h3>
                                </div>
                                @php
                                    print_r($package->service_description);
                                @endphp
                               
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6">

                            <div class="left-part">
                                <figure>
                                    <img src=" {{ asset('public/uploads/interior/'.$package->service_image) }}" alt="#">
                                </figure>

                            </div>
                        </div>

                    @endif
                       
                    </div>
                @php
                    $i++;
                @endphp
                @endforeach

               

              



            </div>
        @endforeach

           
        </section>

    </div>
    <!--Content Area End-->
  @endsection