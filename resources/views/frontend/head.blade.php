<meta name="viewport" content="width=device-width, initial-scale=1">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="keywords" content="" />
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- Custom Theme files -->
<link href="{{ asset('public/css/frontend/bootstrap.css') }}" type="text/css" rel="stylesheet" media="all">
<link href="{{ asset('public/css/frontend/owl.css') }}" type="text/css" rel="stylesheet" media="all">
<link href="{{ asset('public/css/frontend/style.css') }}" type="text/css" rel="stylesheet" media="all">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-wvfXpqpZZVQGK6TAh5PVlGOfQNHSoD2xbE+QkPxCAFlNEevoEH3Sl0sibVcOQVnN" crossorigin="anonymous"><!-- font-awesome icons -->
<link rel="stylesheet" href="{{ asset('public/css/frontend/owl.carousel.css') }}" type="text/css" media="all"/> <!-- Owl-Carousel-CSS -->
<link href="{{ asset('public/css/frontend/responsive.css') }}" type="text/css" rel="stylesheet" media="all">
<!-- //Custom Theme files -->
<!-- js -->
<script src="{{ asset('public/js/frontend/jquery-2.2.3.min.js') }}"></script>
<!-- //js -->
<!-- web-fonts -->
<link href="//fonts.googleapis.com/css?family=Berkshire+Swash" rel="stylesheet">
<link href="//fonts.googleapis.com/css?family=Yantramanav:100,300,400,500,700,900" rel="stylesheet">
<!-- //web-fonts -->

<style type="text/css">
	.w3agile-deals { 
    background:url({{ asset('images/specialServices.jpg') }}) no-repeat center 0px fixed;
	background-size: cover; 
}
.w3agile-review{
    background:url({{ asset('images/review_banner.jpg') }}) no-repeat center 0px fixed;
    background-size: cover;
}
</style>