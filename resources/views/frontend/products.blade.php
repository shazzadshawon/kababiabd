@extends('layouts.frontend')

@section('content')

  <div class="banner about-w3bnr" style="background: url({{ asset('images/productBanner.jpg') }}) no-repeat center;">

            <!-- header -->
          
@include('frontend.header')
            <!-- //header-end -->

            <!-- banner-text -->
            <div class="banner-text">
                <div class="container">
                    <h2><span>You Tried The Rest</span><br> Now Try The Best</h2>
                </div>
            </div>
        </div>

        <div class="container">
            <ol class="breadcrumb w3l-crumbs">
                <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Home</a></li>
                @php
                    $menu = DB::table('categories')->where('id',$sub->category_id)->first();
                @endphp
                <li><a href="{{ url('menu/'.$menu->id) }}">{{ $menu->cat_name }}</a></li>
                <li class="active">{{ $sub->sub_cat_name }}</li>
            </ol>
        </div>
        <!-- //breadcrumb -->

        <!-- products -->
        <div class="products">
            <div class="container">
                <div class="col-md-9 product-w3ls-right">
                    <div class="product-top">
                        <h4>{{ $sub->sub_cat_name}}</h4>
                        <h5 class="pull-right">{{ count($portfolios) }} Item(s)</h5>
                      
                      
                        <div class="clearfix"> </div>
                    </div>
                    <div class="products-row">


                        @include('frontend.products_item')

                        <div class="clearfix"> </div>
                    </div>
                </div>
                <div class="col-md-3 rsidebar">
                    <div class="rsidebar-top">
                        <div class="sidebar-row">
                            <h4>All Sub Categories</h4>
                           
                                @include('frontend.sidebar_category')
                            <div class="clearfix"> </div>
                            <!-- script for tabs -->
                            <script type="text/javascript">
                                $(function() {

                                    var menu_ul = $('.faq > li > ul'),
                                           menu_a  = $('.faq > li > a');

                                    menu_ul.hide();

                                    menu_a.click(function(e) {
                                        e.preventDefault();
                                        if(!$(this).hasClass('active')) {
                                            menu_a.removeClass('active');
                                            menu_ul.filter(':visible').slideUp('normal');
                                            $(this).addClass('active').next().stop(true,true).slideDown('normal');
                                        } else {
                                            $(this).removeClass('active');
                                            $(this).next().stop(true,true).slideUp('normal');
                                        }
                                    });

                                });
                            </script>
                            <!-- script for tabs -->
                        </div>
                    </div>
                </div>
                <div class="clearfix"> </div>
            </div>
        </div>
        <!-- //products -->

        <div class="container-fluid">
            <div class="w3agile-deals prds-w3text" style="background:url({{asset('images/review_banner.jpg')}}) no-repeat center 0px fixed;">
                <h5>KABABIA today symbolizes to the traditional home made curry flavors in the Sylhet.</h5>
            </div>
        </div>

        <!-- special Dishes -->
        @include('frontend.special_food')
       
        <!-- //special Dishes -->

        <!-- modal -->
        
        
        <!-- //modal -->


@endsection