@extends('layouts.frontend')

@section('content')

        <div class="banner" style="background: url({{ asset('public/uploads/slider/'.$cover->slider_image) }});
        background-size: cover;
        height: 100%;
        background-position: center;
        background-repeat: no-repeat;">
                <!-- header -->
        @include('frontend.header')
          
                <!-- //header-end --> 
                <!-- banner-text -->
                <div class="banner-text">       
                        <div class="container">
                <!--Banner Header Text ans Sub Header Text-->
                                <h2><span>{{ $cover->slider_title }}</span><br> {{ $cover->slider_subtitle }}</h2>

                @include('frontend.order_banner')

                @if (Session::has('message'))
        
                <div class="alert alert-success" role="alert">
                    <strong></strong><h3 style="color: green; text-align: center;"> {{Session::get('message')}}</h3>
                </div>
                      
                @endif





                        </div>
                </div>
        </div>


  <!-- //banner -->   

    <!-- Offers -->
    @include('frontend.offers')
  <!-- //Offers -->

  <!-- Special Services -->
     @include('frontend.special_services')
  <!-- //Special Services -->

    <!-- special Dishes -->
     @include('frontend.special_food')
    <!-- //special Dishes -->

    <!-- Clients Review-->
     @include('frontend.review')
  <!-- //clients Review-->

@endsection