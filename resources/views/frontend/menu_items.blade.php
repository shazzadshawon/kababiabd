
@php
    $categories = DB::table('categories')->get();
@endphp

@foreach ($categories as $cat)



    <div class="col-sm-2">
        <ul class="multi-column-dropdown">
            <a href="{{ url('menu/'.$cat->id) }}"> <h6>{{ $cat->cat_name }}</h6> </a>
            @php
                $sub_categories = DB::table('int_subcategories')->where('category_id',$cat->id)->get();
            @endphp
            @foreach ($sub_categories as $sub)
                <li><a href="{{ url('submenu/'.$sub->id) }}">{{ $sub->sub_cat_name }}</a></li>
            @endforeach
           
        </ul>
    </div>

@endforeach