<div class="add-products">
    <div class="container">
        <div class="add-products-row">
            <div class="w3ls-add-grids" style="background-image: url({{ asset('public/uploads/blog/'.$offers[0]->blog_image) }})">
                <a href="products.php">
                    <h4><span>
                    {{ $offers[0]->blog_title }}
                </span></h4>
                    <h5> {{ $offers[0]->blog_title }} </h5>
                    <h6><br></h6>
                </a>
            </div>
            <div class="w3ls-add-grids w3ls-add-grids-right" style="background-image: url({{ asset('public/uploads/blog/'.$offers[1]->blog_image) }})">
                <a href="products.php">
                    <h4><span>
                    {{ $offers[1]->blog_title }}
                </span></h4>
                    <h5>{{ $offers[1]->blog_title }}</h5>
                    <h6><br></h6>
                </a>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>