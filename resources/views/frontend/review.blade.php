<div class="w3agile-review">
    <div class="container">
        <h3 class="w3ls-title" style="margin-top: 5%;">Clients Review</h3>
        <div class="two-item-carousel owl-carousel owl-theme" style="margin-top: 7%; margin-bottom: 7%;">

            <!--Testimonial Block-->
            @foreach ($reviews as $review)
                
            <div class="testimonial-block">
                <div class="inner-box">
                    <div class="content">
                        <div class="image-box">
                            <img src="{{ asset('public/uploads/review/'.$review->review_image) }}" alt="" />
                        </div>
                        <h3>{{ $review->review_title }}</h3>
                        <div class="designation">{{ $review->created_at }}</div>
                        <div class="text">
                            @php
                                print_r($review->review_description);
                            @endphp
                        </div>
                    </div>
                </div>
            </div>
            @endforeach

       


        </div>
    </div>
</div>