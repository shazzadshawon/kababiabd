@foreach ($portfolios as $pro)
    <div class="modal video-modal fade" id="{{ $pro->id }}" tabindex="-1" role="dialog" aria-labelledby="{{ $pro->id }}">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <section>
                <div class="modal-body">
                    <div class="col-md-5 modal_body_left">
                         <img src="{{ asset('public/uploads/portfolio/'. $pro->service_image) }}" class="img-responsive" alt="img">
                    </div>
                    <div class="col-md-7 modal_body_right single-top-right">
                        <h3 class="item_name">{{ $pro->service_title }}</h3>
                        <p>{{ $sub->sub_cat_name }}</p>
<!--                        <div class="single-rating">-->
<!--                            <ul>-->
<!--                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>-->
<!--                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>-->
<!--                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>-->
<!--                                <li><i class="fa fa-star-o" aria-hidden="true"></i></li>-->
<!--                                <li class="w3act"><i class="fa fa-star-o" aria-hidden="true"></i></li>-->
<!--                                <li class="rating">20 reviews</li>-->
<!--                                <li><a href="#">Add your review</a></li>-->
<!--                            </ul>-->
<!--                        </div>-->
                        <div class="single-price">
                            <ul>
                                <li>&#2547;{{ $pro->price }}</li>
                                <li>Product Code: {{ $pro->code }}</li>
                              
                            </ul>
                        </div>
                        <p class="single-price-text">
                            @php
                                print_r($pro->service_description);
                            @endphp
                         </p>
                       

                        
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </section>
        </div>
    </div>
</div>
@endforeach