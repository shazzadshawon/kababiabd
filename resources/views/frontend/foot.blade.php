<!-- cart-js -->
<script src="{{ asset('public/js/frontend/minicart.js') }}"></script>
<script>
    w3ls.render();

    w3ls.cart.on('w3sb_checkout', function (evt) {
        var items, len, i;

        if (this.subtotal() > 0) {
            items = this.items();

            for (i = 0, len = items.length; i < len; i++) {
            }
        }
    });
</script>
<!-- //cart-js -->
<!-- Owl-Carousel-JavaScript -->

<script src="{{ asset('public/js/frontend/owl.carousel.js') }}"></script>
<script>
    $(document).ready(function() {
        $("#owl-demo").owlCarousel ({
            items : 3,
            lazyLoad : true,
            autoPlay : true,
            pagination : true,
        });
    });
</script>
<!-- //Owl-Carousel-JavaScript -->
<!-- start-smooth-scrolling -->

<script src="{{ asset('public/js/frontend/SmoothScroll.min.js') }}"></script>

<script src="{{ asset('public/js/frontend/move-top.js') }}"></script>

<script src="{{ asset('public/js/frontend/easing.js') }}"></script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){
            event.preventDefault();

            $('html,body').animate({scrollTop:$(this.hash).offset().top},1000);
        });
    });
</script>
<!-- //end-smooth-scrolling -->
<!-- smooth-scrolling-of-move-up -->
<script type="text/javascript">
    $(document).ready(function() {
        /*
         var defaults = {
         containerID: 'toTop', // fading element id
         containerHoverID: 'toTopHover', // fading element hover id
         scrollSpeed: 1200,
         easingType: 'linear'
         };
         */

        $().UItoTop({ easingType: 'easeOutQuart' });

    });
</script>
<!-- //smooth-scrolling-of-move-up -->
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->

<script src="{{ asset('public/js/frontend/bootstrap.js') }}"></script>

<script type="text/javascript">
    //Three Item Carousel
    //        if ($('.three-item-carousel').length) {
    //            $('.three-item-carousel').owlCarousel({
    //                loop:true,
    //                margin:10,
    //                nav:true,
    //                smartSpeed: 500,
    //                autoplay: 4000,
    //                navText: [ '<span class="fa fa-angle-left"></span>', '<span class="fa fa-angle-right"></span>' ],
    //                responsive:{
    //                    0:{
    //                        items:1
    //                    },
    //                    480:{
    //                        items:1
    //                    },
    //                    600:{
    //                        items:3
    //                    },
    //                    800:{
    //                        items:3
    //                    },
    //                    1024:{
    //                        items:3
    //                    }
    //                }
    //            });
    //        }

    //Two Item Carousel
    if ($('.two-item-carousel').length) {
        $('.two-item-carousel').owlCarousel({
            loop:true,
            margin:30,
            nav:true,
            smartSpeed: 500,
            autoplay: true,
            autoplayTimeout: 10000,
            autoplayHoverPause: false,
            navText: [ '<span class="fa fa-angle-left"></span>', '<span class="fa fa-angle-right"></span>' ],
            responsive:{
                0:{
                    items:1
                },
                480:{
                    items:1
                },
                600:{
                    items:2
                },
                800:{
                    items:2
                },
                1024:{
                    items:2
                }
            }
        });
    }

</script>

        <script src="http://maps.google.com/maps/api/js?key=AIzaSyDElOclVeevpAwrC01r05nJSI6Bri8dSXA"></script>
        <script src="{{ asset('public/js/frontend/mapScript.js') }}"></script>