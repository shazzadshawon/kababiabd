<div class="w3agile-deals">
    <div class="container">
        <h3 class="w3ls-title">Special Services</h3>
        <div class="dealsrow">
            <div class="col-md-6 col-sm-6 deals-grids">
                <div class="deals-left">
                    <i class="fa fa-truck" aria-hidden="true"></i>
                </div>
                <div class="deals-right">
                    <h4>{{ $services[0]->about_title }}</h4>
                    <p>@php
                        print_r($services[0]->about_description);
                    @endphp</p>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="col-md-6 col-sm-6 deals-grids">
                <div class="deals-left">
                    <i class="fa fa-birthday-cake" aria-hidden="true"></i>
                </div>
                <div class="deals-right">
                    <h4>{{ $services[1]->about_title }}</h4>
                    <p>@php
                        print_r($services[1]->about_description);
                    @endphp</p>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="col-md-6 col-sm-6 deals-grids">
                <div class="deals-left">
                    <i class="fa fa-users" aria-hidden="true"></i>
                </div>
                <div class="deals-right">
                   <h4>{{ $services[2]->about_title }}</h4>
                    <p>@php
                        print_r($services[2]->about_description);
                    @endphp</p>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="col-md-6 col-sm-6 deals-grids">
                <div class="deals-left">
                    <i class="fa fa-building" aria-hidden="true"></i>
                </div>
                <div class="deals-right">
                     <h4>{{ $services[3]->about_title }}</h4>
                    <p>@php
                        print_r($services[3]->about_description);
                    @endphp</p>
                </div>
                <div class="clearfix"> </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>