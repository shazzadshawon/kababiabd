@extends('layouts.frontend')

@section('content')



        <!-- banner -->
        <div class="banner about-w3bnr" style="background: url({{ asset('images/bannerContact.jpg') }}) no-repeat right;">

            <!-- header -->
            @include('frontend.header')
            <!-- //header-end -->

            <!-- banner-text -->
            <div class="banner-text">
                <div class="container">
                    <h2>Delicious food from the <br> <span>Best Chefs For you.</span></h2>
                </div>
            </div>
        </div>
        <!-- //banner -->
        <!-- breadcrumb -->
        <div class="container">
            <ol class="breadcrumb w3l-crumbs">
                <li><a href="index.php"><i class="fa fa-home"></i> Home</a></li>
                <li class="active">Contact Us</li>
            </ol>
        </div>
        <!-- //breadcrumb -->
        <!-- contact -->
        <div id="contact" class="contact cd-section">
            <div class="container">
                <h3 class="w3ls-title">Contact us</h3>
                <p class="w3lsorder-text">Lorem ipsum dolor sit amet, consectetur adipisicing elit sheets containing sed </p>
                <div class="container-fluid">

                    <div class="col-xs-6 col-sm-6 contact-w3lsleft">
                        <div class="map-outer" style="width: 100%; height: 550px;">
                            <!--Map Canvas-->
                            <div class="map-canvas"
                                 data-zoom="16"
                                 data-lat="24.8882321"
                                 data-lng="91.8800005"
                                 data-type="roadmap"
                                 data-hue="#ffc400"
                                 data-title="Kababia Restaurant"
                                 data-content="K. K. Mansion (2nd Floor), Subhanighat (Beside Oasis Hospital), Sylhet<br><a href='mailto:contact@kababiabd.com'>contact@kababiabd.com</a>" style="height: inherit;">
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-6 col-sm-6 contact-w3lsright">
                        <h6>Welcome to <span>KABABIA </span> – a pleasant and elegant Indian cuisine with superb service. </h6>
                        <div class="address-row">
                            <div class="col-xs-2 address-left">
                                <span class="glyphicon" aria-hidden="true">
                                    <i class="fa fa-home" aria-hidden="true"></i>

                                </span>
                            </div>
                            <div class="col-xs-10 address-right">
                                <h5>Visit Us</h5>
                                <p>K. K. Mansion (2nd Floor), Subhanighat (Beside Oasis Hospital), Sylhet </p>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                        <div class="address-row w3-agileits">
                            <div class="col-xs-2 address-left">
                                <span class="glyphicon" aria-hidden="true">
                                    <i class="fa fa-envelope" aria-hidden="true"></i>

                                </span>
                            </div>
                            <div class="col-xs-10 address-right">
                                <h5>Mail Us</h5>
                                <p><a href="mailto:contact@kababiabd.com"> contact@kababiabd.com</a></p>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                        <div class="address-row">
                            <div class="col-xs-2 address-left">
                               <span class="glyphicon" aria-hidden="true">
                                    <i class="fa fa-volume-control-phone" aria-hidden="true"></i>
                                </span>
                            </div>
                            <div class="col-xs-10 address-right">
                                <h5>Call Us</h5>
                                <p>+88 017 41228971</p>
                            </div>
                            <div class="clearfix"> </div>
                        </div>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
            <!-- map -->
<!--            <div class="map agileits">-->
<!--                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3023.948805392833!2d-73.99619098458929!3d40.71914347933105!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x89c25a27e2f24131%3A0x64ffc98d24069f02!2sCANADA!5e0!3m2!1sen!2sin!4v1479793484055"></iframe>-->
<!--            </div>-->
            <!-- //map -->
        </div>
        <!-- //contact -->

@endsection