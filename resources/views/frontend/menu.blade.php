@extends('layouts.frontend')

@section('content')



 <div class="banner about-w3bnr" style="background: url({{ asset('images/menuBanner.jpg') }}) no-repeat center;">

            <!-- header -->
            @include('frontend.header')
            <!-- //header-end -->

            <!-- banner-text -->
            <div class="banner-text">
                <div class="container">
                    <h2><span>You Tried The Rest</span><br> Now Try The Best</h2>
                </div>
            </div>
        </div>
        <!-- //banner -->
        <!-- breadcrumb -->
        <div class="container">
            <ol class="breadcrumb w3l-crumbs">
                <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Home</a></li>
                <li class="active">{{ $main->cat_name }}</li>
            </ol>
        </div>
        <!-- //breadcrumb -->
        <!-- menu list -->
        <div class="wthree-menu">
            <img src="images/i2.jpg" class="w3order-img" alt=""/>
            <div class="container">
                <h3 class="w3ls-title">{{ $main->cat_name }} Menu</h3>
                <p class="w3lsorder-text">Here are the Sub Category list under {{ $main->cat_name }} Category</p>
                <div class="menu-agileinfo">

                    @foreach ($menus as $menu)
                        
                        <div class="col-md-4 col-sm-4 col-xs-6 menu-w3lsgrids">
                            <a href="{{ url('submenu/'.$menu->id) }}"> {{ $menu->sub_cat_name }}</a>
                        </div>
                    @endforeach





                    <div class="clearfix"> </div>
                </div>

            </div>
        </div>
        <!-- //menu list -->

            @include('frontend.offers')

@endsection