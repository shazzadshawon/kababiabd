<!DOCTYPE html>
<html lang="en">
  <head>
      <title>Rong Tuli</title>
      <?php require('head.php'); ?>
  </head>
  <body>
    <?php require('preloader.php'); ?>
    <main class="body-wrapper">
        <!-- /.navbar -->
        <?php require('header.php'); ?>
        <!-- /.navbar -->

        <!-- .revolution -->
        <?php require('home_slider.php'); ?>
        <!-- /.revolution -->

        <!-- .special offer -->
        <?php require('special_offer.php'); ?>
        <!-- /.special offer -->

        <!--Recent Work-->
        <?php require('recent_work.php'); ?>
        <!--/. Recent Work-->

        <!--A Little About ME-->
<!--        --><?php //require('details_page.php');?>
        <!--//A Little About ME-->

        <!--Service Slider-->
        <?php require('service_slider.php'); ?>
        <!--.Service Slider-->

        <!--Client Review-->
        <?php require('client_review.php'); ?>
        <!--./Client Review-->

        <!-- /footer -->
        <?php require('footer.php'); ?>
        <!-- ./footer -->
    </main>
    <!--/.body-wrapper -->
    <script src="style/js/bootstrap.min.js"></script>
    <script src="style/js/plugins.js"></script>
    <script src="style/js/classie.js"></script>
    <script src="style/js/jquery.themepunch.tools.min.js"></script>
    <script src="style/js/scripts.js"></script>
  </body>
</html>