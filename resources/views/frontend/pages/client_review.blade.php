<div class="dark-wrapper">
    <div class="container inner">
        <h3 class="section-title text-center">Client <strong>Review</strong></h3>
        <div class="divide10"></div>
        <div class="testimonials2 small-quote">
            <div class="row">
            @php 
            $i=0;
            @endphp
                @foreach($reviews as $review)
                @if($i%2==0)
                <div class="col-sm-12 col-md-6">
                    <div class="quote">
                        <div class="icon text-center"> <img  src="{{asset('public/uploads/review/'.$review->review_image)}}" alt="" />
                            <div class="author">
                                <h4 class="post-title">{{$review->review_title}}</h4>
                                <!-- <span class="meta">Interface Designer</span>  -->
                            </div>
                        </div>
                        <div class="box">
                            <blockquote>
                                @php
                                    print_r($review->review_description);
                                @endphp
                            </blockquote>
                        </div>
                    </div>
                    <!--/.quote -->
                </div>
                @else
                <!--/column -->
                <div class="col-sm-12 col-md-6">
                    <div class="quote right">
                        <div class="icon text-center"> <img  src="{{asset('public/uploads/review/'.$review->review_image)}}" alt="" />
                            <div class="author">
                                <h4 class="post-title">{{$review->review_title}}</h4>
                                <span class="meta"></span>
                             </div>
                            <!--/.author -->
                        </div>
                        <!--/.icon -->
                        <div class="box">
                            <blockquote>
                               @php
                                    print_r($review->review_description);
                                @endphp
                            </blockquote>
                        </div>
                    </div>
                    <!--/.quote -->
                </div>
                @endif
                @php
                $i++;
                @endphp
                @endforeach
                <!--/column -->
            </div>
            <!--/.row -->

            <!--/.row -->
        </div>
        <!--/.testimonials -->
    </div>
    <!--/.container -->
</div>
<!--/.dark-wrapper -->