<div class="tp-fullscreen-container revolution">
    <div class="tp-fullscreen">
        <ul>
            @foreach($sliders as $slide)
            <li data-transition="fade">
                <img src="{{asset('public/uploads/slider/'.$slide->slider_image)}}" alt="" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat" />
                <div class="tp-caption large text-center sfb" data-x="center" data-y="293" data-speed="900" data-start="800" data-easing="Sine.easeOut">{{ $slide->slider_title }}</div>
                <div class="tp-caption medium text-center sfb" data-x="center" data-y="387" data-speed="900" data-start="1500" data-easing="Sine.easeOut">{{ $slide->slider_subtitle }}</div>
            </li>
            @endforeach
          
<!--            <li data-transition="fade">-->
<!--                <img src="style/video/nyc.jpg" alt="" data-bgfit="cover" data-bgposition="center top" data-bgrepeat="no-repeat" />-->
<!--                <div class="tp-caption large text-center sfb" data-x="center" data-y="293" data-speed="900" data-start="800" data-endspeed="100" data-easing="Sine.easeOut" style="z-index: 2;">html5 self hosted video support</div>-->
<!--                <div class="tp-caption medium text-center sfb" data-x="center" data-y="387" data-speed="900" data-start="1500" data-endspeed="100" data-easing="Sine.easeOut" style="z-index: 2;">for better visualization of your company</div>-->
<!--                <div class="tp-caption tp-fade fadeout fullscreenvideo"-->
<!--                     data-x="0"-->
<!--                     data-y="0"-->
<!--                     data-speed="1000"-->
<!--                     data-start="1100"-->
<!--                     data-easing="Power4.easeOut"-->
<!--                     data-elementdelay="0.01"-->
<!--                     data-endelementdelay="0.1"-->
<!--                     data-endspeed="1500"-->
<!--                     data-endeasing="Power4.easeIn"-->
<!--                     data-autoplay="true"-->
<!--                     data-autoplayonlyfirsttime="false"-->
<!--                     data-nextslideatend="true"-->
<!--                     data-dottedoverlay="twoxtwo"-->
<!--                     data-volume="mute" data-forceCover="1" data-aspectratio="16:9" data-forcerewind="on" style="z-index: 1;">-->
<!--                    <video class="" preload="none" width="100%" height="100%"-->
<!--                           poster='style/video/nyc.jpg'>-->
<!--                        <source src='style/video/nyc.mp4' type='video/mp4' />-->
<!--                        <source src='style/video/nyc.webm' type='video/webm' />-->
<!--                    </video>-->
<!--                </div>-->
<!--            </li>-->
        </ul>
        <div class="tp-bannertimer tp-bottom"></div>
    </div>
    <!-- /.tp-fullscreen-container -->
</div>