<div class="cbp-item print motion">
    <a class="cbp-caption fancybox-media" data-rel="portfolio" href="https://vimeo.com/24243147" data-title-id="title-1">
        <div class="cbp-caption-defaultWrap"> <img src="style/images/art/p1.jpg" alt="" /> </div>
        <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
                <div class="cbp-l-caption-body">
                    <div class="cbp-l-caption-title"><span class="cbp-plus"></span></div>
                </div>
            </div>
        </div>
        <!--/.cbp-caption-activeWrap -->
    </a>
    <div id="title-1" class="hidden">
        <h3>Ipsum Tortor Dapibus</h3>
        <p>Aenean eu leo quam. Pellentesque ornare sem lacinia quam venenatis vestibulum.</p>
    </div>
    <!-- /.hidden -->
</div>
<!--/.cbp-item -->

<div class="cbp-item print"> <a class="cbp-caption fancybox-media" data-rel="portfolio" href="style/images/art/p2-full.jpg" data-title-id="title-2">
        <div class="cbp-caption-defaultWrap"> <img src="style/images/art/p2.jpg" alt="" /> </div>
        <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
                <div class="cbp-l-caption-body">
                    <div class="cbp-l-caption-title"><span class="cbp-plus"></span></div>
                </div>
            </div>
        </div>
        <!--/.cbp-caption-activeWrap -->
    </a> </div>
<!--/.cbp-item -->

<div class="cbp-item web logo"> <a class="cbp-caption fancybox-media" data-rel="portfolio" href="style/images/art/p3-full.jpg" data-title-id="title-3">
        <div class="cbp-caption-defaultWrap"> <img src="style/images/art/p3.jpg" alt="" /> </div>
        <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
                <div class="cbp-l-caption-body">
                    <div class="cbp-l-caption-title"><span class="cbp-plus"></span></div>
                </div>
            </div>
        </div>
        <!--/.cbp-caption-activeWrap -->
    </a>
    <div id="title-3" class="hidden">
        <h3>Ullamcorper Sem Bibendum</h3>
        <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>
    </div>
    <!-- /.hidden -->
</div>
<!--/.cbp-item -->

<div class="cbp-item web"> <a class="cbp-caption fancybox-media" data-rel="portfolio" href="style/images/art/p4-full.jpg" data-title-id="title-4">
        <div class="cbp-caption-defaultWrap"> <img src="style/images/art/p4.jpg" alt="" /> </div>
        <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
                <div class="cbp-l-caption-body">
                    <div class="cbp-l-caption-title"><span class="cbp-plus"></span></div>
                </div>
            </div>
        </div>
        <!--/.cbp-caption-activeWrap -->
    </a> </div>
<!--/.cbp-item -->

<div class="cbp-item print logo"> <a class="cbp-caption fancybox-media" data-rel="portfolio" href="style/images/art/p5-full.jpg" data-title-id="title-5">
        <div class="cbp-caption-defaultWrap"> <img src="style/images/art/p5.jpg" alt="" /> </div>
        <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
                <div class="cbp-l-caption-body">
                    <div class="cbp-l-caption-title"><span class="cbp-plus"></span></div>
                </div>
            </div>
        </div>
        <!--/.cbp-caption-activeWrap -->
    </a> </div>
<!--/.cbp-item -->

<div class="cbp-item print web"> <a class="cbp-caption fancybox-media" data-rel="portfolio" href="style/images/art/p6-full.jpg" data-title-id="title-6">
        <div class="cbp-caption-defaultWrap"> <img src="style/images/art/p6.jpg" alt="" /> </div>
        <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
                <div class="cbp-l-caption-body">
                    <div class="cbp-l-caption-title"><span class="cbp-plus"></span></div>
                </div>
            </div>
        </div>
        <!--/.cbp-caption-activeWrap -->
    </a>
    <div id="title-6" class="hidden">
        <h3>Ullamcorper Sem Bibendum</h3>
        <p>Praesent commodo cursus magna, vel scelerisque nisl consectetur et.</p>
    </div>
    <!-- /.hidden -->
</div>
<!--/.cbp-item -->

<div class="cbp-item motion logo"> <a class="cbp-caption fancybox-media" data-rel="portfolio" href="https://vimeo.com/6757600" data-title-id="title-7">
        <div class="cbp-caption-defaultWrap"> <img src="style/images/art/p7.jpg" alt="" /> </div>
        <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
                <div class="cbp-l-caption-body">
                    <div class="cbp-l-caption-title"><span class="cbp-plus"></span></div>
                </div>
            </div>
        </div>
        <!--/.cbp-caption-activeWrap -->
    </a> </div>
<!--/.cbp-item -->

<div class="cbp-item print logo"> <a class="cbp-caption fancybox-media" data-rel="portfolio" href="style/images/art/p8-full.jpg" data-title-id="title-8">
        <div class="cbp-caption-defaultWrap"> <img src="style/images/art/p8.jpg" alt="" /> </div>
        <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
                <div class="cbp-l-caption-body">
                    <div class="cbp-l-caption-title"><span class="cbp-plus"></span></div>
                </div>
            </div>
        </div>
        <!--/.cbp-caption-activeWrap -->
    </a> </div>
<!--/.cbp-item -->

<div class="cbp-item logo web"> <a class="cbp-caption fancybox-media" data-rel="portfolio" href="style/images/art/p9-full.jpg" data-title-id="title-9">
        <div class="cbp-caption-defaultWrap"> <img src="style/images/art/p9.jpg" alt="" /> </div>
        <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
                <div class="cbp-l-caption-body">
                    <div class="cbp-l-caption-title"><span class="cbp-plus"></span></div>
                </div>
            </div>
        </div>
        <!--/.cbp-caption-activeWrap -->
    </a> </div>
<!--/.cbp-item -->

<div class="cbp-item print web"> <a class="cbp-caption fancybox-media" data-rel="portfolio" href="style/images/art/p10-full.jpg" data-title-id="title-10">
        <div class="cbp-caption-defaultWrap"> <img src="style/images/art/p10.jpg" alt="" /> </div>
        <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
                <div class="cbp-l-caption-body">
                    <div class="cbp-l-caption-title"><span class="cbp-plus"></span></div>
                </div>
            </div>
        </div>

        <!--/.cbp-caption-activeWrap -->

    </a> </div>

<!--/.cbp-item -->

<div class="cbp-item print"> <a class="cbp-caption fancybox-media" data-rel="portfolio" href="style/images/art/p11-full.jpg" data-title-id="title-11">
        <div class="cbp-caption-defaultWrap"> <img src="style/images/art/p11.jpg" alt="" /> </div>
        <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
                <div class="cbp-l-caption-body">
                    <div class="cbp-l-caption-title"><span class="cbp-plus"></span></div>
                </div>
            </div>
        </div>

        <!--/.cbp-caption-activeWrap -->

    </a> </div>

<!--/.cbp-item -->

<div class="cbp-item print logo"> <a class="cbp-caption fancybox-media" data-rel="portfolio" href="style/images/art/p12-full.jpg" data-title-id="title-12">
        <div class="cbp-caption-defaultWrap"> <img src="style/images/art/p12.jpg" alt="" /> </div>
        <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
                <div class="cbp-l-caption-body">
                    <div class="cbp-l-caption-title"><span class="cbp-plus"></span></div>
                </div>
            </div>
        </div>
        <!--/.cbp-caption-activeWrap -->
    </a> </div>
<!--/.cbp-item -->

<div class="cbp-item print logo"> <a class="cbp-caption fancybox-media" data-rel="portfolio" href="style/images/art/p13-full.jpg" data-title-id="title-13">
        <div class="cbp-caption-defaultWrap"> <img src="style/images/art/p13.jpg" alt="" /> </div>
        <div class="cbp-caption-activeWrap">
            <div class="cbp-l-caption-alignCenter">
                <div class="cbp-l-caption-body">
                    <div class="cbp-l-caption-title"><span class="cbp-plus"></span></div>
                </div>
            </div>
        </div>
        <!--/.cbp-caption-activeWrap -->
    </a>
</div>
<!--/.cbp-item -->