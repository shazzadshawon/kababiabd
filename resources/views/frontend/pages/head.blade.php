<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="style/images/favicon_r.jpg">
<!-- Bootstrap core CSS -->
<link href="{{asset('public/assets/style/css/bootstrap.min.css')}}" rel="stylesheet">
<link href="{{asset('public/assets/style/css/plugins.css')}}" rel="stylesheet">
<link href="{{asset('public/assets/style/style.css')}}" rel="stylesheet">
<link href="{{asset('public/assets/style/css/color/purple.css')}}" rel="stylesheet">
<link href='http://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Karla:400,400italic,700,700italic' rel='stylesheet' type='text/css'>
<link href="{{asset('public/assets/style/type/icons.css')}}" rel="stylesheet">
<script src="{{asset('public/assets/style/js/jquery.min.js')}}"></script>
<!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->