@extends('layouts.frontend')

@section('content')
            <div class="post-parallax parallax inverse-wrapper parallax2" style="background-image: url(style/images/bg_about.jpg);">
                <div class="container inner text-center">
                    <div class="headline text-center">
                        <h2>Gallery</h2>
                        <p class="lead">Gallery page subheading</p>
                    </div>
                    <!-- /.headline -->
                </div>
            </div>

            <div id="sticky-filter" class="sticky-filter dark-wrapper container">
                <ul>
                @foreach($gallery_cats as $cat)
                    <li><a href="#{{$cat->id}}">{{$cat->cat_name}}</a></li>
                @endforeach
                </ul>
            </div>
            <!-- /filter -->
            @foreach($gallery_cats as $cat)
                <section id="{{$cat->id}}" class="light-wrapper">
                        <div class="container inner">
                            <h3 class="section-title text-center">{{$cat->cat_name}} gallery</h3>
                            <p class="text-center"><!-- Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. --></p>
                            <div class="divide20"></div>
                            <div class="cbp-panel">
                                <div class="cbp cbp-onepage-grid">
                                <?php 
                                $galleries = DB::table('galleries')->where('category_id',$cat->id)->get();
                                 ?>
                                 <?php foreach ($galleries as $gal): ?>
                                     <?php if ($gal->type==0): ?>
                                          <div class="cbp-item"> <a class="cbp-caption fancybox-media" data-rel="portfolio" href="{{asset('public/uploads/gallery/'.$cat->cat_name.'/'.$gal->gallery_image)}}">
                                            <div class="cbp-caption-defaultWrap">
                                             <img style="height: 300px" src="{{asset('public/uploads/gallery/'.$cat->cat_name.'/'.$gal->gallery_image)}}" alt="" /> </div>
                                            <div class="cbp-caption-activeWrap">
                                                <div class="cbp-l-caption-alignCenter">
                                                    <div class="cbp-l-caption-body">
                                                        <div class="cbp-l-caption-title"><span class="cbp-plus"></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/.cbp-caption-activeWrap -->
                                        </a> </div>
                                    <?php else: ?>
                                         <div class="cbp-item"> <a class="cbp-caption fancybox-media" data-rel="portfolio" href="{{asset('public/uploads/gallery/'.$cat->cat_name.'/'.$gal->video_name)}}">
                                            <div class="cbp-caption-defaultWrap">
                                             <img style="height: 300px" src="{{asset('public/uploads/gallery/'.$cat->cat_name.'/'.$gal->cover_image)}}" alt="" /> </div>
                                            <div class="cbp-caption-activeWrap">
                                                <div class="cbp-l-caption-alignCenter">
                                                    <div class="cbp-l-caption-body">
                                                        <div class="cbp-l-caption-title"><span class="cbp-plus"></span></div>
                                                    </div>
                                                </div>
                                            </div>
                                            <!--/.cbp-caption-activeWrap -->
                                        </a> </div>
                                     <?php endif ?>
                                 <?php endforeach ?>
                                   
                                    <!--/.cbp-item -->






                                </div>
                                <!--/.cbp -->

                            </div>
                            <!--/.cbp-panel -->
                        </div>
                        <!-- /.container -->
                    </section>
            @endforeach

           
            <!-- /#conceptual -->

         

@endsection