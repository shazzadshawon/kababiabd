<div class="light-wrapper">
    <div class="container inner">
        <div class="thin">
            <h3 class="section-title text-center">Special Offers</h3>
            <p class="text-center">Special Related some text. Special Related some text. Special Related some text. Special Related some text. Special Related some text. Special Related some text. </p>
        </div>
        <!-- /.thin -->
        <div class="divide10"></div>
        <div class="carousel-wrapper">
            <div class="carousel carousel-boxed blog">

                @foreach( $offers as $offer )
                <div class="item post caption-overlay custom-overlay">
                    <figure><a href="#"><img style="height: 300px" src="{{asset('public/uploads/blog/'.$offer->blog_image)}}" alt="" /> </a></figure>
                    <div class="caption bottom-right">
                        <div class="title">
                            <h3 class="main-title layer">{{$offer->blog_title}}</h3>
                        </div>
                        <!--/.title -->
                    </div>
                    <!--/.caption -->
                </div>
                @endforeach
                <!-- /.post -->

            
                <!-- /.post -->
            </div>
            <!--/.carousel -->
        </div>
        <!--/.carousel-wrapper -->
    </div>
    <!--/.container -->

</div>