@extends('layouts.frontend')

@section('content')


            <div class="post-parallax parallax inverse-wrapper parallax2" style="background-image: url(../style/images/bg_portfolio.jpg);">
                <div class="container inner text-center">
                    <div class="headline text-center">
                        <h2>{{$portfolio->service_title}}</h2>
                        <!-- <p class="lead">Portfolio One details page subheading</p> -->
                    </div>
                    <!-- /.headline -->
                </div>
            </div>

            <div class="light-wrapper">
    <div class="col-image">
        <div class="bg-wrapper col-md-6">
            <div class="bg-holder" style="background-image: url(../public/uploads/service/{{$portfolio->service_image}});"></div>
        </div>
        <!--/.bg-wrapper -->
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-md-offset-7 inner-col">
                    <!-- <h3 class="section-title">{{$portfolio->service_title}}</h3> -->
                   @php
                    print_r($portfolio->service_description);
                   @endphp
                
                    <!-- /.progress-list -->
                </div>
                <!--/column -->
            </div>
            <!--/.row -->
        </div>
        <!--/.container -->
    </div>
    <!--/.col-image -->

</div>
<!-- /.light-wrapper -->

@endsection
