@extends('layouts.frontend')

@section('content')
            <!-- /.navbar -->

            <div class="post-parallax parallax inverse-wrapper parallax2" style="background-image: url(style/images/bg_packages.jpg);">
                <div class="container inner text-center">
                    <div class="headline text-center">
                        <h2>Packages</h2>
                        <p class="lead">Packages details page subheading</p>
                    </div>
                    <!-- /.headline -->
                </div>
            </div>

            <div style="background: purple">
                <div class="container inner">
                    <div class="row">


                    @foreach($categories as $cat)
                        <div class="col-sm-6 col-md-3">
                            <div class="pricing panel">
                                <div class="panel-heading" style="background: #c153c1;">
                                    <h3 style="color: white;" class="panel-title">{{$cat->package_title}}</h3>
                                    <h2 style="color: white;" class="panel-title">${{$cat->package_price}}</h2>

                                </div>
                                <!--/.panel-heading -->
                                <div class="panel-body">
                                    <table class="table">
                                    <?php 
                                    $items = DB::table('package_items')->where('cat_id',$cat->id)->get();

                                     ?>
                                    <?php foreach ($items as $item): ?>
                                         <tr><td>{{$item->item}}</td></tr>
                                    <?php endforeach ?>
                                       
                                       

                                    </table>
                                </div>
                                <!--/.panel-body -->
                            </div>
                            <!--/.pricing -->
                        </div>
                    @endforeach
                        <!--/column -->
                       
                        <!--/column -->
                      
                      
                        <!--/column -->
                    </div>
                    <!--/.row -->

<!--                    <div class="row">-->
<!--                        <div class="col-md-12">-->
<!--                            <div class="col-sm-6 col-md-6">-->
<!--                                <div class="pricing panel">-->
<!--                                    <div class="panel-heading">-->
<!--                                        <h3 class="panel-title">ROYAL</h3>-->
<!--                                    </div>-->
<!--                                    <div class="panel-body">-->
<!--                                        <table class="table">-->
<!--                                            <tr><td> Package Item </td></tr>-->
<!--                                            <tr><td> Package Item </td></tr>-->
<!--                                            <tr><td> Package Item </td></tr>-->
<!--                                            <tr><td> Package Item </td></tr>-->
<!--                                            <tr><td> Package Item </td></tr>-->
<!--                                            <tr><td> Package Item </td></tr>-->
<!--                                            <tr><td> Package Item </td></tr>-->
<!--                                        </table>-->
<!--                                    </div>-->
<!--                                    <div class="panel-footer"> <a href="#" class="btn" role="button">Sign Up</a></div>-->
<!--                                </div>-->
<!--                            </div>-->
<!--                        </div>-->
<!--                    </div>-->
                </div>
                <!--/.container -->
            </div>
            <!-- /.light-wrapper -->

@endsection