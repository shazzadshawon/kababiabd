<div class="w3agile-spldishes">
    <div class="container">
        <h3 class="w3ls-title">Special Dishes</h3>
        <div class="spldishes-agileinfo">
            <div class="col-md-3 spldishes-w3left">
                <h5 class="w3ltitle">Kababia Specials</h5>
                <p>Some Descriptions Some Descriptions Some Descriptions Some Descriptions Some Descriptions Some Descriptions</p>
            </div>
            <div class="col-md-9 spldishes-grids">
                <!-- Owl-Carousel -->
                <div id="owl-demo" class="owl-carousel text-center agileinfo-gallery-row">
                    @foreach ($dishes as $food)
                         <a href="#" class="item g1">
                            <img class="lazyOwl" src="{{ asset('public/uploads/team/'.$food->team_image) }}" title="Our latest gallery" alt=""/>
                            <div class="agile-dish-caption">
                                <h4>{{ $food->team_title }}</h4>
                                <span>@php
                                    print_r($food->team_description);
                                @endphp</span>
                            </div>
                        </a>
                    @endforeach
                  
                  
                </div>
            </div>
            <div class="clearfix"> </div>
        </div>
    </div>
</div>