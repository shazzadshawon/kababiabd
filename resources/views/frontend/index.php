<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Kababia</title>
        <?php require('head.php'); ?>
    </head>
<body>
	<!-- banner -->
	<div class="banner" style="background: url(images/Chicken_Tangri_Kebab.jpg) no-repeat center;">
		<!-- header -->
        <?php require('header.php'); ?>
		<!-- //header-end --> 
		<!-- banner-text -->
		<div class="banner-text">	
			<div class="container">
                <!--Banner Header Text ans Sub Header Text-->
				<h2><span>You Tried The Rest</span><br> Now Try The Best</h2>

                <?php require('order_banner.php'); ?>

			</div>
		</div>
	</div>
	<!-- //banner -->   

    <!-- Offers -->
    <?php require('offers.php'); ?>
	<!-- //Offers -->

	<!-- Special Services -->
    <?php require('special_services.php'); ?>
	<!-- //Special Services -->

    <!-- special Dishes -->
    <?php require('special_food.php'); ?>
    <!-- //special Dishes -->

    <!-- Clients Review-->
    <?php require('review.php'); ?>
	<!-- //clients Review-->

    <!--Footer-->
    <?php require('footer.php'); ?>
	<!-- //footer -->

    <?php require('foot.php'); ?>
</body>
</html>