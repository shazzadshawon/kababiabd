@foreach ($portfolios as $pro)
    <div class="modal video-modal fade" id="{{ $pro->id }}" tabindex="-1" role="dialog" aria-labelledby="{{ $pro->id }}">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">×</span></button>
            </div>
            <section>
                <div class="modal-body">
                    <div class="col-md-12">
                         <img style="width: 100%" src="{{ asset('public/uploads/gallery/'. $pro->gallery_image) }}" class="img-responsive" alt="img">
                    </div>
                  
                    <div class="clearfix"> </div>
                </div>
            </section>
        </div>
    </div>
</div>
@endforeach