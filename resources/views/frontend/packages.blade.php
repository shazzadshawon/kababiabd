@extends('layouts.frontend')

@section('content')

  <!-- banner -->
        <div class="banner about-w3bnr" style="background: url({{ asset('images/HarialiKebab.jpg') }}) no-repeat right;">

            <!-- header -->
            @include('frontend.header')
            <!-- //header-end -->

            <!-- banner-text -->
            <div class="banner-text">
                <div class="container">
                    <h2>You Tried The Best<br> <span>Now Try The Best</span></h2>
                </div>
            </div>
        </div>

<div class="container">
    <ol class="breadcrumb w3l-crumbs">
        <li><a href="{{ url('/') }}"><i class="fa fa-home"></i> Home</a></li>
        <li class="active">Packages</li>
    </ol>
</div>
<!-- //breadcrumb -->
<!--  about-page -->
<div class="about">
    <div class="container">
        <h3 class="w3ls-title w3ls-title1">Packages</h3>
@php
	$i=0;
@endphp
       @foreach ($packages as $pak)
       		<div class="about-text" style="border: 15px solid rgb(255, 233, 232); padding: 2em; margin-top: 5%; box-sizing: border-box;">
       			@if ($i%2==0)
       				 <div class="col-xs-12 col-sm-12 col-md-6">
				        <img style="width: 100%" class="img-responsive" src="{{ asset('public/uploads/service/'.$pak->service_image) }}" style="border-radius: 6px;" >
				    </div>

				    <div class="col-xs-12 col-sm-12 col-md-6">
				        <h3>{{ $pak->service_title }}</h3>
				        <p style="margin-top: 5%;">
				        	@php
				        		print_r($pak->service_description);
				        	@endphp
				        </p>
				    </div>


				@else


				    <div class="col-xs-12 col-sm-12 col-md-6">
				        <h3>{{ $pak->service_title }}</h3>
				        <p style="margin-top: 5%;">
				        	@php
				        		print_r($pak->service_description);
				        	@endphp
				        </p>
				    </div>
				     <div class="col-xs-12 col-sm-12 col-md-6">
				        <img style="width: 100%" class="img-responsive" src="{{ asset('public/uploads/service/'.$pak->service_image) }}" style="border-radius: 6px;" >
				    </div>

       			@endif

				   
				    <div class="clearfix"> </div>
				</div>
				@php
					$i++;
				@endphp
       @endforeach

    </div>
</div>

@endsection