@extends('layouts.frontend')

@section('content')

  <!-- banner -->
        <div class="banner about-w3bnr" style="background: url({{ asset('images/about.jpg') }}) no-repeat right;">

            <!-- header -->
            @include('frontend.header')
            <!-- //header-end -->

            <!-- banner-text -->
           <div class="banner-text">
                <div class="container">
                    <h2><span>You Tried The Rest</span><br> Now Try The Best</h2>
                </div>
            </div>
        </div>

        <div class="container">
            <ol class="breadcrumb w3l-crumbs">
                <li><a href="index.php"><i class="fa fa-home"></i> Home</a></li>
                <li class="active">About</li>
            </ol>
        </div>
        <!-- //breadcrumb -->
        <!--  about-page -->
        <div class="about">
            <div class="container">
                <h3 class="w3ls-title w3ls-title1">About Us</h3>
                <div class="about-text">
                    <p>Welcome to KABABIA – a pleasant and elegant Indian cuisine with superb service. To make your visit memorable, the menu has been compiled to place at your disposal a choice of delicately prepared and pleasingly tasty dishes having the aroma and flavor of Indian spices made to perfection. Let the mystic environment of KABABIA blend with your valued presence. KABABIA today symbolizes to the traditional home made curry flavors in the Sylhet. With several years of business experience the management hired one of the best chefs in Bangladesh and have managed to extinguish Indian cliché in the menu by delivering the cuisine with a modernized whirl.</p>

                    <div class="clearfix"> </div>
                </div>
                <div class="history">
                    <h3 class="w3ls-title">How does it work ?</h3>
                    <p>Welcome to KABABIA – a pleasant and elegant Indian cuisine with superb service. To make your visit memorable, the menu has been compiled to place at your disposal a choice of delicately prepared and pleasingly tasty dishes having the aroma and flavor of Indian spices made to perfection. Let the mystic environment of KABABIA blend with your valued presence. KABABIA today symbolizes to the traditional home made curry flavors in the Sylhet. With several years of business experience the management hired one of the best chefs in Bangladesh and have managed to extinguish Indian cliché in the menu by delivering the cuisine with a modernized whirl.</p>
                    <h3 class="w3ls-title">Our history</h3>
                    <p>Welcome to KABABIA – a pleasant and elegant Indian cuisine with superb service. To make your visit memorable, the menu has been compiled to place at your disposal a choice of delicately prepared and pleasingly tasty dishes having the aroma and flavor of Indian spices made to perfection. Let the mystic environment of KABABIA blend with your valued presence. KABABIA today symbolizes to the traditional home made curry flavors in the Sylhet. With several years of business experience the management hired one of the best chefs in Bangladesh and have managed to extinguish Indian cliché in the menu by delivering the cuisine with a modernized whirl.</p>
                </div>
            </div>
        </div>

@endsection