<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;

class SubcategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         $subcats = DB::table('int_subcategories')

         ->leftjoin('categories', 'int_subcategories.category_id','=','categories.id')
         ->select('int_subcategories.*','categories.cat_name')
         ->get();
        return view('backend.int_subcategory.subcategories',compact('subcats'));
    }



    public function add()
    {
        //$cats = DB::table('categories')
        //->get();
        $types = DB::table('categories')->get();
        return view('backend.int_subcategory.addsubcategory',compact('types'));
    }



    public function store(Request $request)
    {
        //return Input::all();
        DB::table('int_subcategories')->insert(
        [
            'sub_cat_name' => Input::get('sub_cat_name'),
            'sub_cat_status' => 1,
            'category_id' => Input::get('category_id'),
        ]
        );
         return redirect('intsubcategories')->with('success', 'New Sub Category Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $cats = DB::table('categories')
        ->get();
         $sub_cat = DB::table('int_subcategories')
        ->where('int_subcategories.id',$id)
        ->leftjoin('categories','int_subcategories.category_id','=','categories.id')
        ->select('int_subcategories.*', 'categories.cat_name')
        ->get();
        $subcat = $sub_cat[0];
        return view('backend.int_subcategory.editsubcategory',compact('cats','subcat'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('int_subcategories')
        ->where('id', $id)
        ->update(
        [
            'sub_cat_name' => Input::get('sub_cat_name'),
            'sub_cat_status' => 1,
            'category_id' => Input::get('category_id'),
        ]
        );
         return redirect('intsubcategories')->with('success', 'Sub Category updated Successfully');
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('int_subcategories')->where('id', '=', $id)->delete();
       // DB::table('services')->where('service_sub_cat_id', '=', $id)->delete();

        return redirect('intsubcategories')->with('success', 'Sub Category deleted Successfully');
    }
}
