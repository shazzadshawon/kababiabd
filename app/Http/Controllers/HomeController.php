<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    
    public function home(Request $request)
    {




        //$sliders = DB::table('sliders')->get();
        $cover = DB::table('sliders')->first();
        $services = DB::table('abouts')->get();
       
        $dishes = DB::table('teams')->get();
        $stories = DB::table('birthday')->get();

        $galleries = DB::table('galleries')->get();

        $offers = DB::table('blogs')->get();
        $reviews = DB::table('reviews')->get();

        $work_gallery_cats = DB::table('work_gallery_cats')->get();
        $workgalleries = DB::table('workgalleries')->get();
       
 // echo "<pre>";
 //         print_r($reviews);
 //         exit;
//return $reviews;
        return view('frontend.home',compact('cover','services','dishes','offers','galleries','reviews','work_gallery_cats'));
    }

   

     public function about()
    {

        return view('frontend.about');
    }     

    public function menu($id)
    {
      $offers = DB::table('blogs')->get();
      $main = DB::table('categories')->where('id',$id)->first();
      $menus = DB::table('int_subcategories')->where('category_id',$id)
                ->leftjoin('categories','int_subcategories.category_id','categories.id')
                ->select('int_subcategories.*','categories.cat_name')
                ->get();

        return view('frontend.menu',compact('menus','main','offers'));
    }       

    public function submenu($id)
    {
        $submenus =  DB::table('int_subcategories')->orderby('id','desc')->get();
        $sub = DB::table('int_subcategories')->where('id',$id)->first();
        $portfolios = DB::table('portfolios')->where('service_type_id',$id)->orderby('id','desc')->get();
        $dishes = DB::table('teams')->orderby('id','desc')->get();
        return view('frontend.products',compact('portfolios','dishes','sub','submenus'));
    }     
    public function portfolio($id)
    {
        $portfolio = DB::table('portfolios')->where('id',$id)->first();
        return view('frontend.rongtuli.portfolio_details',compact('portfolio'));
    }    

    public function service($id)
    {
        $portfolio = DB::table('services')->where('id',$id)->first();
        return view('frontend.rongtuli.service_detail',compact('portfolio'));
    }    
   public function packages()
    {
        $packages = DB::table('services')->get();
        return view('frontend.packages',compact('packages'));
    }    


    public function gallery()
    {
         //$images = DB::table('gallery_cats')->get();
         $portfolios = DB::table('galleries')->get();
         //return count($galleries);
        return view('frontend.gallery',compact('portfolios'));
    }   

















  //   public function service_cat($id)
  //   {
  //       $services = DB::table('services')
  //       ->where('service_type_id',$id)
  //       ->get();

  //       $type = DB::table('servicetypes')
  //           ->where('id',$id)
  //           ->first();
  //       return view('frontend.service_cat',compact('services','type'));
  //   }    
    
  //   public function allpackage()
  //   {
  //       $packages = DB::table('packages')->get();
  //       return view('frontend.allpackage',compact('packages'));
  //   }  

    

  //   public function wedding_event($id)
  //   {
       
  //       $servic = DB::table('services')->where('id',$id)->get();
  //       $service = $servic[0];
  //       return view('frontend.singlewedding',compact('service'));
  //   }    

  //   public function birthday_cat()
  //   {
  //       $birthdays = DB::table('birthday')->get();
  //       return view('frontend.allbirthday',compact('birthdays'));
  //   }  
  // public function birthday($id)
  //   {
       
  //       $servic = DB::table('birthday')->where('id',$id)->get();
  //       $service = $servic[0];
  //       return view('frontend.singlebirthday',compact('service'));
  //   }    

  //    public function music_cat()
  //   {
  //       $musics = DB::table('music')->get();
  //       return view('frontend.allmusic',compact('musics'));
  //   }  
  // public function music($id)
  //   {
       
  //       $servic = DB::table('music')->where('id',$id)->get();
  //       $service = $servic[0];
  //       return view('frontend.singlemusic',compact('service'));
  //   }    


  // public function int($id)
  //   {
       
  //       $servic = DB::table('interior')->where('id',$id)->get();
  //       $service = $servic[0];
  //       return view('frontend.singleinterior',compact('service'));
  //   }    
  //   public function allint($id)
  //   {
       
  //       $ct = DB::table('int_categories')->where('id',$id)->get();
  //       $catid = $ct[0]->id;
  //       $catname = $ct[0]->cat_name;
  //       $ints = DB::table('interior')->where('service_sub_cat_id',$catid)->get();
        
  //       return view('frontend.allint',compact('ints','catname'));
  //   }    
  //    public function singlepackage($id)
  //   {
  //       $package = DB::table('packages')->where('id',$id)->first();
  //       return view('frontend.singlepackage',compact('package'));
  //   }     
  
  //   public function client_contact()
  //   {
  //       return view('frontend.contact_client');
  //   }    
    
  //   public function storecontact_client()
  //   {
  //       $filename = time().".jpg";

  //       Image::make(Input::file('contact_image'))->save('public/uploads/contact/'.$filename);

  //       DB::table('contacts')
  //       ->insert(
  //           [
  //               'contact_title' => Input::get('contact_title'),
  //               'contact_email' => Input::get('contact_phone'),
  //               'contact_phone' => Input::get('contact_phone'),
  //               'contact_image' => $filename,
  //               'contact_description' => Input::get('contact_description'),
  //           ]
  //           );
  //       return redirect()->back()->with('success','Message Sent Successfully');
  //   }    

    public function contact()
    {
        return view('frontend.contact');
    }    

     public function storecontact(Request $request)
    {
        DB::table('contacts')
        ->insert(
            [
                'contact_title' => Input::get('contact_title'),
                'contact_email' => Input::get('contact_email'),
                'contact_phone' => Input::get('contact_phone'),
                'code' => Input::get('code'),
                'contact_description' => Input::get('contact_description'),
                'address' => Input::get('address'),
            ]
            );
            
$to = "contact@kababiabd.com";
//$to = "akramul.i@agvcorp.com";
$subject = $request->get('contact_title');
$txt =  'Product Code: '.$request->get('code').'<\br>';
$txt .= 'Contact no: '.$request->get('contact_phone').'<\br>';
$txt .= 'Description: '.$request->get('contact_description').'<\br>';
$txt .= 'Address: '.$request->get('address').'<\br>';

$headers = "MIME-Version: 1.0" . "\r\n";
$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";

$headers .= 'From: <'.$request->get('contact_email').'>' . "\r\n";
//$headers .= 'Cc: user@example.com' . "\r\n";


mail($to,$subject,$txt,$headers);



        return redirect()->back()->with('success','Your order has been place successfully');
    }    
    


}
