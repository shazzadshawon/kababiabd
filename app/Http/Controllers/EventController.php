<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;
class EventController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = DB::table('events')->get();
        return view('backend.event.events',compact('events'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        return view('backend.event.addevent');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::table('events')->insert([
            'event_name' => Input::get('event_name'),
            'event_description' => Input::get('editor1'),
            'event_date' => Input::get('event_date'),
            ]);
        return redirect('events')->with('success','Event Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = DB::table('events')->where('id',$id)->first();
        return view('backend.event.editevent',compact('event'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
          DB::table('events')
            ->where('id', $id)
            ->update([
                    'event_name' => Input::get('event_name'),
                    
                    'event_description' => Input::get('editor1'),
                    
                    
                ]);
        if (!empty(Input::get('event_date'))) {
             DB::table('events')
            ->where('id', $id)
            ->update([
                    
                    'event_date' => Input::get('event_date'),
                    
                    
                    
                ]);
        }
        return redirect('events')->with('success','Event Info Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         DB::table('events')
            ->where('id', $id)->delete();
            return redirect('events')->with('success','Event Info Deleted Successfully');
    }
}
