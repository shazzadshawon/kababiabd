<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;
class StoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //return 'gt';
        $services = DB::table('birthday')->get();
        return view('backend.story.storys',compact('services'));
    }


    public function add()
    {
        
        return view('backend.story.addstory');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $imgname = Input::get('name');
        $filename = time().'.jpg';



        Image::make(Input::file('name'))->save('public/uploads/birthday/'.$filename);

        DB::table('birthday')->insert(
        [
            'service_title' => Input::get('service_title'),
           
            'service_image' => $filename,
            'service_description' => Input::get('editor1'),
            'service_status' => 1,
        ]
        );
         return redirect('storys')->with('success', 'New Story Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $services = DB::table('birthday')
                    ->where('id',$id)
                    ->get();
        //$servicetypes = DB::table('int_subcategories')->get();
        $service = $services[0];
        //return $services;
        return view('backend.story.editstory',compact('service'));
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //return Input::all();
         DB::table('birthday')
            ->where('id', $id)
            ->update([
                    'service_title' => Input::get('service_title'),
                    'service_description' => Input::get('editor1'),
                    'service_status' => 1,
                ]);
            if(Input::file('name'))
            {
                $item = DB::table('birthday')
                    ->where('id', $id)->first();
                $image = $item->service_image;

                unlink('public/uploads/birthday/'.$image);

                $filename = time().'.jpg';

            Image::make(Input::file('name'))->save('public/uploads/birthday/'.$filename);
            DB::table('birthday')
            ->where('id', $id)
            ->update([
                    
                    'service_image' => $filename,
                    
                ]);

            }

            return redirect('storys')->with('success', 'Story Updated Successfully');

    }



    public function destroy($id)
    {
         $item = DB::table('birthday')
                    ->where('id', $id)->first();
                $image = $item->service_image;

                unlink('public/uploads/birthday/'.$image);

        
        DB::table('birthday')->where('id', $id)->delete();


        return redirect('storys')->with('success', 'Selected  Story removed Successfully');
    }

//     public function edit_video()
//     {
//         $video = DB::table('video')->first();
//         //return $video->video_name;
//         return view('backend.birthday.editvideo',compact('video'));
//     }


//     public function update_video()
//     {

//         $input = Input::all();

//         $video = DB::table('video')->first();

//         $name = $video->video_name;

//         unlink('public/uploads/video/'.$name);

//         $file = array_get($input,'video_name');
//         // SET UPLOAD PATH 
//         $destinationPath = 'public/uploads/video/'; 
//         // GET THE FILE EXTENSION
//         $extension = $file->getClientOriginalExtension(); 
//         // RENAME THE UPLOAD WITH RANDOM NUMBER 
//         $fileName = rand(11111, 99999) . '.' . $extension; 
//         // MOVE THE UPLOADED FILES TO THE DESTINATION DIRECTORY 
//         $upload_success = $file->move($destinationPath, $fileName); 
// //return $fileName;
//             DB::table('video')
//             ->where('id',1)
//             ->update([
//                 'video_name' => $fileName,
//                 ]);



    
//         return redirect()->back()->with('success','video updated Successfully');
     
//     }
    
  

}
