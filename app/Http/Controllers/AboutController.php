<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;

class AboutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $abouts = DB::table('abouts')
         ->get();
        return view('backend.about.abouts',compact('abouts'));
    }

   

    public function add()
    {
        return view('backend.about.addabout');
    }


    public function store(Request $request)
    {
        //return 'hy';
        DB::table('abouts')->insert(
        [
            'about_title' => Input::get('do'),
            'about_description' => Input::get('editor1'),
        ]
        );
        return redirect('abouts')->with('success', 'New data Added Successfully');
    }


    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $about = DB::table('abouts')
        ->where('id',$id)
        ->first();
        //return $cat;
        return view('backend.about.editabout',compact('about'));
    }


    public function update(Request $request, $id)
    {
        //return Input::all();

        DB::table('abouts')
            ->where('id', $id)
            ->update([
                     'about_title' => Input::get('do'),
                     'about_description' => Input::get('editor1'),
                     
                ]);

            return redirect('abouts')->with('success', 'Data Updated Successfully');


    }

  

    public function destroy($id)
    {
        DB::table('abouts')->where('id', $id)->delete();
        return redirect('abouts')->with('success', 'Data Deleted Successfully');
    }
}
