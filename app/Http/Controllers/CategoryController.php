<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cats = DB::table('categories')
         
         ->get();
        return view('backend.category.categories',compact('cats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        //$types = DB::table('servicetypes')->get();
        return view('backend.category.addcategory');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return 'hy';
         DB::table('categories')->insert(
        [
            'cat_name' => Input::get('cat_name'),
            'cat_type' => Input::get('cat_type'),
            'cat_status' => 1,
        ]
        );
         return redirect('categories')->with('success', 'New Category Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $types = DB::table('servicetypes')->get();
        $cat = DB::table('categories')
        // ->leftjoin('servicetypes', 'categories.cat_type','=','servicetypes.id')
        // ->select('categories.*','servicetypes.type_name')
        ->where('id',$id)
        ->get();
        //return $cat;
        return view('backend.category.editcategory',compact('cat','types'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //return Input::all();

        DB::table('categories')
            ->where('id', $id)
            ->update([
                     'cat_name' => Input::get('cat_name'),
                     'cat_type' => Input::get('cat_type'),
                ]);

            return redirect('categories')->with('success', 'Package Category Updated Successfully');


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        // $x = DB::table('categories')->where('id', '=', $id)->get();
        // $x->delete();

        DB::table('categories')->where('id', $id)->delete();
        //DB::table('categories')->where('id', $id)->delete();

        return redirect('categories')->with('success', 'Package Category Deleted Successfully');
    }
}
