<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;


class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sliders = DB::table('sliders')
        ->get();
        return view('backend.slider.sliders',compact('sliders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        return view('backend.slider.addslider');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $filename = time().'.jpg';
        Image::make(Input::file('slider_image'))->save('public/uploads/slider/'.$filename);
        DB::table('sliders')->insert(
        [
            'slider_title' => Input::get('slider_title'),
            'slider_subtitle' => Input::get('slider_subtitle'),
            'slider_image' => $filename,
            'slider_status' => 1,
        ]
        );
         return redirect('sliders')->with('success', 'New Slider Image Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
         $slide = DB::table('sliders')
                    ->where('id',$id)
                    ->get();
                    $slider = $slide[0];
        return view('backend.slider.editslider',compact('slider'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::table('sliders')
            ->where('id', $id)
            ->update([
                     'slider_title' => Input::get('slider_title'),
                    'slider_subtitle' => Input::get('slider_subtitle'),
                    
                ]);
            if(Input::file('slider_image'))
            {
                  $image =  DB::table('sliders')
                    ->where('id', $id)->first();
                    //unlink('public/uploads/slider/'.$image->slider_image);
                

                $file = $request->file('slider_image');
                $rules = array('file' => 'required|mimes:png,jpeg,jpg'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
                $validator = \Validator::make(array('file'=> $file), $rules);
                if($validator->passes()){
                  $destinationPath = 'public/uploads/slider/'; // upload folder in public directory
                  $extension = $file->getClientOriginalExtension();
                  //$filename = $file->getClientOriginalName();
                  $filename = \Auth::id().'_'.time().'.'.$extension;
                  $upload_success = $file->move($destinationPath, $filename);
                  
        
                  // save into database
                  
                 
                }
   
   
                   DB::table('sliders')
            ->where('id', $id)
            ->update([
                    
                    'slider_image' => $filename,
                    
                ]);

            }

            return redirect('sliders')->with('success', 'Slider Image Updated Successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        DB::table('sliders')->where('id', $id)->delete();
        return redirect('sliders')->with('success', 'Slider Image removed Successfully');
    
    }
}
