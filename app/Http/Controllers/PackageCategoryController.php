<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;

class PackageCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $cats = DB::table('package_category')
         
         ->get();
        return view('backend.packagecategory.package_category',compact('cats'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add()
    {
        //$types = DB::table('servicetypes')->get();
        return view('backend.packagecategory.addpackagecategory');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //return 'hy';
         DB::table('package_category')->insert(
        [
            'package_title' => Input::get('package_title'),
            'package_price' => Input::get('package_price'),
  
        ]
        );
         return redirect('editpackagecategory')->with('success', 'New Category Added Successfully');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {

        $cats = DB::table('package_category')
        // ->leftjoin('servicetypes', 'categories.cat_type','=','servicetypes.id')
        // ->select('categories.*','servicetypes.type_name')
        //->where('id',$id)
        ->get();
        //return $cat;
        return view('backend.packagecategory.editpackagecategory',compact('cats'));
    }


    public function update(Request $request, $id)
    {
        //return Input::all();

        DB::table('package_category')
            ->where('id', $id)
            ->update([
                    'package_title' => Input::get('package_title'),
                    'package_price' => Input::get('package_price'),

                    
                ]);

            return redirect('editpackagecategory')->with('success', ' Updated Successfully');


    }
        public function delete($id)
    {
        //return Input::all();
         DB::table('package_items')
            ->where('cat_id', $id)
            ->delete();

        DB::table('package_category')
            ->where('id', $id)
            ->delete();

            return redirect('editpackagecategory')->with('success', ' Package Deleted Successfully Successfully');


    }
 // public function update2(Request $request, $id)
 //    {
 //        //return Input::all();

 //        DB::table('package_category')
 //            ->where('id', 1)
 //            ->update([
 //                    'package_title1' => Input::get('package_title1'),
 //                    'package_price1' => Input::get('package_price1'),

                   
 //                ]);

 //            return redirect('editpackagecategory')->with('success', ' Updated Successfully');


 //    }
 // public function update3(Request $request, $id)
 //    {
 //        //return Input::all();

 //        DB::table('package_category')
 //            ->where('id', 1)
 //            ->update([
 //                    'package_title1' => Input::get('package_title2'),
 //                    'package_price1' => Input::get('package_price2'),

                    
 //                ]);

 //            return redirect('editpackagecategory')->with('success', ' Updated Successfully');


 //    }
 // public function update4(Request $request, $id)
 //    {
 //        //return Input::all();

 //        DB::table('package_category')
 //            ->where('id', 1)
 //            ->update([
 //                    'package_title1' => Input::get('package_title3'),
 //                    'package_price1' => Input::get('package_price3'),

 //                ]);

 //            return redirect('editpackagecategory')->with('success', ' Updated Successfully');


 //    }

  
    public function package_items()
    {
        // $x = DB::table('categories')->where('id', '=', $id)->get();
        // $x->delete();

        $cats = DB::table('package_category')->get();
        //DB::table('categories')->where('id', $id)->delete();

       return view('backend.packagecategory.package_items',compact('cats'));
    }
  
    public function addpackageitem()
    {
        // $x = DB::table('categories')->where('id', '=', $id)->get();
        // $x->delete();

        DB::table('package_items')->insert([
            'item' => Input::get('item'),
            'cat_id' => Input::get('cat_id'),
            ]);
        //DB::table('categories')->where('id', $id)->delete();

       return redirect('package_items')->with('success', 'Package Item Added Successfully');
    }



    public function deletepackageitem($id)
    {
        // $x = DB::table('categories')->where('id', '=', $id)->get();
        // $x->delete();

        DB::table('package_items')->where('id', $id)->delete();
        //DB::table('categories')->where('id', $id)->delete();

        return redirect('package_items')->with('success', ' Item Deleted Successfully');
    }
}
