<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\Input;
use DB;
class BlogController extends Controller
{

   


    public function index()
    {
        $blogs = DB::table('blogs')->orderby('id','desc')
                    ->get();
        return view('backend.blog.blogs',compact('blogs'));
    }

    


    public function add()
    {
        //$blogtypes = DB::table('blogtypes')->get();
        return view('backend.blog.addblog');
    }

   



    public function store(Request $request)
    {
        $imgname = Input::get('name');
        $filename = time().'.jpg';



        Image::make(Input::file('name'))->save('public/uploads/blog/'.$filename);

        DB::table('blogs')->insert(
        [
            'blog_title' => Input::get('blog_title'),
            'blog_type_id' => 0,
            'blog_image' => $filename,
            'blog_description' => Input::get('editor1'),
            'blog_status' => 1,
        ]
        );
         return redirect('allblogs')->with('success', 'New Service Added Successfully');
    }

   


 
     public function view( \SammyK\LaravelFacebookSdk\LaravelFacebookSdk $fb, $id)
    {
         $blogs = DB::table('blogs')
                    ->where('blogs.id',$id)
                    //->leftjoin('blogtypes','blogs.blog_type_id','=','blogtypes.id')
                   // ->select('blogs.*','blogtypes.type_name')
                    ->get();
        //$blogtypes = DB::table('blogtypes')->get();
        $blog = $blogs[0];
        
         
          
        session(['blog_id' => $blog->id]);
        session(['blog_title' => $blog->blog_title]);
         
        // if(!session_id()) {
        //     session_start();
        //     //$_SESSION["post_id"] = $cat->id;
        //     session(['wed' => '']);
        //     session(['wed' => $blog->id]);
        // }
        
        
        
        
      
         $login_link = $fb
            ->getRedirectLoginHelper()
            ->getLoginUrl('http://example.com/blog_callback', ['email','user_events']);
           // ->getLoginUrl('localhost/red_done_n/blog_callback', ['email', 'user_events']);
        
         
        return view('backend.blog.singleblog',compact('blog','login_link'));
    }

    
    
    
    



    public function edit($id)
    {
        $blogs = DB::table('blogs')
                    ->where('blogs.id',$id)
                    ->get();
       
        $blog = $blogs[0];
        //return $blogs;
        return view('backend.blog.editblog',compact('blog'));
    }


    public function update(Request $request, $id)
    {
        //return Input::all();
         DB::table('blogs')
            ->where('id', $id)
            ->update([
                    'blog_title' => Input::get('blog_title'),
                    'blog_description' => Input::get('editor1'),
                    'blog_status' => 1,
                ]);
            if(Input::file('name'))
            {
                //return 'hy';
                 $filename = time().'.jpg';

                 Image::make(Input::file('name'))->save('public/uploads/blog/'.$filename);
                   DB::table('blogs')
                    ->where('id', $id)
                    ->update([
                            
                            'blog_image' => $filename,
                            
                        ]);

            }

            return redirect('allblogs')->with('success', 'Service Updated Successfully');

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $ser = DB::table('blogs')->where('id', $id)->delete();
        //$image = $ser
        //if()


        return redirect('allblogs')->with('success', 'Selected Service removed Successfully');
    }



    
    
    
    
    public function blog_callback(\SammyK\LaravelFacebookSdk\LaravelFacebookSdk $fb)
    {
           
        // if(!session_id()) {
        //     session_start();
            
        // }
         
   
         $post_id =  session('blog_id');
         $post_title =  session('blog_title');
       
        // $FbAppInfo = new FbAppInfo();
        //         $fb = $FbAppInfo->GetFbAppInfo();
        
        $helper = $fb->getRedirectLoginHelper();
        
        try {
          $accessToken = $helper->getAccessToken();
          //return typeof($accessToken);
          //$accessToken = $helper->getAccessToken();
          //echo $accessToken; exit;
        } catch(Facebook\Exceptions\FacebookResponseException $e) {
         
            return redirect()->back()->with('danger',$e->getMessage());
            
        } catch(Facebook\Exceptions\FacebookSDKException $e) {
          // When validation fails or other local issues
          return redirect()->back()->with('danger',$e->getMessage());
        }
        
        if (! isset($accessToken)) {
          if ($helper->getError()) {
            header('HTTP/1.0 401 Unauthorized');
//            echo "Error: " . $helper->getError() . "\n";
//            echo "Error Code: " . $helper->getErrorCode() . "\n";
//            echo "Error Reason: " . $helper->getErrorReason() . "\n";
         //echo "Error Description: " . $helper->getErrorDescription() . "\n";
              return redirect()->back()->with('danger',$helper->getErrorDescription());
          } else {
            header('HTTP/1.0 400 Bad Request');
            return redirect()->back()->with('danger','Bad Request');
          }
         // exit;
        }
        
        // Logged in
        
       // echo '<h3>Access Token</h3>';
       // var_dump($accessToken->getValue());
        
        // The OAuth 2.0 client handler helps us manage access tokens
        $oAuth2Client = $fb->getOAuth2Client();
        
        // Get the access token metadata from /debug_token
        $tokenMetadata = $oAuth2Client->debugToken($accessToken);
        
        
       
        //echo '<h3>Metadata</h3>';
       // var_dump($tokenMetadata);
        
        // Validation (these will throw FacebookSDKException's when they fail)
        $tokenMetadata->validateAppId('131604694118980'); // Replace {app-id} with your app id
        // If you know the user ID this access token belongs to, you can validate it here
        //$tokenMetadata->validateUserId('123');
        $tokenMetadata->validateExpiration();
        
        if (! $accessToken->isLongLived()) {
          // Exchanges a short-lived access token for a long-lived one
          try {
            //$accessToken = $oAuth2Client->getLongLivedAccessToken('EAABeioWfpIgBAHOc0XTZBwVKb4PZAbhzOfkiZAy3LoZCLPZAu9WlYkrjpEPC75RMhNZAnxLyyztpFRMZCpoyOZC7XgxXKNLMUEtw3ZCi3GqL06gVdQOmdtKL5wTHpDy3Xr7XIVt8Fq6ZBobmOMso49r8iHgJ6AsYVcc6gnu6ZBMO6Jea5eJLsU2rQvZCqs42UZBwmdZChhgSD3zeBZA9QZDZD');
            $accessToken = $oAuth2Client->getLongLivedAccessToken('131604694118980|oyTccHXJJnF-MGVpJ6Rls0TgWWs');
                                                                    
          } catch (Facebook\Exceptions\FacebookSDKException $e) {
            
              return redirect()->back()->with('danger',$e->getMessage());
           
              
            //  echo "<p>Error getting long-lived access token: " . $helper->getMessage() . "</p>\n\n";
           // exit;
          }
        
          echo '<h3>Long-lived</h3>';
          //$access = $accessToken->getValue()
            
         // var_dump($accessToken->getValue());
        }
        
        $_SESSION['fb_access_token'] = (string) $accessToken;
        
        // User is logged in with a long-lived access token.
        // You can redirect them to a members-only page.
        //header('Location: https://example.com/members.php');
        
        
        
         // define your POST parameters (replace with your own values)
                $params = array(
                  "access_token" => $accessToken, // see: https://developers.facebook.com/docs/facebook-login/access-tokens/
                  "message" =>  $post_title,
                  "link" => "http://example.com/blog_callback/".$post_id,
                
                  "name" => "Shobarjonnoweb",
                 
                 
                );
                 
                // post to Facebook
                // see: https://developers.facebook.com/docs/reference/php/facebook-api/
                try {
                  $ret = $fb->post('/125126468121186/feed', $params);
                  return redirect('/shobarjonnoweb')->with('success','Successfully posted to Facebook');
                } catch(Exception $e) {
                  //echo $e->getMessage();
                  return redirect()->back()->with('danger',$e->getMessage());
                }
        
        

    }

}
