-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 23, 2017 at 11:20 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 5.6.30

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kababia`
--

-- --------------------------------------------------------

--
-- Table structure for table `abouts`
--

CREATE TABLE `abouts` (
  `id` int(10) UNSIGNED NOT NULL,
  `about_title` longtext COLLATE utf8mb4_unicode_ci,
  `about_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `about_description` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `abouts`
--

INSERT INTO `abouts` (`id`, `about_title`, `about_image`, `about_description`, `created_at`, `updated_at`) VALUES
(8, 'Title', NULL, '<p>g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6</p>', NULL, NULL),
(9, 'Demo', NULL, '<p>5y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y</p>\r\n\r\n<p>&nbsp;</p>', NULL, NULL),
(10, 'Neww', NULL, '<p>5y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y</p>', NULL, NULL),
(11, 'yjytj', NULL, '<p>5y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y5y6 g65y</p>', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `birthday`
--

CREATE TABLE `birthday` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_description` longtext COLLATE utf8mb4_unicode_ci,
  `service_sub_cat_id` int(11) DEFAULT NULL,
  `service_type_id` int(11) DEFAULT NULL,
  `service_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `birthday`
--

INSERT INTO `birthday` (`id`, `service_title`, `service_image`, `service_description`, `service_sub_cat_id`, `service_type_id`, `service_status`, `created_at`, `updated_at`) VALUES
(4, 'WEDDING PLANNING & MANAGEMENT', '1502010838.jpg', '<p>We Offer You An Array Of Services For Wedding Event Management And Co-Ordination While You Can Simply Sit Back And Enjoy The Celebrations And Our Expert Team Takes Care Of All The Arrangements. We Have An Expansive Database And Personal Relationships With Many Of Bangladeshi&#39;s Best Known Vendors And Key Suppliers. So It Easy For Us To Manage The Best Value Of Your Money.</p>', NULL, NULL, '1', NULL, NULL),
(5, 'PHOTO & VIDEOGRAPHY', '1502010970.jpg', '<p>The Blissful Day Of Your Wedding Is A Utopian Time When Two Lives Unite, And Two Hearts Beat As One. And Wedding Photo &amp; Videography Is The Memorializing Of Two People Reveling In Love And Making A Promise To Share A Life &ndash; For A Lifetime! Our Wedding Photography Services Will Preserve Each Magic Moment For All Time!</p>', NULL, NULL, '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `birthday_categories`
--

CREATE TABLE `birthday_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `cat_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cat_type` int(11) DEFAULT NULL,
  `cat_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `birthday_subcategories`
--

CREATE TABLE `birthday_subcategories` (
  `id` int(10) UNSIGNED NOT NULL,
  `sub_cat_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_cat_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_type` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `blogs`
--

CREATE TABLE `blogs` (
  `id` int(10) UNSIGNED NOT NULL,
  `blog_title` longtext COLLATE utf8mb4_unicode_ci,
  `blog_image` longtext COLLATE utf8mb4_unicode_ci,
  `blog_description` longtext COLLATE utf8mb4_unicode_ci,
  `blog_sub_cat_id` int(11) DEFAULT NULL,
  `blog_type_id` int(11) DEFAULT NULL,
  `blog_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `blogs`
--

INSERT INTO `blogs` (`id`, `blog_title`, `blog_image`, `blog_description`, `blog_sub_cat_id`, `blog_type_id`, `blog_status`, `created_at`, `updated_at`) VALUES
(20, 'fff', '1508416374.jpg', '<p>fff</p>', NULL, 0, '1', '2017-10-19 12:32:54', NULL),
(21, 'Special offer 1', '1508416359.jpg', '<p>Special offer Description</p>', NULL, 0, '1', '2017-10-19 12:32:39', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `cat_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cat_type` int(11) DEFAULT NULL,
  `cat_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `cat_name`, `cat_type`, `cat_status`, `created_at`, `updated_at`) VALUES
(6, 'Kabab', NULL, '1', NULL, NULL),
(7, 'Fish', NULL, '1', NULL, NULL),
(8, 'Beef', NULL, '1', NULL, NULL),
(9, 'Chicken', NULL, '1', NULL, NULL),
(10, 'Rice', NULL, '1', NULL, NULL),
(11, 'Chineese', NULL, '1', NULL, NULL),
(12, 'Thai', NULL, '1', NULL, NULL),
(13, 'Indian', NULL, '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `contacts`
--

CREATE TABLE `contacts` (
  `id` int(11) NOT NULL,
  `contact_title` longtext COLLATE utf8_unicode_ci,
  `code` longtext COLLATE utf8_unicode_ci,
  `contact_email` longtext COLLATE utf8_unicode_ci,
  `contact_phone` longtext COLLATE utf8_unicode_ci,
  `address` longtext COLLATE utf8_unicode_ci,
  `contact_description` longtext COLLATE utf8_unicode_ci,
  `contact_image` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `contacts`
--

INSERT INTO `contacts` (`id`, `contact_title`, `code`, `contact_email`, `contact_phone`, `address`, `contact_description`, `contact_image`, `created_at`, `updated_at`) VALUES
(9, 'Akash', 'bf007, bf-005', 'akash@gmail.com', '987654433', 'Banani, 28', 'Order Description', NULL, '2017-10-22 08:40:34', '2017-10-22 08:40:34');

-- --------------------------------------------------------

--
-- Table structure for table `eventcategories`
--

CREATE TABLE `eventcategories` (
  `id` int(10) UNSIGNED NOT NULL,
  `event_cat_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event_cat_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event_cat_type` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(10) UNSIGNED NOT NULL,
  `event_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `event_description` longtext COLLATE utf8mb4_unicode_ci,
  `event_date` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `event_name`, `event_image`, `event_description`, `event_date`, `created_at`, `updated_at`) VALUES
(1, 'Mega Event', NULL, '<p>Description</p>', '2017-08-08', NULL, NULL),
(2, 'njnuj', NULL, '<p>ju7j 8kiu8ooki desewr jyujyj weswede&nbsp;ju7j 8kiu8ooki desewr jyujyj weswede&nbsp;ju7j 8kiu8ooki desewr jyujyj weswede&nbsp;ju7j 8kiu8ooki desewr jyujyj weswede&nbsp;ju7j 8kiu8ooki desewr jyujyj weswede</p>', '2017-08-06', NULL, NULL),
(3, 'Mega Event 3', NULL, '<p>nyhnjh r4e45r4 ,l,k,kioliliok</p>', '2017-08-15', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `galleries`
--

CREATE TABLE `galleries` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` int(11) NOT NULL,
  `video_name` longtext COLLATE utf8mb4_unicode_ci,
  `cover_image` longtext COLLATE utf8mb4_unicode_ci,
  `gallery_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gallery_image_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `galleries`
--

INSERT INTO `galleries` (`id`, `type`, `video_name`, `cover_image`, `gallery_image`, `category_id`, `gallery_image_status`, `created_at`, `updated_at`) VALUES
(12, 0, NULL, NULL, '1508661730.jpg', NULL, '1', NULL, NULL),
(13, 0, NULL, NULL, '1508661736.jpg', NULL, '1', NULL, NULL),
(14, 0, NULL, NULL, '1508661785.jpg', NULL, '1', NULL, NULL),
(15, 0, NULL, NULL, '1508661803.jpg', NULL, '1', NULL, NULL),
(16, 0, NULL, NULL, '1508661810.jpg', NULL, '1', NULL, NULL),
(17, 0, NULL, NULL, '1508661825.jpg', NULL, '1', NULL, NULL),
(18, 0, NULL, NULL, '1508661834.jpg', NULL, '1', NULL, NULL),
(19, 0, NULL, NULL, '1508661859.jpg', NULL, '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `gallery_cats`
--

CREATE TABLE `gallery_cats` (
  `id` int(11) NOT NULL,
  `cat_name` longtext,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `gallery_cats`
--

INSERT INTO `gallery_cats` (`id`, `cat_name`, `status`) VALUES
(12, 'Nature', NULL),
(13, 'Photography', NULL),
(14, 'Landscape', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hit_counter`
--

CREATE TABLE `hit_counter` (
  `id` int(11) NOT NULL,
  `counter` int(11) DEFAULT NULL,
  `daily_count` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hit_counter`
--

INSERT INTO `hit_counter` (`id`, `counter`, `daily_count`, `created_at`, `updated_at`) VALUES
(1, 9, 2, '2017-08-01 05:26:41', '17-08-01');

-- --------------------------------------------------------

--
-- Table structure for table `interior`
--

CREATE TABLE `interior` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_description` longtext COLLATE utf8mb4_unicode_ci,
  `service_sub_cat_id` int(11) DEFAULT NULL,
  `service_type_id` int(11) DEFAULT NULL,
  `service_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `interior`
--

INSERT INTO `interior` (`id`, `service_title`, `service_image`, `service_description`, `service_sub_cat_id`, `service_type_id`, `service_status`, `created_at`, `updated_at`) VALUES
(3, 'Catering Package 1', '1502014346.jpg', '<p>This is where catering comes in. A good catering service, like ours, will provide</p>\r\n\r\n<ul>\r\n	<li>The delicious menu</li>\r\n	<li>Help with setting up</li>\r\n	<li>Arrange the dining area</li>\r\n	<li>Free Drink for Everyone</li>\r\n	<li>Serve the food efficiently</li>\r\n	<li>Take care to avoid any food related glitches</li>\r\n	<li>Serve as waiters and waitresses</li>\r\n	<li>Assist at the bar, if needed</li>\r\n	<li>Calculate the amount of food needed</li>\r\n	<li>Decide the sequence of dishes</li>\r\n	<li>Make sure the food tastes and looks good</li>\r\n	<li>Explain the exotic dishes to guests</li>\r\n</ul>', 6, NULL, '1', NULL, NULL),
(4, 'Wedding Package 1', '1502014301.jpg', '<p>It is amazing how the right lighting and sound systems can transform your event from a mundane, one among a thousand weddings to an ethereal, magical, fairy tale land where the beautiful princess ties the knot with her prince charming.</p>\r\n\r\n<p>Good and well placed lighting, we have found, can hide all the minor faults that can often make a wedding venue unattractive. It subtly highlights the more pleasant elements that are worth showing off. We always emphasize to our clients the fact of how important the proper lighting is for their marriage location.</p>', 5, NULL, '1', NULL, NULL),
(5, 'Wedding Package 2', '1502014251.jpg', '<p>Choosing the right wedding venue is important, as it can often be one of the things that makes or mars your special day. When the preliminaries are ever, and the D-Day has been decided, You, now, have some idea of when the event will take place, it&rsquo;s time to decide on the wedding venue!</p>\r\n\r\n<p>The process is complicated, and requires a fair amount of serious thought. It is not a simple matter of randomly deciding on the venue your best friend or neighbor tied the knot at. Each wedding, each couple, each event is different, so... take all your special needs into account while choosing the wedding venue</p>', 5, NULL, '1', NULL, NULL),
(6, 'Catering Package 2', '1502014407.jpg', '<p>Finding a wedding caterer who will not let you down is a lot to manage on your own. While no celebration of such a magnitude is ever complete if good food is not part of it, finding the right person or service to provide that food can make or mar the wedding for your guests. Unless the guests enjoy the food set out for them, their enjoyment of the event will be incomplete. Make the food and the service great, on the other hand, and it adds a dimension to their enjoyment, and they will be talking about it for a long time afterwards!</p>', 6, NULL, '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `int_categories`
--

CREATE TABLE `int_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `cat_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cat_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `int_categories`
--

INSERT INTO `int_categories` (`id`, `cat_name`, `cat_status`, `created_at`, `updated_at`) VALUES
(5, 'WEDDING PACKAGES', '1', NULL, NULL),
(6, 'CATERING PACKAGES', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `int_subcategories`
--

CREATE TABLE `int_subcategories` (
  `id` int(10) UNSIGNED NOT NULL,
  `sub_cat_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` int(11) DEFAULT NULL,
  `sub_cat_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `int_subcategories`
--

INSERT INTO `int_subcategories` (`id`, `sub_cat_name`, `category_id`, `sub_cat_status`, `created_at`, `updated_at`) VALUES
(8, 'Beef Kabab', 6, '1', NULL, NULL),
(9, 'Fish Item 1', 7, '1', NULL, NULL),
(10, 'Fish Item 2', 7, '1', NULL, NULL),
(11, 'Set Menu 1', 10, '1', NULL, NULL),
(12, 'Steem Rice', 10, '1', NULL, NULL),
(13, 'Mashroom Rice', 10, '1', NULL, NULL),
(14, 'Keema Rice', 10, '1', NULL, NULL),
(15, 'Fruit Sushi', 11, '1', NULL, NULL),
(16, 'Heavenly Coconut', 12, '1', NULL, NULL),
(17, 'Beef Afgani', 8, '1', NULL, NULL),
(18, 'Chicken Afgani', 9, '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(13, '2014_10_12_000000_create_users_table', 1),
(14, '2014_10_12_100000_create_password_resets_table', 1),
(15, '2017_07_17_113813_create_categories_table', 1),
(16, '2017_07_17_113838_create_subcategories_table', 1),
(17, '2017_07_17_113916_create_sliders_table', 1),
(18, '2017_07_17_113940_create_events_table', 1),
(19, '2017_07_17_113959_create_teams_table', 1),
(20, '2017_07_17_114018_create_reviews_table', 1),
(21, '2017_07_17_114045_create_galleries_table', 1),
(22, '2017_07_17_114101_create_abouts_table', 1),
(23, '2017_07_17_114121_create_services_table', 1),
(24, '2017_07_18_035856_create_eventcategories_table', 1),
(27, '2017_07_18_042138_create_packages_table', 2),
(28, '2017_07_18_042154_create_packagecategories_table', 2),
(29, '2017_07_18_055256_create_servicetypes_table', 3),
(30, '2014_02_09_225721_create_visitor_registry', 4);

-- --------------------------------------------------------

--
-- Table structure for table `music`
--

CREATE TABLE `music` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_description` longtext COLLATE utf8mb4_unicode_ci,
  `service_sub_cat_id` int(11) DEFAULT NULL,
  `service_type_id` int(11) DEFAULT NULL,
  `service_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `music`
--

INSERT INTO `music` (`id`, `service_title`, `service_image`, `service_description`, `service_sub_cat_id`, `service_type_id`, `service_status`, `created_at`, `updated_at`) VALUES
(1, 'music 1', '1501179964.jpg', '<p>po8nuop;i</p>', NULL, NULL, '1', NULL, NULL),
(3, 'music 2', '1501180077.jpg', '<p>o8ujo8u nop9ou9 p009u0p9</p>', NULL, NULL, '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `music_subcategories`
--

CREATE TABLE `music_subcategories` (
  `id` int(10) UNSIGNED NOT NULL,
  `sub_cat_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_cat_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_type` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `offers`
--

CREATE TABLE `offers` (
  `id` int(11) NOT NULL,
  `offer_description` longtext,
  `offer_start` longtext,
  `offer_end` longtext,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `offers`
--

INSERT INTO `offers` (`id`, `offer_description`, `offer_start`, `offer_end`, `created_at`, `updated_at`) VALUES
(2, 'Offer 1', '15 07 2017 12:00 am', '31 07 2017 12:00 am', NULL, NULL),
(3, 'Offer 2', '08 09 2017 12:00 am', '29 09 2017 12:00 am', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `package_category`
--

CREATE TABLE `package_category` (
  `id` int(11) NOT NULL,
  `package_title` longtext,
  `package_price` longtext
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `package_category`
--

INSERT INTO `package_category` (`id`, `package_title`, `package_price`) VALUES
(1, 'Bronze', '10000'),
(5, 'Silver', '150000'),
(6, 'Gold', '25000'),
(7, 'Plutinum', '50000'),
(8, 'new', '545');

-- --------------------------------------------------------

--
-- Table structure for table `package_items`
--

CREATE TABLE `package_items` (
  `id` int(11) NOT NULL,
  `item` longtext,
  `cat_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `package_items`
--

INSERT INTO `package_items` (`id`, `item`, `cat_id`) VALUES
(2, 'item1-bronze', 1),
(3, 'item1-Gold', 6),
(4, 'New item', 8),
(5, 'New item', 7),
(6, 'New item', 5);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `portfolios`
--

CREATE TABLE `portfolios` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `code` longtext COLLATE utf8mb4_unicode_ci,
  `service_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_description` longtext COLLATE utf8mb4_unicode_ci,
  `price` longtext COLLATE utf8mb4_unicode_ci,
  `service_sub_cat_id` int(11) DEFAULT NULL,
  `service_type_id` int(11) DEFAULT NULL,
  `service_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `portfolios`
--

INSERT INTO `portfolios` (`id`, `service_title`, `code`, `service_image`, `service_description`, `price`, `service_sub_cat_id`, `service_type_id`, `service_status`, `created_at`, `updated_at`) VALUES
(19, 'Beef 7', 'bk007', '1508650526.jpg', '<p>hnu</p>', '400', NULL, 8, '1', NULL, NULL),
(20, 'Beef 5', 'bk-005', '1508650439.jpg', '<p>mhgm</p>', '680', NULL, 17, '1', NULL, NULL),
(21, 'Beef 6', NULL, '1508650500.jpg', '<p>uiiiiiiiiii</p>', '250', NULL, 8, '1', NULL, NULL),
(22, 'Beef 4', NULL, '1508650419.jpg', '<p>hjyjt</p>', '450', NULL, 17, '1', NULL, NULL),
(23, 'Beef 1', NULL, '1508647701.jpg', '<p>Description</p>', '750', NULL, 8, '1', NULL, NULL),
(24, 'Beef 2', NULL, '1508649846.jpg', '<p>trgft4 gytyki65 ioyk6g5ioy</p>', '4566', NULL, 8, '1', NULL, NULL),
(25, 'Beef 3', NULL, '1508649895.jpg', '<p>hgj uihui fnriguvt iojtrme</p>', '555', NULL, 8, '1', NULL, NULL),
(26, 'Beef 8', NULL, '1508652532.jpg', '<p>uj77i klolkli</p>', NULL, NULL, 8, '1', NULL, NULL),
(27, 'Beef 9', 'bk-009', '1508660688.jpg', '<p>ffvdrfvrfr gbghjyjt myukyukyk</p>', '499', NULL, 8, '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `reviews`
--

CREATE TABLE `reviews` (
  `id` int(10) UNSIGNED NOT NULL,
  `review_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `review_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `review_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `review_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `reviews`
--

INSERT INTO `reviews` (`id`, `review_title`, `review_description`, `review_image`, `review_status`, `created_at`, `updated_at`) VALUES
(8, 'Client 1', '<p>This is a demo review 1</p>', '1508732708.jpg', '1', '23 October, 2017', NULL),
(9, 'Client 2', '<p>Demo&nbsp;review 2</p>', '1508732723.jpg', '1', '23 October, 2017', NULL),
(10, 'Client 3', '<p>trbtyhnyun 65u6n5u5u</p>', '1508732753.jpg', '1', '23 October, 2017', NULL),
(11, 'Client 4', '<p>tbt 6yh67 65yuh65yun 656un6un 77jmuyjmuy hjujymnu drfewr sdcse ki,lk,il&nbsp;</p>', '1508732768.jpg', '1', '23 October, 2017', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `services`
--

CREATE TABLE `services` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_description` longtext COLLATE utf8mb4_unicode_ci,
  `service_sub_cat_id` int(11) DEFAULT NULL,
  `service_type_id` int(11) DEFAULT NULL,
  `service_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `services`
--

INSERT INTO `services` (`id`, `service_title`, `service_image`, `service_description`, `service_sub_cat_id`, `service_type_id`, `service_status`, `created_at`, `updated_at`) VALUES
(15, 'Package 1', '1508666617.jpg', '<p>We offer you an array of services for wedding event management and co-ordination while you can simply sit back and enjoy the celebrations and our expert team takes care of all the arrangements. We have an expansive database and personal relationships with many</p>', 3, 3, '1', NULL, NULL),
(16, 'Package 2', '1508666634.jpg', '<p>ave specialised in live events and have worked for clients ranging from BBC television to the Royal family. Balloon releases for the Millennium and balloon drops on live television. We have also become specialists in &#39;balloon lifts&#39; enabling people to &#39;fly&#39; using balloons. Our Balloon Decorating team are willing and able to produce almost anything you require. If you would like to discuss our Balloon Dec</p>', 3, 2, '1', NULL, NULL),
(17, 'Package 5', '1508666644.jpg', '<p>ave specialised in live events and have worked for clients ranging from BBC television to the Royal family. Balloon releases for the Millennium and balloon drops on live television. We have also become specialists in &#39;balloon lifts&#39; enabling people to &#39;fly&#39; using balloons. Our Balloon Decorating team are willing and able to produce almost anything you require. If you would like to discuss our Balloon Dec</p>', NULL, 0, '1', NULL, NULL),
(18, 'Package 3', '1508666653.jpg', '<p>ave specialised in live events and have worked for clients ranging from BBC television to the Royal family. Balloon releases for the Millennium and balloon drops on live television. We have also become specialists in &#39;balloon lifts&#39; enabling people to &#39;fly&#39; using balloons. Our Balloon Decorating team are willing and able to produce almost anything you require. If you would like to discuss our Balloon Dec</p>', NULL, 0, '1', NULL, NULL),
(19, 'Package 4', '1505799969.jpg', '<p>ver the years we have specialised in live events and have worked for clients ranging from BBC television to the Royal family. Balloon releases for the Millennium and balloon drops on live television. We have also become specialists in &#39;balloon lifts&#39; enabling people to &#39;fly&#39; using balloons. Our Balloon Decorating team are willing and able to produce almost anything you require. If you would like to discuss our Balloon Decorat</p>', NULL, 0, '1', NULL, NULL),
(20, 'Package 6', '1508666664.jpg', '<p>ave specialised in live events and have worked for clients ranging from BBC television to the Royal family. Balloon releases for the Millennium and balloon drops on live television. We have also become specialists in &#39;balloon lifts&#39; enabling people to &#39;fly&#39; using balloons. Our Balloon Decorating team are willing and able to produce almost anything you require. If you would like to discuss our Balloon Dec</p>', NULL, 0, '1', NULL, NULL),
(21, 'Package 7', '1508666685.jpg', '<p>Dala service</p>', NULL, 0, '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `servicetypes`
--

CREATE TABLE `servicetypes` (
  `id` int(10) UNSIGNED NOT NULL,
  `type_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `servicetypes`
--

INSERT INTO `servicetypes` (`id`, `type_name`, `created_at`, `updated_at`) VALUES
(1, 'Wedding', NULL, NULL),
(2, 'Birthday', NULL, NULL),
(3, 'Corporate', NULL, NULL),
(4, 'Ticketing', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `single`
--

CREATE TABLE `single` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_description` longtext COLLATE utf8mb4_unicode_ci,
  `service_sub_cat_id` int(11) DEFAULT NULL,
  `service_type_id` int(11) DEFAULT NULL,
  `service_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `sliders`
--

CREATE TABLE `sliders` (
  `id` int(10) UNSIGNED NOT NULL,
  `slider_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slider_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slider_subtitle` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slider_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sliders`
--

INSERT INTO `sliders` (`id`, `slider_image`, `slider_title`, `slider_subtitle`, `slider_status`, `created_at`, `updated_at`) VALUES
(3, '1508666562.jpg', 'You Tried The Rest', 'Now Try The Best', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `subcategories`
--

CREATE TABLE `subcategories` (
  `id` int(10) UNSIGNED NOT NULL,
  `sub_cat_name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_cat_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_type` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `subcategories`
--

INSERT INTO `subcategories` (`id`, `sub_cat_name`, `sub_cat_status`, `service_type`, `created_at`, `updated_at`) VALUES
(3, 'sub cat 1', '1', 1, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `teams`
--

CREATE TABLE `teams` (
  `id` int(10) UNSIGNED NOT NULL,
  `team_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `team_description` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `team_contact` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `team_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `team_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `teams`
--

INSERT INTO `teams` (`id`, `team_title`, `team_description`, `team_contact`, `team_image`, `team_status`, `created_at`, `updated_at`) VALUES
(4, 'Item 1', 'Item 1', NULL, '1508415708.jpg', '1', NULL, NULL),
(13, 'Item 2', 'Item 2', NULL, '1508415723.jpg', '1', NULL, NULL),
(14, 'Item 3', 'Item 3', NULL, '1508415735.jpg', '1', NULL, NULL),
(15, 'Item 4', 'Item 4', NULL, '1508415754.jpg', '1', NULL, NULL),
(16, 'Item 5', 'Item 5', NULL, '1508415772.jpg', '1', NULL, NULL),
(17, 'Item 6', 'Item 6', NULL, '1508415787.jpg', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'admin@gmail.com', '$2y$10$36UNCJVp0rA0UURa3leOiebSOKhgqjAticgs9BquGRH1uEv6LB/SS', 'ji2xi5P8kXUIXoYY6BQtBaTEqYFcae7tA7DSuggazgIoaZ9yiszWXtv5OFjx', NULL, NULL),
(2, 'Admin AGV', 'admin@agvcorp.biz', '$2y$10$IhzYjNrAxUQY6GlO4ZiAzuUtwquN7a.UB1ly2sA.QrFRESp6betX6', NULL, NULL, NULL),
(3, 'User', 'user@gmail.com', '$2y$10$.rSw4TumCpfMHdaVw7808u9quytqWV5v3MzZ06clWb9oZPc67cQu2', 'QeVAUwNyWhgQYmuv6ZG4COlbNc1ARlEmqJ1TTLUh7bkXUiBb3RilZuBkrz6j', NULL, NULL),
(4, 'Akash', 'akash@gmail.com', '$2y$10$yVMVbg/0ERbvhNQEiVI8TOVgUBa7sGAMYqzXPTbMw7UH.LuFvv0NS', NULL, NULL, NULL),
(6, 'Shadi Mubarak Admin', 'admin@shadimubarakbd.com', '$2y$10$DOtr/Yri61J672PqBA/05eC8E.Q8gb/KIRgHlPkDDloX8eWbHqz3C', 'NMnHvVE83GWG9zxAHHfiwbpdGtJKWGxhR3u5f6a2ddrlf68MJb91ud2NiV9q', NULL, NULL),
(7, 'RongTuli Admin', 'admin@rongtulievent.com', '$2y$10$wOWZcMX5Db0rgiOuUqvoReQGGIrqX3NXjzjEMUfcaT9/YSE4IWt2O', 'aWeSKpjuzvGxer7CIVeEGSb0hhzq95kbvADhTAM207OB8r96LH8Y6iwIilS3', NULL, NULL),
(8, 'Kababia Admin', 'admin@kababiabd.com', '$2y$10$VJ5zryf7OTMrwgRn2a2XJu8v02KWVJpKQ/7j4apPsvG6MM7SbH4IK', 'xDl7eaXR3Dz19b4j5Ng8gErGqMrtM5lUfX805lFS5pIU0OwzXMoacjGhoZMp', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `video`
--

CREATE TABLE `video` (
  `id` int(11) NOT NULL,
  `video_name` longtext COLLATE utf8_unicode_ci,
  `status` int(11) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `video`
--

INSERT INTO `video` (`id`, `video_name`, `status`, `created_at`, `updated_at`) VALUES
(1, '59294.mp4', 1, '2017-07-31 13:10:30', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `visitors`
--

CREATE TABLE `visitors` (
  `id` int(11) NOT NULL,
  `counter` int(11) DEFAULT '0',
  `daily_count` int(11) DEFAULT '0',
  `dhaka` int(11) DEFAULT '0',
  `chittagong` int(11) DEFAULT '0',
  `barisal` int(11) DEFAULT '0',
  `khulna` int(11) DEFAULT '0',
  `mymensingh` int(11) DEFAULT '0',
  `rajshahi` int(11) DEFAULT '0',
  `rangpur` int(11) DEFAULT '0',
  `sylhet` int(11) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `visitors`
--

INSERT INTO `visitors` (`id`, `counter`, `daily_count`, `dhaka`, `chittagong`, `barisal`, `khulna`, `mymensingh`, `rajshahi`, `rangpur`, `sylhet`, `created_at`, `updated_at`) VALUES
(1, 12, 5, 4, 2, 1, 1, 0, 0, 0, 0, NULL, '17-08-13');

-- --------------------------------------------------------

--
-- Table structure for table `visitor_registry`
--

CREATE TABLE `visitor_registry` (
  `id` int(10) UNSIGNED NOT NULL,
  `ip` varchar(32) COLLATE utf8mb4_unicode_ci NOT NULL,
  `country` varchar(4) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `clicks` int(10) UNSIGNED NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `visitor_registry`
--

INSERT INTO `visitor_registry` (`id`, `ip`, `country`, `clicks`, `created_at`, `updated_at`) VALUES
(1, '127.0.0.1', NULL, 1, '2017-07-30 05:45:56', '2017-07-30 05:45:56');

-- --------------------------------------------------------

--
-- Table structure for table `wedding`
--

CREATE TABLE `wedding` (
  `id` int(10) UNSIGNED NOT NULL,
  `service_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `service_description` longtext COLLATE utf8mb4_unicode_ci,
  `service_sub_cat_id` int(11) DEFAULT NULL,
  `service_type_id` int(11) DEFAULT NULL,
  `service_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `workgalleries`
--

CREATE TABLE `workgalleries` (
  `id` int(10) UNSIGNED NOT NULL,
  `type` int(11) NOT NULL,
  `video_name` longtext COLLATE utf8mb4_unicode_ci,
  `cover_image` longtext COLLATE utf8mb4_unicode_ci,
  `gallery_image` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `category_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `gallery_image_status` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `workgalleries`
--

INSERT INTO `workgalleries` (`id`, `type`, `video_name`, `cover_image`, `gallery_image`, `category_id`, `gallery_image_status`, `created_at`, `updated_at`) VALUES
(67, 0, NULL, NULL, '1505729245.jpg', '22', '1', NULL, NULL),
(68, 0, NULL, NULL, '1505729536.jpg', '22', '1', NULL, NULL),
(69, 0, NULL, NULL, '1505729543.jpg', '22', '1', NULL, NULL),
(70, 0, NULL, NULL, '1505729551.jpg', '23', '1', NULL, NULL),
(71, 0, NULL, NULL, '1505729561.jpg', '23', '1', NULL, NULL),
(72, 0, NULL, NULL, '1505729572.jpg', '24', '1', NULL, NULL),
(74, 1, '1505798959.mp4', 'cover-1505798959.jpg', NULL, '28', '1', NULL, NULL),
(75, 1, '1505799137.mp4', 'cover-1505799137.jpg', NULL, '22', '1', NULL, NULL),
(76, 1, '1505801630.mp4', 'cover-1505801630.jpg', NULL, '27', '1', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `work_gallery_cats`
--

CREATE TABLE `work_gallery_cats` (
  `id` int(11) NOT NULL,
  `cat_name` longtext,
  `status` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `work_gallery_cats`
--

INSERT INTO `work_gallery_cats` (`id`, `cat_name`, `status`) VALUES
(22, 'Bokeh', NULL),
(23, 'Landscape', NULL),
(24, 'Events', NULL),
(27, 'Photography', NULL),
(28, 'Wedding', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `abouts`
--
ALTER TABLE `abouts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `birthday`
--
ALTER TABLE `birthday`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `birthday_categories`
--
ALTER TABLE `birthday_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `birthday_subcategories`
--
ALTER TABLE `birthday_subcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `eventcategories`
--
ALTER TABLE `eventcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `galleries`
--
ALTER TABLE `galleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `gallery_cats`
--
ALTER TABLE `gallery_cats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hit_counter`
--
ALTER TABLE `hit_counter`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `interior`
--
ALTER TABLE `interior`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `int_categories`
--
ALTER TABLE `int_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `int_subcategories`
--
ALTER TABLE `int_subcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `music`
--
ALTER TABLE `music`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `music_subcategories`
--
ALTER TABLE `music_subcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `offers`
--
ALTER TABLE `offers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `package_category`
--
ALTER TABLE `package_category`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `package_items`
--
ALTER TABLE `package_items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `portfolios`
--
ALTER TABLE `portfolios`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `reviews`
--
ALTER TABLE `reviews`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `services`
--
ALTER TABLE `services`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `servicetypes`
--
ALTER TABLE `servicetypes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `single`
--
ALTER TABLE `single`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sliders`
--
ALTER TABLE `sliders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subcategories`
--
ALTER TABLE `subcategories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `teams`
--
ALTER TABLE `teams`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `video`
--
ALTER TABLE `video`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visitors`
--
ALTER TABLE `visitors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `visitor_registry`
--
ALTER TABLE `visitor_registry`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `wedding`
--
ALTER TABLE `wedding`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `workgalleries`
--
ALTER TABLE `workgalleries`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `work_gallery_cats`
--
ALTER TABLE `work_gallery_cats`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `abouts`
--
ALTER TABLE `abouts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `birthday`
--
ALTER TABLE `birthday`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `birthday_categories`
--
ALTER TABLE `birthday_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `birthday_subcategories`
--
ALTER TABLE `birthday_subcategories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `eventcategories`
--
ALTER TABLE `eventcategories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `galleries`
--
ALTER TABLE `galleries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `gallery_cats`
--
ALTER TABLE `gallery_cats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `hit_counter`
--
ALTER TABLE `hit_counter`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `interior`
--
ALTER TABLE `interior`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `int_categories`
--
ALTER TABLE `int_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `int_subcategories`
--
ALTER TABLE `int_subcategories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;
--
-- AUTO_INCREMENT for table `music`
--
ALTER TABLE `music`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `music_subcategories`
--
ALTER TABLE `music_subcategories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `offers`
--
ALTER TABLE `offers`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `package_category`
--
ALTER TABLE `package_category`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `package_items`
--
ALTER TABLE `package_items`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `portfolios`
--
ALTER TABLE `portfolios`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `reviews`
--
ALTER TABLE `reviews`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `services`
--
ALTER TABLE `services`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `servicetypes`
--
ALTER TABLE `servicetypes`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `single`
--
ALTER TABLE `single`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `sliders`
--
ALTER TABLE `sliders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `subcategories`
--
ALTER TABLE `subcategories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `teams`
--
ALTER TABLE `teams`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `video`
--
ALTER TABLE `video`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `visitors`
--
ALTER TABLE `visitors`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `visitor_registry`
--
ALTER TABLE `visitor_registry`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `wedding`
--
ALTER TABLE `wedding`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `workgalleries`
--
ALTER TABLE `workgalleries`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=77;
--
-- AUTO_INCREMENT for table `work_gallery_cats`
--
ALTER TABLE `work_gallery_cats`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
